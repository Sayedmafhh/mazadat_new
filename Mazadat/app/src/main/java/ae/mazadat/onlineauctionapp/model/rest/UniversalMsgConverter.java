package ae.mazadat.onlineauctionapp.model.rest;

import android.os.Parcel;
import android.util.Log;

import org.parceler.ParcelConverter;

import java.util.HashMap;

/**
 * Created by NirajKumar on 2/25/17.
 */

public class UniversalMsgConverter implements ParcelConverter<Object> {
    @Override
    public void toParcel(Object input, Parcel parcel) {
        if (input == null) {
            parcel.writeString("");
        }
        else {
            if (input instanceof String) {
                parcel.writeString((String) input);
            }else {
                Log.i("Size of value", "size of value:" + ((HashMap) input).size());
                parcel.writeString(input.toString());
            }
        }
    }

    @Override
    public Object fromParcel(Parcel parcel) {
        return parcel.readString();
    }
}