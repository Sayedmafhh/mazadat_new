package ae.mazadat.onlineauctionapp.model.rest;

import com.firebase.client.annotations.Nullable;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

/**
 * Created by NirajKumar on 2/24/17.
 */

@Parcel
public class UniversalMsg {
    @Nullable
    @SerializedName("name")
    public String name;

    @Nullable
    @SerializedName("label")
    public String label;

    @Nullable
    @SerializedName("value")
    @ParcelPropertyConverter(UniversalMsgConverter.class)
    public Object value;

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public Object getValue() {
        return value;
    }
}


