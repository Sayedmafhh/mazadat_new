package ae.mazadat.onlineauctionapp.model.draw;

/**
 * Created by agile on 5/30/16.
 */
public class MZDrawNumberItem  {

    int id;
    String type;

    String plateType;
    String leftNum;
    String rightNum;
    String city;
    String ask;
    String price;

    // Du
    String provider;
    String providerC;
    String number;
    String prefix;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLeftNum() {
        return leftNum;
    }

    public String getRightNum() {
        return rightNum;
    }

    public String getCity() {
        return city;
    }

    public void setLeftNum(String leftNum) {
        this.leftNum = leftNum;
    }

    public void setRightNum(String rightNum) {
        this.rightNum = rightNum;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPlateType() {
        return plateType;
    }

    public void setPlateType(String plateType) {
        this.plateType = plateType;
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getProviderC() {
        return providerC;
    }

    public void setProviderC(String providerC) {
        this.providerC = providerC;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
