package ae.mazadat.onlineauctionapp.adapter.gallery;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.parceler.Parcels;

import java.util.List;
import java.util.Random;

import ae.mazadat.onlineauctionapp.model.rest.MZImg;
import ae.mazadat.onlineauctionapp.ui.GalleryActivity;

/**
 * Created by agile on 5/16/16.
 */
public class PhotosPagerAdapter extends PagerAdapter {
    private final Random random = new Random();
    List<MZImg> mImages;
    Context mContext;

    public PhotosPagerAdapter(Context context, List<MZImg> images) {
        mImages = images;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int i) {

        ImageView img = new ImageView(mContext);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GalleryActivity.class);
                intent.putExtra("images", Parcels.wrap(mImages));
                intent.putExtra("position", i);

                mContext.startActivity(intent);
            }
        });

        String src = mImages.get(i).getSrc().replace("https", "http"); // TODO: Fix

        Glide.with(mContext).load(src).into(img);

        //img.setLayoutParams(new ViewPager.LayoutParams(Gallery.LayoutParams.FILL_PARENT, Gallery.LayoutParams.FILL_PARENT));
        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            Utils.fetchDrawableOnThread(Utils.getPath(ctx)+"/"+dataList.get(pos).getUrlImg(), img, true);
        view.addView(img, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);


        return img;
    }
}
