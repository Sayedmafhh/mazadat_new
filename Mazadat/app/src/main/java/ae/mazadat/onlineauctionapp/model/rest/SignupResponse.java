package ae.mazadat.onlineauctionapp.model.rest;

import org.parceler.Parcel;

/**
 * Created by agile on 4/4/16.
 */
@Parcel
public class SignupResponse {

    String lang;
    String status;
    String msg;

    public String getLang() {
        return lang;
    }

    public String getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }
}
