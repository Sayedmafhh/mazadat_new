package ae.mazadat.onlineauctionapp.ui.menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGApiResponse;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class ChangePwdFragment extends Fragment {

    AGApi api;
    Call<AGApiResponse> changePwdCall;

    SessionManager session = App.getSessionManager();
    Map<String, String> requestParams = new HashMap<>();

    @Bind(R.id.etPwd)
    EditText etPwd;

    @Bind(R.id.etRePwd)
    EditText etRePwd;

    @Bind(R.id.btnReset)
    Button btnReset;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ChangePwdFragment() {}

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ChangePwdFragment newInstance(int columnCount) {
        ChangePwdFragment fragment = new ChangePwdFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_pwd, container, false);
        ButterKnife.bind(this, view);

        requestParams.put("lang", session.getMZLang());

        api = App.getRestAdapter().create(AGApi.class);
        changePwdCall = api.changePwd(requestParams);

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPwd();
            }
        });

        return view;
    }

    private void resetPwd() {

        boolean cancel = false;
        View focusView = null;

        String pwd   = etPwd.getText().toString();
        String rePwd = etRePwd.getText().toString();

        // Check for a valid email / mobile
        if (TextUtils.isEmpty(pwd)) {
            etPwd.setError(getString(R.string.error_field_required));
            focusView = etPwd;
            cancel = true;
        }

        else if (TextUtils.isEmpty(rePwd)) {
            etRePwd.setError(getString(R.string.error_field_required));
            focusView = etRePwd;
            cancel = true;
        }

        else if (!pwd.contentEquals(rePwd)) {
            etRePwd.setError(getString(R.string.error_not_match_pwd));
            focusView = etRePwd;
            cancel = true;

            Toast.makeText(getActivity(), getString(R.string.error_not_match_pwd), Toast.LENGTH_SHORT).show();
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            AGUtil.closeKeyboard(getActivity());

            requestParams.put("new_password", pwd);
            requestParams.put("confirm_password", rePwd);

            final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(R.color.colorPrimaryDark);
            pDialog.setTitleText(getString(R.string.resetting_pwd));
            pDialog.setCancelable(false);
            pDialog.show();

            changePwdCall.enqueue(new AGRetrofitCallback<AGApiResponse>() {
                @Override
                public void onResponse(Call<AGApiResponse> call, Response<AGApiResponse> response) {
                    super.onResponse(call, response);

                    if (response.isSuccessful()) {
                        if (isAdded() && getActivity() != null) {
                            AGApiResponse res = response.body();

                            if (res.getStatus() != null && res.getStatus().contentEquals("200")) {
                                pDialog.dismiss();
                                AGUtil.successAlert(getActivity(), "Reset Password!", res.getMsg());
                            } else {
                                AGUtil.errorAlert(getActivity(), "Opps!", res.getMsg());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<AGApiResponse> call, Throwable t) {
                    super.onFailure(call, t);
                    if (isAdded() && getActivity() != null) {
                        pDialog.dismiss();
                        AGUtil.errorAlert(getActivity(), "Opps!", getString(R.string.err_unable_to_reset));
                    }
                }
            });
        }

    }

}
