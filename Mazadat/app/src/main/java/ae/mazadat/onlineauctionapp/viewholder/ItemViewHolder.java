package ae.mazadat.onlineauctionapp.viewholder;

import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by agile on 4/1/16.
 */
public class ItemViewHolder extends RecyclerView.ViewHolder {

    public final View mView;

    @Nullable
    @Bind(R.id.price)
    public TextView mPrice;

    @Nullable
    @Bind(R.id.lblBids)
    public TextView lblBids;

    @Nullable
    @Bind(R.id.bids)
    public TextView mBids;

    @Nullable
    @Bind(R.id.rtC1)
    public RelativeLayout rtC1;
    @Nullable
    @Bind(R.id.divider)
    public View divider;
    @Nullable
    @Bind(R.id.soldLayout)
    public RelativeLayout soldLayout;
    @Nullable
    @Bind(R.id.counterLayout)
    public LinearLayout counterLayout;
    @Nullable
    @Bind(R.id.rtC2)
    public RelativeLayout rtC2;

    @Nullable
    @Bind(R.id.rtC3)
    public RelativeLayout rtC3;

    // Timer
    @Nullable
    @Bind(R.id.tvC1)
    public TextView tvC1;

    @Nullable
    @Bind(R.id.tvC1T)
    public TextView tvC1T;

    @Nullable
    @Bind(R.id.tvC2)
    public TextView tvC2;

    @Nullable
    @Bind(R.id.tvC2T)
    public TextView tvC2T;

    @Nullable
    @Bind(R.id.tvC3)
    public TextView tvC3;

    @Nullable
    @Bind(R.id.tvC3T)
    public TextView tvC3T;

    @Nullable
    @Bind(R.id.vTag)
    public View vTag;
    @Nullable
    @Bind(R.id.hummerImg)
    public ImageView hummerImg;

//    @Bind(R.id.timer) public IconTextView timer;

    int position;
    public boolean isExpired = false;
    public boolean isRemoved = false;
    public String auctionType = "";

    public void setPosition(int position) {
        this.position = position;
    }

    //    @Bind(R.id.min_bid) public TextView mMinBid;

//    @Bind(R.id.tvDay) public TextView tvDay;
//    @Bind(R.id.tvHr)  public TextView tvHr;
//    @Bind(R.id.tvMin) public TextView tvMin;
//    @Bind(R.id.tvSec) public TextView tvSec;

//    @Bind(R.id.date) public TextView mDate;

//    @Bind(R.id.btnBid) public Button mBtnBid;

    public MZItem mItem;

    public void updateTimeRemaining() {
        if (mItem.getEndTime() != null) {
            long timeDiff = mItem.getEndTime().getTime() - (System.currentTimeMillis() - mItem.getTimeDiff());

            if (timeDiff > 0) {
                long diff = timeDiff;

                final long days = diff / (24 * 60 * 60 * 1000);
                diff -= days * (24 * 60 * 60 * 1000);

                final long hours = diff / (60 * 60 * 1000);
                diff -= hours * (60 * 60 * 1000);

                final long minutes = diff / (60 * 1000);
                diff -= minutes * (60 * 1000);

                final long seconds = diff / 1000;

                // Build UI
                if (days > 0) {
                    // Day + Hour + Min
                    tvC1T.setText(R.string.day);
                    tvC1.setText(days + "");

                    tvC2T.setText(R.string.hour);
                    tvC2.setText(hours + "");

                    tvC3T.setText(R.string.min);
                    tvC3.setText(minutes + "");

                } else {
                    // Hour + Min + Sec

                    tvC1T.setText(R.string.hour);
                    tvC1.setText(hours + "");

                    tvC2T.setText(R.string.min);
                    tvC2.setText(minutes + "");

                    tvC3T.setText(R.string.sec);
                    tvC3.setText(seconds + "");

                    if (seconds == 0 && minutes == 0 && hours == 0) {
                        // Expired item
                        isExpired = true;
                    } else {
                        // Blink when last 5 min
                        if (hours == 0 && minutes < 5) {
                            blinkOnce(vTag, false);
                            blinkOnce(rtC1, true);
                            blinkOnce(rtC2, true);
                            blinkOnce(rtC3, true);
                        }// stop if not 5 mins
                        else{
                           // stopBlink();
                            stopBlink(vTag, false);
                            stopBlink(rtC1, true);
                            stopBlink(rtC2, true);
                            stopBlink(rtC3, true);

                        }
                    }
                }


//            mPrice.setText(days + " days " + hours + " hrs " + minutes + " mins " + seconds + " sec");
            } else {
//            mPrice.setText("Expired!!");
            }
        }

    }

    public ItemViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        mView = view;
    }

    public void blinkOnce(final View v, final boolean isRelativeLayout) {
        Log.i("BlinkItemViewHolder", "In side blink in Item view holder");
        if (isRelativeLayout) {
            AnimationDrawable an = (AnimationDrawable) v.getBackground();
            an.start();
        } else {
            ViewPropertyAnimator an = v.animate().setDuration(200).alpha(1);
            final int redColor;
            final int normalColor;
            // TODO: Set color
            if (auctionType.contentEquals(AGConf.KEY_AUCTION_GRID)) {
                redColor = R.color.colorItemRedTrans;
                normalColor = R.color.colorItemGreenTrans;
            } else {
                redColor = R.color.colorItemRed;
                normalColor = R.color.colorItemGreen;
            }

            an.withEndAction(new Runnable() {
                @Override
                public void run() {
                    v.setBackgroundResource(redColor);
                    v.animate().setDuration(700).alpha(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            v.setBackgroundResource(normalColor);
                        }
                    });
                }
            });
        }
    }


    public void stopBlink(final View v, final boolean isRelativeLayout) {

        if (isRelativeLayout) {
            AnimationDrawable an = (AnimationDrawable) v.getBackground();
            an.stop();
        } /*else {
            ViewPropertyAnimator an = v.animate().setDuration(200).alpha(1);
            final int redColor;
            final int normalColor;
            // TODO: Set color
            if (auctionType.contentEquals(AGConf.KEY_AUCTION_GRID)) {
                redColor = R.color.colorItemRedTrans;
                normalColor = R.color.colorItemGreenTrans;
            } else {
                redColor = R.color.colorItemRed;
                normalColor = R.color.colorItemGreen;
            }

            an.withEndAction(new Runnable() {
                @Override
                public void run() {
                    v.setBackgroundResource(redColor);
                    v.animate().setDuration(700).alpha(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            v.setBackgroundResource(normalColor);
                        }
                    });
                }
            });
        }*/
    }

}
