package ae.mazadat.onlineauctionapp.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @Bind(R.id.coordinator)
    CoordinatorLayout vCoordinator;

    @Bind(R.id.content_frame)
    View vContainer;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawer,         /* DrawerLayout object */
                toolbar,
                R.string.navigation_drawer_open,  /* "open drawer" description */
                R.string.navigation_drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getActionBar().setTitle("Title ....");
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle("Title Drawer");
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(false);

        // mDrawerToggle.setHomeAsUpIndicator(R.drawable.menu_icon);
        toolbar.setNavigationIcon(R.drawable.ic_bars2);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
            }
        });

        drawer.setDrawerListener(mDrawerToggle);

        displayView(R.id.nav_home);
        setTitle("");
    }


    public static void startFromLocation(int[] startingLocation, Activity startingActivity) {
        Intent intent = new Intent(startingActivity, MainActivity.class);
        intent.putExtra(AGConf.ARG_REVEAL_START_LOCATION, startingLocation);
        startingActivity.startActivity(intent);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO: Hide text when already logged in!
        getMenuInflater().inflate(R.menu.home, menu);

        if (session.isLoggedIn()) {
            MenuItem item = menu.findItem(R.id.act_login);
            item.setVisible(false);

            MenuItem langItem = menu.findItem(R.id.act_lang_switch);
            langItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.act_login:
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up,R.anim.no_change);
                break;

            case R.id.act_lang_switch:
                switchLang();
                overridePendingTransition(0, 0);
                break;
        }

        return true;
    }




    @Override
    public void onBackPressed() {
        if (hasDrawer) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (!viewIsAtHome) { //if the current view is not the Home fragment
                    displayView(R.id.nav_home); //display the Home fragment
                } else {
                    moveTaskToBack(true);  //If view is in Home fragment, exit application
                }
            }
        }
        else {
            super.onBackPressed();
        }
    }
}
