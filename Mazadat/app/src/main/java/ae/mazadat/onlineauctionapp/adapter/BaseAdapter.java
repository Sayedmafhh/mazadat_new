package ae.mazadat.onlineauctionapp.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewPropertyAnimator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.viewholder.ItemViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MZItem} and makes a call to the
 * specified {@link ItemsListFragment.OnMZItemInteractionListener}.
 */
public abstract class BaseAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    protected String auctionType = "";

    protected Context mContext;
    protected final List<MZItem> mValues;
    protected final List<ItemViewHolder> lstHolders;
    protected List<ItemViewHolder> lstToRemove;

    Timer tmr;
    protected Handler mHandler = new Handler();
    protected Runnable updateRemainingTimeRunnable = new Runnable() {
        @Override
        public void run() {
            synchronized (lstHolders) {
                //                    long ms = System.currentTimeMillis();
                boolean itemToRemove = false;
                // Reset
                lstToRemove = new ArrayList<>();
                for (ItemViewHolder holder : lstHolders) {
                    if (holder.isRemoved) {
                        int position = holder.getAdapterPosition();
                        if (position > -1) {
                            lstToRemove.add(holder);
//                            lstHolders.remove(position);
//                            break;
                        }
                    }
                    if (holder.isExpired && !holder.isRemoved && !itemToRemove) {
                        if (holder.getAdapterPosition() > -1) {
                            remove(holder);
                            itemToRemove = true; // Remove one item / second
                        }
                    }

                    holder.updateTimeRemaining();
                }

                lstHolders.removeAll(lstToRemove);
            }
        }
    };


    protected boolean timerInitiated = false;

    protected final ItemsListFragment.OnMZItemInteractionListener mListener;

    public BaseAdapter(Context context, ItemsListFragment.OnMZItemInteractionListener listener) {
        mValues   = new ArrayList<>();
        mListener = listener;
        mContext  = context;
        lstHolders = new ArrayList<>();
//        IMPORTANT: Do not activate here startUpdateTimer();
    }


    public void startUpdateTimer() {

        this.tmr = new Timer();
        tmr.schedule(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(updateRemainingTimeRunnable);
            }
        }, 1000, 1000);
    }

    public void cancelUpdateTimer() {
        this.tmr.cancel();
    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public List<MZItem> getItems() {
        return mValues;
    }

    public void add(MZItem model) {
        mValues.add(model);
        notifyItemInserted(mValues.size() - 1);
    }

    public void remove(ItemViewHolder holder) {
        int position = holder.getAdapterPosition();
        if (position >= 0) {
            holder.isRemoved = true;
            mValues.remove(position);
            notifyItemRemoved(position);
        }
    }


    public void update(int position, MZItem item) {
        if (position >= 0) {
            mValues.remove(position);
            mValues.add(position, item);
            notifyItemChanged(position);
        }
    }

    public void clear() {
        mValues.clear();
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(String.valueOf(mValues.get(position).getId()));
    }

    public void blink(final ItemViewHolder holder, final int timeNo) {
        final View v = holder.vTag;
        Log.i("BlinkBaseAdapter", "Blink in base adapter");
        ViewPropertyAnimator an = v.animate().setDuration(100).alpha(1);

        final int redColor;
        final int normalColor;
        if (auctionType.contentEquals(AGConf.KEY_AUCTION_GRID)) {
            redColor    = R.color.colorItemRedTrans;
            normalColor = R.color.black_overlay;
        } else {
            redColor    = R.color.colorItemRed;
            normalColor = R.color.colorItemNormal;
        }

        if (timeNo < 8) {
            an.withEndAction(new Runnable() {
                @Override
                public void run() {
                    v.setBackgroundResource(redColor);
                    v.animate().setDuration(100).alpha(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            blink(holder, timeNo+1);
                        }
                    });
                }
            });

        } else {
            v.setBackgroundResource(normalColor);
            // No more bid blink
            holder.mItem.setBided(false);
        }
    }

    public void blink(final ItemViewHolder holder) {
        Log.i("Blink", "Blinking");
        holder.blinkOnce(holder.vTag, false);
        holder.blinkOnce(holder.rtC1, true);
        holder.blinkOnce(holder.rtC2, true);
        holder.blinkOnce(holder.rtC3, true);
    }

}
