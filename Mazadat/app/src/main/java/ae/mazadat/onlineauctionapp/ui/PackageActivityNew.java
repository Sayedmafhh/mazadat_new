package ae.mazadat.onlineauctionapp.ui;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import org.parceler.Parcels;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sudhirkumar on 4/10/2017.
 */

public class PackageActivityNew extends Activity {
    MZItem mItem;
    String mType;
    @Bind(R.id.backBtn)
    ImageView backBtn;
    @Bind(R.id.tvPkg)
    TextView tvPkg;
    @Bind(R.id.webview)
    WebView webview;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_benefits);
        ButterKnife.bind(this);
        Parcelable data = getIntent().getParcelableExtra(AGConf.KEY_ITEM_DATA);
        mItem = Parcels.unwrap(data);
        if (mItem!=null){
            System.out.println("*******not null");
        }else {
            System.out.println("*******null");
        }
        SessionManager session = App.getSessionManager();

        webview.setBackgroundColor(Color.TRANSPARENT);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvPkg.setText(mItem.getDuPackageName());
        webview.setBackgroundColor(Color.TRANSPARENT);


        String dir = session.getMZLang().contentEquals("ae") ? "rtl" : "ltr";
        String html = "<body dir=\"" + dir + "\">";
        html += mItem.getDuPackageDetails();
        html += "</body>";

        webview.loadData(html, "text/html; charset=UTF-8", null);

    }
}
