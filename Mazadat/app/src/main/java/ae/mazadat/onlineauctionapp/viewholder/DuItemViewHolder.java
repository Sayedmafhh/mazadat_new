package ae.mazadat.onlineauctionapp.viewholder;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ae.mazadat.onlineauctionapp.R;
import butterknife.Bind;

/**
 * Created by agile on 4/1/16.
 */
public class DuItemViewHolder extends ItemViewHolder {

    @Bind(R.id.tvPrefix) public TextView tvPrefix;
    @Bind(R.id.tvNumber) public TextView tvNumber;

    @Nullable
    @Bind(R.id.price_counter)
    public LinearLayout priceCounter;

    @Nullable
    @Bind(R.id.ll_bid_date_time)
    public LinearLayout llBidDateTime;

    @Nullable
    @Bind(R.id.txtview_bid_date_time)
    public TextView txtViewBidDateTime;

    public DuItemViewHolder(View view) {
        super(view);
    }

    @Nullable
    @Bind(R.id.tvPackage) public TextView tvPackage;

//    @Bind(R.id.ivNumber) public ImageView ivNumber;
}
