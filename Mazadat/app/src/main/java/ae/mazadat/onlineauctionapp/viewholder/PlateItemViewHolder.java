package ae.mazadat.onlineauctionapp.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ae.mazadat.onlineauctionapp.R;
import butterknife.Bind;

/**
 * Created by agile on 4/1/16.
 */
public class PlateItemViewHolder extends ItemViewHolder {

    @Bind(R.id.tvPltL) public TextView tvPltL;
    @Bind(R.id.tvPltR) public TextView tvPltR;

//    @Bind(R.id.timer) public IconTextView timer;

    public PlateItemViewHolder(View view) {
        super(view);
    }

    @Bind(R.id.tvMotocycle)
    public TextView tvMotocycle;

    //    @Bind(R.id.min_bid) public TextView mMinBid;

//    @Bind(R.id.tvDay) public TextView tvDay;
//    @Bind(R.id.tvHr)  public TextView tvHr;
//    @Bind(R.id.tvMin) public TextView tvMin;
//    @Bind(R.id.tvSec) public TextView tvSec;

    // Plate related
    @Bind(R.id.ivPlate) public ImageView ivPlate;
//    @Bind(R.id.date) public TextView mDate;

//    @Bind(R.id.btnBid) public Button mBtnBid;

}
