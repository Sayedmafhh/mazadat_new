package ae.mazadat.onlineauctionapp.ui.auction;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.CarsAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.ui.GridFragment;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link ItemsListFragment.OnMZItemInteractionListener}
 * interface.
 */
public class CarsFragment extends GridFragment {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CarsFragment() {
    }

    Call<MZResponse> getDetailsItems;
    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CarsFragment newInstance(int columnCount) {
        CarsFragment fragment = new CarsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_base_list, container, false);

        setHasFab(true);
        setHasLoadMore(true);

        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(false);

        init(getActivity());

        adapter = new CarsAdapter(this,getActivity(), mListener);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));

        iniFirebase(AGConf.FIREBASE_CARS_URL);

        if (savedInstanceState == null) {
            loadItems();
        }

        return rootView;
    }
    public void _loadTabs( final String auctionType, final MZItem item) {


        api = App.getRestAdapter().create(AGApi.class);






        // Get details  for this item
        Map<String, String> params = new HashMap<>();
        params.put("lang", session.getMZLang());
        params.put("category", String.valueOf(item.getCategory()));
        params.put("item_id", String.valueOf(item.getId()));




        getDetailsItems = api.getItem(params);

        getDetailsItems.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {
                if (response.isSuccessful()) {

                    MZResponse MZResponse = response.body();
                    //   mItem = response.body();
                    List<MZItem> items = MZResponse.getItems();
                    // mItem =;
                   // openItemDetails(sourceActivity, auctionType, items.get(0));
                    mListener.onMZItemInteraction(items.get(0), auctionType);
                //    doWhenSuccessLoad(reset, items);


                } else {
                    Toast.makeText(getActivity(), getString(R.string.err_cant_get_deposit), Toast.LENGTH_LONG).show();
                }

                // _initTabs();
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void serviceLoadItems(final boolean reset) {
        super.serviceLoadItems(reset);

        filterParams.put("lang", session.getMZLang());
        filterParams.put("limit", String.valueOf(ITEMS_PER_PAGE));
        filterParams.put("offset", String.valueOf(currentPage * ITEMS_PER_PAGE));
       // filterParams.put("development", String.valueOf(1));

        gettingItemsCall = api.getCars(filterParams);
        System.out.println("cars fragments "+filterParams.toString());
        gettingItemsCall.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {

                if (response.isSuccessful()) {
                    MZResponse MZResponse = response.body();
                    Log.i("Is Online Bid", "Is online bid: " + (MZResponse.isActivate_bid() ? "YES" : "NO") );
                    Date currentTime = MZResponse.getCurrentTime();
                    long timeDiff;
                    if (currentTime != null) {
                        timeDiff = System.currentTimeMillis() - currentTime.getTime();
                    } else {
                        timeDiff = 0;
                    }

                    int deposit      = MZResponse.getUserDeposit();
                    List<MZItem> items = MZResponse.getItems();

                    mFilters = MZResponse.getFilters();
                    mSorts   = MZResponse.getSorts();

                    // TODO: When failed - this may throw an error!

                    _initFiltersAndSorts();

                    doWhenSuccessLoad(reset, items);

                    session.setCarsDeposit(deposit);

                    if (items != null && items.size() > 0) {
                        for (int i=0; i<items.size(); i++) {
                            MZItem item = items.get(i);
                            item.setOnlineAuction(!(MZResponse.isActivate_bid()));
//                            item.setUserDeposit(deposit);
                            item.setCurrentTime(currentTime);
                            item.setTimeDiff(timeDiff);
                            if (session.getUid() > 0 && item.getBidder() != null && item.getBidder().contentEquals(session.getUid() + "")) {
                                item.setMyBid(true);
                            }
                            adapter.add(item);
                        }
                    } else {
                        initNoItems();
                    }
                } else {
                    doWhenFailureApiCall("Opps!", "Unable to load items!");
                }
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {
            }
        });

    }

    @Override
    protected void registerPN() {
        regParams.put("category", String.valueOf(AGConf.CAT_CARS));
        _registerPN();
    }
}
