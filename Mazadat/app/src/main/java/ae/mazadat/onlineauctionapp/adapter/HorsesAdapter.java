package ae.mazadat.onlineauctionapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.MZItemDetails;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.viewholder.GridItemViewHolder;
import ae.mazadat.onlineauctionapp.viewholder.ItemViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MZItem} and makes a call to the
 * specified {@link ItemsListFragment.OnMZItemInteractionListener}.
 */
public class HorsesAdapter extends BaseAdapter {

    public HorsesAdapter(Context context, ItemsListFragment.OnMZItemInteractionListener listener) {
        super(context, listener);
        auctionType = AGConf.KEY_AUCTION_GRID;
    }


    @Override
    public int getItemViewType(int position) {
        return mValues.get(position).getCity();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_grid, parent, false);

        final GridItemViewHolder vh = new GridItemViewHolder(view);
        return vh;
    }

    public void onViewRecycled(ItemViewHolder vh) {
        super.onViewRecycled(vh);

        final GridItemViewHolder holder = (GridItemViewHolder) vh;
        holder.vTag.setBackgroundResource(R.color.black_overlay);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder vh, int position) {

        final GridItemViewHolder holder = (GridItemViewHolder) vh;

        // Reset style
        holder.vTag.setBackgroundColor(mContext.getResources().getColor(R.color.black_overlay));
        holder.vTag.setAlpha(1);

        holder.mItem = mValues.get(position);

        synchronized (lstHolders) {
            lstHolders.add(holder);
        }

        MZItemDetails details = holder.mItem.getDetails();
        if (details != null) {

            if (details.getImages() != null && details.getImages().size() > 0) {
                // Show BG
                if (details.getFeaturedImage() != null) {
                    String src = details.getFeaturedImage().get(0).getThump();
                    String f = src.replace("https", "http"); // TODO: Remove
                    Glide.with(mContext).load(f).centerCrop().placeholder(R.drawable.ic_horses).into(holder.ivBg);
                }
            }

            holder.tvPrice.setText(String.format("%,d", holder.mItem.getPrice()));
            holder.tvBids.setText(holder.mItem.getBids() + "");

            String title = "";

            if (details.getName() != null) {
                title = details.getName();
            }

            holder.tvModel.setText(title);
        }

        if (holder.mItem.isMyBid()) {
            holder.vTag.setBackgroundResource(R.color.colorItemGreenTrans);
        } else {
            if (holder.mItem.isBided()) {
                blink(holder, 0);
            }
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onMZItemInteraction(holder.mItem, AGConf.KEY_HORSES_AUCTION);
                }
            }
        });

        holder.updateTimeRemaining();
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(String.valueOf(mValues.get(position).getId()));
    }

}
