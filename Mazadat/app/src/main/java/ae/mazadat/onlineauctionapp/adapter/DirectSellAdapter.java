package ae.mazadat.onlineauctionapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.MZItemDetails;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.util.MZUtil;
import ae.mazadat.onlineauctionapp.viewholder.ItemViewHolder;
import ae.mazadat.onlineauctionapp.viewholder.SellItemViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MZItem} and makes a call to the
 * specified {@link ItemsListFragment.OnMZItemInteractionListener}.
 */
public class DirectSellAdapter extends BaseAdapter {

    public DirectSellAdapter(Context context, ItemsListFragment.OnMZItemInteractionListener listener) {
        super(context, listener);
        auctionType = AGConf.KEY_AUCTION_GRID;
    }

    @Override
    public int getItemViewType(int position) {
        return mValues.get(position).getCity();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_plate_sell, parent, false);

        final SellItemViewHolder vh = new SellItemViewHolder(view);
        return vh;
    }


    @Override
    public void onBindViewHolder(final ItemViewHolder vh, int position) {

        final SellItemViewHolder holder = (SellItemViewHolder) vh;
        holder.mItem = mValues.get(position);

        synchronized (lstHolders) {
            lstHolders.add(holder);
        }

        holder.ivPlate.setImageDrawable(null);
        holder.tvSoldOut.setVisibility(View.GONE);
        holder.btnBuy.setVisibility(View.VISIBLE);
        holder.vPrice.setVisibility(View.VISIBLE);
        holder.tvMotocycle.setVisibility(View.GONE);
//        holder.ivSoldOut.setVisibility(View.GONE);

        MZItemDetails details = holder.mItem.getDetails();
        if (details != null && details.getPlateLetter() != null && details.getNumber() != null) {

            String type = holder.mItem.getDetails().getPlateType();

            if (holder.mItem.getCity() == 7) { // Dubai
                if (type.contentEquals("3")) {
                    holder.tvPltR.setTextColor(mContext.getResources().getColor(R.color.white));
                    holder.tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                } else if (type.contentEquals("2")) {
                    holder.tvPltR.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryText));
                    holder.tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                } else {
                    holder.tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    holder.tvPltR.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryText));
                }
            } else {
                if (type.contentEquals("2")) { // Motocycle
                    holder.tvMotocycle.setVisibility(View.VISIBLE);
                } else {
                    holder.tvMotocycle.setVisibility(View.GONE);
                }

                holder.tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                holder.tvPltR.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryText));
            }

            holder.ivPlate.setImageDrawable(MZUtil.getPlateDrawable(holder.mItem.getCity(), type));

//            if (holder.mItem.getCity() == 10) { // Abu Dhabi
//                holder.tvPltL.setTextColor(mContext.getResources().getColor(R.color.white));
//            } else {
//                holder.tvPltL.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryText));
//            }

            holder.tvPltL.setText(holder.mItem.getDetails().getPlateLetter());
            holder.tvPltR.setText(holder.mItem.getDetails().getNumber());
        }

        if (holder.mItem.getSoldBy() != null && !holder.mItem.getSoldBy().contentEquals("0")) {
//            holder.ivSoldOut.setVisibility(View.VISIBLE);
            holder.tvSoldOut.setVisibility(View.VISIBLE);
            holder.vPrice.setVisibility(View.GONE);
            holder.btnBuy.setVisibility(View.GONE);
        }

        holder.mPrice.setText(String.format("%,d", holder.mItem.getSellPrice()));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onMZItemInteraction(holder.mItem, AGConf.KEY_DIRECT_SELL);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(String.valueOf(mValues.get(position).getId()));
    }

}
