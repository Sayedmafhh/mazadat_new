package ae.mazadat.onlineauctionapp.ui;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.rest.DashboardItem;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGApiResponse;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class InitActivity extends AppCompatActivity {

    AGApi api;
    Call<List<DashboardItem>> getDashboardCall;
    Call<AGApiResponse> checkUpdateCall;

    SessionManager session = App.getSessionManager();

    String lang = "";

    @Bind(R.id.ivSplash)
    ImageView ivSplash;

    @Bind(R.id.vSplash)
    View v;

    @Bind(R.id.tvVersion)
    TextView tvVersion;

    String version = "";

    Thread timer;

    boolean updateIsRequired = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        lang = session.getMZLang();

        String iLang = getIntent().getStringExtra(AGConf.KEY_LANG);
        if (iLang != null) {
            lang = iLang;
        }

        version = getResources().getString(R.string.version_name);
        PackageInfo pInfo = null;
        try {
            String versionName = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
            tvVersion.setText(versionName);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Glide.with(this)
                .load(R.drawable.splash)
                .asGif()
//                .crossFade(1000)
                .into(ivSplash);

        api = App.getRestAdapter().create(AGApi.class);

        getDashboardItems();
    }

    private void checkUpdateIsRequired() {
        Map<String, String> params = new HashMap<>();
        params.put("platform", "android");
        params.put("version",  version);
        checkUpdateCall = api.checkUpdateIsRequired(params);
        checkUpdateCall.enqueue(new Callback<AGApiResponse>() {
            @Override
            public void onResponse(Call<AGApiResponse> call, Response<AGApiResponse> response) {
                if (response.isSuccessful()) {
                    AGApiResponse res = response.body();
//                    updateIsRequired = res.isNeedUpdate() == 1;
                    showSplash();
                } else {
                    showSplash();
                }
            }

            @Override
            public void onFailure(Call<AGApiResponse> call, Throwable t) {
                showSplash();
            }
        });


    }


    private void showSplash() {
        timer = new Thread()
        {
            public void run()
            {
                try {
                    sleep(1700);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    openApp();
                }
            }
        };

        timer.start();
    }

    private void getDashboardItems() {

        getDashboardCall = api.getDashboard(lang);
        getDashboardCall.enqueue(new AGRetrofitCallback<List<DashboardItem>>() {
            @Override
            public void onResponse(Call<List<DashboardItem>> call, Response<List<DashboardItem>> response) {
                super.onResponse(call, response);

                if (response.isSuccessful()) {
                    List<DashboardItem> items = response.body();

                    session.saveDashboard(items);
                    checkUpdateIsRequired();

                } else {
                    if (session.getDashboard() == null) {
                        // Show this message for the first time, otherwise enable OFFLINE access to app
                        AGUtil.errorAlert(InitActivity.this, "Opps!", getString(R.string.err_first_launch));
                    } else {
                        checkUpdateIsRequired();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<DashboardItem>> call, Throwable t) {
                super.onFailure(call, t);

                if (session.getDashboard() == null) {
                    // Show this message for the first time, otherwise enable OFFLINE access to app
                    AGUtil.errorAlert(InitActivity.this, "Opps!", getString(R.string.err_first_launch));
                } else {
                    checkUpdateIsRequired();
                }
            }
        });
    }

    private void openApp() {
//        Intent i = new Intent(InitActivity.this, MainActivity.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        InitActivity.this.startActivity(i);

        if (updateIsRequired) {

            Runnable showUpdateAlert = new Runnable() {
                @Override
                public void run() {
                    new SweetAlertDialog(InitActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getString(R.string.app_update))
                            .setContentText(getString(R.string.app_update_msg))
                            .setConfirmText(getString(R.string.update))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog dialog) {
                                    dialog.dismiss();
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                }
                            })
//                            .setCancelable(false)
                            .show();
                }
            };
            runOnUiThread(showUpdateAlert);
            return;
        }


        int[] startingLocation = new int[2];
        v.getLocationOnScreen(startingLocation);
        startingLocation[0] += v.getWidth() / 2;
        MainActivity.startFromLocation(startingLocation, this);
//        overridePendingTransition(0, 0);
//        finish();
    }
}
