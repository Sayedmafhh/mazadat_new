package ae.mazadat.onlineauctionapp.ui.services;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import org.parceler.Parcels;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.gallery.PhotosPagerAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.MZItemDetails;
import ae.mazadat.onlineauctionapp.model.fb.FBItem;
import ae.mazadat.onlineauctionapp.model.rest.Deposit;
import ae.mazadat.onlineauctionapp.ui.BaseActivity;
import ae.mazadat.onlineauctionapp.ui.details.tab.BidFragment;
import ae.mazadat.onlineauctionapp.ui.details.tab.DepositFragment;
import ae.mazadat.onlineauctionapp.ui.details.tab.DetailsFragment;
import ae.mazadat.onlineauctionapp.ui.details.tab.LocationFragment;
import ae.mazadat.onlineauctionapp.ui.details.tab.NotesFragment;
import ae.mazadat.onlineauctionapp.ui.details.tab.PackageFragment;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.MZUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class ItemDetailsFragment_new extends Fragment {

    AGApi api;
    Call<Deposit> getDepositCall;

    protected Firebase fbRef;

    @Bind(R.id.date)
    TextView date;

    @Bind(R.id.bids)
    TextView mBids;

    // Plates
    @Nullable
    @Bind(R.id.tvPltL)
    TextView tvPltL;

    @Nullable
    @Bind(R.id.tvPltR)
    TextView tvPltR;

    @Nullable
    @Bind(R.id.ivPlate)
    ImageView ivPlate;

    // Du
    @Nullable
    @Bind(R.id.tvPrefix)
    TextView tvPrefix;

    @Nullable
    @Bind(R.id.tvNumber)
    TextView tvNumber;

    @Nullable
    @Bind(R.id.tvMotocycle)
    TextView tvMotocycle;

    MZItem mItem;
    Deposit deposit; // API Call

    SessionManager session = App.getSessionManager();
    @Nullable
    @Bind(R.id.pkgBenefitBtn)
    TextView pkgBenefitBtn;
    @Nullable
    @Bind(R.id.ivNumber)
    ImageView ivNumber;
    //    @Bind(R.id.tvPrefix)
//    TextView tvPrefix;
    @Nullable
    @Bind(R.id.duNumber)
    RelativeLayout duNumber;
    @Nullable
    @Bind(R.id.tvLblBids)
    TextView tvLblBids;
    @Nullable
    @Bind(R.id.tvLblExpiry)
    TextView tvLblExpiry;
    @Nullable
    @Bind(R.id.vHr)
    FrameLayout vHr;
    @Nullable
    @Bind(R.id.tvPkgName)
    TextView tvPkgName;
    @Nullable
    @Bind(R.id.layoutPkgBenefits)
    LinearLayout layoutPkgBenefits;

    @Nullable
    @Bind(R.id.tvPrice)
    TextView tvPrice;
    @Nullable
    @Bind(R.id.layoutPrice)
    LinearLayout layoutPrice;

    @Nullable
    @Bind(R.id.tvNotes)
    TextView tvNotes;
    @Nullable
    @Bind(R.id.layoutNotes)
    LinearLayout layoutNotes;

    @Nullable
    @Bind(R.id.tvCartSubTotal)
    TextView tvCartSubTotal;
    @Nullable
    @Bind(R.id.layoutcartSubtotal)
    LinearLayout layoutcartSubtotal;

    @Nullable
    @Bind(R.id.RLPkgBenefit)
    RelativeLayout RLPkgBenefit;
    @Nullable
    @Bind(R.id.buyNowBtn)
    Button buyNowBtn;


    /* Tabs */
    private TabsAdapter mTabsAdapter;

    @Bind(R.id.pager)
    ViewPager mViewPager;

    @Bind(R.id.tabs)
    TabLayout tabLayout;

    // Cars
    @Nullable
    @Bind(R.id.picPager)
    ViewPager picPager;

    @Nullable
    @Bind(R.id.indicator)
    CircleIndicator indicator;


    private String currentAuction = AGConf.KEY_PLATES_AUCTION;
    private String fbURL = "";

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailsFragment_new() {
    }


    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ItemDetailsFragment_new newInstance(int columnCount) {
        ItemDetailsFragment_new fragment = new ItemDetailsFragment_new();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getDepositCall != null) {
            getDepositCall.cancel();
        }
        ButterKnife.unbind(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            Parcelable data = getArguments().getParcelable(AGConf.KEY_ITEM_DATA);
            if (data != null) {
                mItem = Parcels.unwrap(data);
            }

            String auction = getArguments().getString(AGConf.KEY_AUCTION_TYPE);
            if (auction != null) {
                currentAuction = auction;
            }
        }

    }

    protected void iniFirebase(String url) {
        fbRef = new Firebase(url);
        fbRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                FBItem item = dataSnapshot.getValue(FBItem.class);

                if (item.getId() == mItem.getId()) {
                    // This is the changed item
                    updateItem(mItem, item);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    protected void updateItem(MZItem mzItem, FBItem item) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 2016-06-14 16:00:00

        Date end_time = new Date();
        try {
            end_time = format.parse(item.getEnd_time());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!end_time.toString().contentEquals(mzItem.getEndTime().toString())) {

            mzItem.setEndTime(end_time); // TODO: SET properly

            mItem = mzItem;

            if (getActivity() != null) {

                date.setText(AGUtil.formatMazad(mItem.getEndTime()));
            }
        }

        if (item.getBids() != mzItem.getBids()) {
            AGUtil.bid(getActivity());
            mzItem.setPrice(Integer.parseInt(item.getCurrent_price()));
            mzItem.setBids(item.getBids());

            mzItem.setBided(true);

            boolean mMyBid = false;
            if (session.getUid() > 0 && item.getUser().contentEquals(session.getUid() + "")) {
                mzItem.setMyBid(true);
                mMyBid = true;
            } else {
                mzItem.setMyBid(false);
            }

            mItem = mzItem;

            if (getActivity() != null) {

                // is it me ?
//                boolean isMyBid = ((ItemDetailsActivity)getActivity()).isMyBid;

                if (mMyBid) {
                    MZUtil.doWhenSuccessBid(getActivity(), getString(R.string.bid_success_title));
                } else {
                    // Update UI, Hide bid button if I'm the bidder
                    mBids.setText(getActivity().getString(R.string.bids_no, mzItem.getBids()));
                    date.setText(AGUtil.formatMazad(mItem.getEndTime()));
                    _initTabs();
                }
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView;

        String title = "";

        MZItemDetails details = mItem.getDetails();


        if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_du_details_new, container, false);
            } else {
                rootView = inflater.inflate(R.layout.fragment_du_details, container, false);
            }

            fbURL = AGConf.FIREBASE_DU_URL;

            if (details.getName() != null) {
                title = details.getName();

            }
        } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {
            /*if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_grid_details_new, container, false);
            } else {
            }*/
            rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            fbURL = AGConf.FIREBASE_CARS_URL;

            if (details.getMake() != null && details.getModel() != null) {
                title += details.getMake() + " " + details.getModel();
            }

            if (details.getYear() != null) {
                title += " " + details.getYear();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_du_details_new, container, false);
            } else {
                rootView = inflater.inflate(R.layout.fragment_plate_details, container, false);

            }
            fbURL = AGConf.FIREBASE_PLATES_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_BAGS_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_du_details_new, container, false);
            } else {

                rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            }
            fbURL = AGConf.FIREBASE_BAGS_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_du_details_new, container, false);
            } else {

                rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            }
            fbURL = AGConf.FIREBASE_JEWELLERY_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_PROP_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_du_details_new, container, false);
            } else {

                rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            }
            fbURL = AGConf.FIREBASE_PROPERTIES_URL;

            if (details.getName() != null) {
                title = details.getName();
            }

        } else if (currentAuction.contentEquals(AGConf.KEY_MISC_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_du_details_new, container, false);
            } else {

                rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            }
            fbURL = AGConf.FIREBASE_MIX_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_HORSES_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_du_details_new, container, false);
            } else {

                rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            }
            fbURL = AGConf.FIREBASE_HORSES_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_COINS_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_du_details_new, container, false);
            } else {

                rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            }
            fbURL = AGConf.FIREBASE_COINS_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else {
            rootView = null;
        }

        ButterKnife.bind(this, rootView);

        getActivity().setTitle(title);

        mTabsAdapter = new TabsAdapter(getActivity().getSupportFragmentManager());

        if (session.isLoggedIn()) {
            _loadDeposit();
        } else {
            _initTabs();
        }

        mBids.setText(getActivity().getString(R.string.bids_no, mItem.getBids()));


        if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
            _setDu(details);
        } else if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
            _setPlate(details);
        }

        date.setText(AGUtil.formatMazad(mItem.getEndTime()));

        iniFirebase(fbURL);

        // Grid
        if (picPager != null && indicator != null) {
            picPager.setAdapter(new PhotosPagerAdapter(getActivity(), mItem.getDetails().getImages()));
            indicator.setViewPager(picPager);
        }
        if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                setDUData();
            }
        }
        return rootView;
    }

    private void setCarData() {
    }

    private void setDUData() {
        tvPkgName.setText(mItem.getDuPackageName());
        tvPrice.setText("AED " + mItem.getPrice());
        tvCartSubTotal.setText("AED ");
        if (mItem.getDetails().getNotes().equals("") && mItem.getDetails().getNotes().length() > 0) {
            tvNotes.setText(Html.fromHtml(mItem.getDetails().getNotes()));
        }
        pkgBenefitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                PackageFragment.newInstance(mItem, currentAuction);
            }
        });

    }

    private void _loadDeposit() {
        api = App.getRestAdapter().create(AGApi.class);

        // Get deposit for this item
        Map<String, String> params = new HashMap<>();
        params.put("lang", session.getMZLang());
        params.put("item", String.valueOf(mItem.getId()));
        getDepositCall = api.getDeposit(params);

        getDepositCall.enqueue(new AGRetrofitCallback<Deposit>() {
            @Override
            public void onResponse(Call<Deposit> call, Response<Deposit> response) {
                if (response.isSuccessful()) {
                    deposit = response.body();

                    setHasOptionsMenu(true);

                    int userDeposit = deposit.getUserDeposit();

                    if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
                        // Update plates deposit
                        session.setPlatesDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
                        session.setDuDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {
                        session.setCarsDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_BAGS_AUCTION)) {
                        session.setBagsDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_COINS_AUCTION)) {
                        session.setCoinsDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_HORSES_AUCTION)) {
                        session.setHorsesDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_PROP_AUCTION)) {
                        session.setPropDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_MISC_AUCTION)) {
                        session.setMixDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
                        session.setWatchesDeposit(userDeposit);
                    }

                } else {
                    Toast.makeText(getActivity(), getString(R.string.err_cant_get_deposit), Toast.LENGTH_LONG).show();
                }

                _initTabs();
            }

            @Override
            public void onFailure(Call<Deposit> call, Throwable t) {
                if (isAdded() && getActivity() != null) {
                    Toast.makeText(getActivity(), getString(R.string.err_cant_get_deposit), Toast.LENGTH_LONG).show();
                    _initTabs();
                }
            }
        });
    }

    private void _initTabs() {

    }


    private void _setDu(MZItemDetails details) {
        if (details != null && details.getNumber() != null) {
            String number = details.getNumber();
            tvPrefix.setText(number.substring(0, 3));
            tvNumber.setText(number.substring(3));
        }
    }


    private void _setPlate(MZItemDetails details) {
        if (details != null && details.getPlateLetter() != null && details.getNumber() != null) {

            String type = mItem.getDetails().getPlateType();

            ivPlate.setImageDrawable(MZUtil.getPlateDrawable(mItem.getCity(), type));

            if (mItem.getCity() == 7) { // Dubai
                if (type.contentEquals("3")) {
                    tvPltR.setTextColor(getActivity().getResources().getColor(R.color.white));
                    tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                } else if (type.contentEquals("2")) {
                    tvPltR.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
                    tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                } else {
                    tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    tvPltR.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
                }
            } else {
                if (type.contentEquals("2")) { // Motocycle
                    tvMotocycle.setVisibility(View.VISIBLE);
                } else {
                    tvMotocycle.setVisibility(View.GONE);
                }

                tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                tvPltR.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
            }

            tvPltL.setText(mItem.getDetails().getPlateLetter());
            tvPltR.setText(mItem.getDetails().getNumber());
        }
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class TabsAdapter extends FragmentPagerAdapter {

        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            Fragment frag;

            if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
                frag = _getPlateItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {
//                if(mItem.getBuyNow().equalsIgnoreCase("1")){
//                    frag = _getCarItemFrags_directSell(position);
//                }else{
//                    frag = _getCarItemFrags(position);
//                }


                frag = _getCarItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_BAGS_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_COINS_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_PROP_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_MISC_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_HORSES_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
                frag = _getDuItemFrags(position);
            } else {
                frag = _getOtherItemFrags(position);
            }

            return frag;
        }

        @Override
        public int getCount() {
            return getTabsCount();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getTabTitle(position);
        }
    }

    private int getTabsCount() {
        if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
            return 3;
        } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {
            return 5;
        } else {
            return 4;
        }
    }

    private String getTabTitle(int position) {
        String title;

        if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
            title = _getPlateTabTitles(position);
        } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {
//            if (mItem.getBuyNow().equals("1")) {
//                title = _getCarsTabTitles_directSell(position);
//            } else {
//            }
            title = _getCarsTabTitles(position);
        } else if (currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
            title = _getWatchesTabTitles(position);
        } else if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
            title = _getDuTabTitles(position);
        } else {
            title = _getOthersTabTitles(position);
        }

        return title;

        // TODO: Handle difference cases
    }

    private String _getPlateTabTitles(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_bid);
            case 1:
                return getString(R.string.tab_notes);
            case 2:
                return getString(R.string.tab_deposit);
        }
        return null;
    }

    private String _getDuTabTitles(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_bid);
            case 1:
                return getString(R.string.tab_package);
            case 2:
                return getString(R.string.tab_notes);
            case 3:
                return getString(R.string.tab_deposit);
        }
        return null;
    }

    private String _getCarsTabTitles(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_bid);
            case 1:
                return getString(R.string.tab_details);
            case 2:
                return getString(R.string.tab_notes);
            case 3:
                return getString(R.string.tab_location);
            case 4:
                return getString(R.string.tab_deposit);
        }
        return null;
    }

    private String _getCarsTabTitles_directSell(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_details);
            case 1:
                return getString(R.string.tab_notes);
            case 2:
                return getString(R.string.tab_location);

        }
        return null;
    }

    private String _getWatchesTabTitles(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_bid);
            case 1:
                return getString(R.string.tab_details);
            case 2:
                return getString(R.string.tab_notes);
            case 3:
                return getString(R.string.tab_deposit);
        }
        return null;
    }

    private String _getOthersTabTitles(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_bid);
            case 1:
                return getString(R.string.tab_details);
            case 2:
                return getString(R.string.tab_notes);
            case 3:
                return getString(R.string.tab_deposit);
        }
        return null;
    }


    private Fragment _getPlateItemFrags(int position) {
        switch (position) {
            case 0:
                return BidFragment.newInstance(mItem, currentAuction, deposit);
            case 1:
                return NotesFragment.newInstance(mItem, currentAuction);
            case 2:
                return DepositFragment.newInstance(mItem, deposit);
        }
        return null;
    }

    private Fragment _getDuItemFrags(int position) {
        switch (position) {
            case 0:
                return BidFragment.newInstance(mItem, currentAuction, deposit);
            case 1:
                return PackageFragment.newInstance(mItem, currentAuction);
            case 2:
                return DetailsFragment.newInstance(mItem, currentAuction);
            case 3:
                return DepositFragment.newInstance(mItem, deposit);
        }
        return null;
    }

    private Fragment _getCarItemFrags(int position) {
        switch (position) {
            case 0:
                return BidFragment.newInstance(mItem, currentAuction, deposit);
            case 1:
                return DetailsFragment.newInstance(mItem, currentAuction);
            case 2:
                return NotesFragment.newInstance(mItem, currentAuction);
            case 3:
                return LocationFragment.newInstance(mItem, currentAuction);
            case 4:
                return DepositFragment.newInstance(mItem, deposit);
        }
        return null;
    }
    private Fragment _getCarItemFrags_directSell(int position) {
        switch (position) {

            case 0:
                return DetailsFragment.newInstance(mItem, currentAuction);
            case 1:
                return NotesFragment.newInstance(mItem, currentAuction);
            case 2:
                return LocationFragment.newInstance(mItem, currentAuction);

        }
        return null;
    }

    private Fragment _getOtherItemFrags(int position) {
        switch (position) {
            case 0:
                return BidFragment.newInstance(mItem, currentAuction, deposit);
            case 1:
                return DetailsFragment.newInstance(mItem, currentAuction);
            case 2:
                return NotesFragment.newInstance(mItem, currentAuction);
            case 3:
                return DepositFragment.newInstance(mItem, deposit);
        }
        return null;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.item_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.act_payment:
                BaseActivity.openPayment(getActivity(), currentAuction);
                return true;
        }
        return false;
    }

}
