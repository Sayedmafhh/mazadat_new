package ae.mazadat.onlineauctionapp.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

/**
 * Created by agile on 3/31/16.
 */

@Parcel
public class MZItem {

    @SerializedName("id")
    public int id;

    @SerializedName("category")
    public int category;

    @SerializedName("end_time")
    Date endTime;

    @SerializedName("city")
    public int city;

    @Nullable
    @SerializedName("type")
    String type;

    @Nullable
    @SerializedName("max_bider")
    String bidder;

//    @SerializedName("start_bid")
//    Date startBid;

    MZItemDetails details;

    // From main
    Date currentTime;
    long timeDiff;

//    int userDeposit;

    // Animation when bid
    boolean isBided;
    // Am I the last bidder?
    boolean isMyBid = false;

    boolean isOnlineAuction = false;

    // Deposit
    @SerializedName("deposit_type")
    public int depositType;

    @SerializedName("deposit")
    public int deposit;

    @SerializedName("min_bid")
    public int minBid; // Min Bid

    @SerializedName("max_bid")
    public long price; // Price


    // Direct Sel
    @SerializedName("desire_bid")
    public int sellPrice; // Price

    @Nullable
    @SerializedName("book")
    int booked;

    @Nullable
    @SerializedName("won_by")
    String soldBy;


    @Nullable
    @SerializedName("buy_now")
    String buyNow;

    @SerializedName("bids")
    public long bids; // Bids count


    // Du
    @Nullable
    public String duPackageName;

    @Nullable
    public String duPackageDetails;


    // Ads
    @SerializedName("img")
    String image;

    // My Bids
    @Nullable
    @SerializedName("item")
    String itemName;

    @Nullable
    @SerializedName("amount")
    String amount;


    public int getId() {
        return id;
    }

//    public String getCategory() {
//        return category;
//    }

    public Date getEndTime() {
        return endTime;
    }

    //    public Date getStartBid() {
//        return startBid;
//    }
    @Nullable
    public String getBuyNow() {
        return buyNow;
    }

    public void setBuyNow(@Nullable String buyNow) {
        this.buyNow = buyNow;
    }

    public MZItemDetails getDetails() {
        return details;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setDetails(MZItemDetails details) {
        this.details = details;
    }

    public int getMinBid() {
        return minBid;
    }

    public void setMinBid(int minBid) {
        this.minBid = minBid;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getBids() {
        return bids;
    }

    public void setBids(long bids) {
        this.bids = bids;
    }

//    public int getUserDeposit() {
//        return userDeposit;
//    }

//    public void setUserDeposit(int userDeposit) {
//        this.userDeposit = userDeposit;
//    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    @Nullable
    public String getType() {
        return type;
    }

    public void setType(@Nullable String type) {
        this.type = type;
    }

    public Date getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Date currentTime) {
        this.currentTime = currentTime;
    }

    public int getDepositType() {
        return depositType;
    }

    public int getDeposit() {
        return deposit;
    }

    public boolean isBided() {
        return isBided;
    }

    public void setBided(boolean bided) {
        isBided = bided;
    }

    @Nullable
    public String getBidder() {
        return bidder;
    }

    public boolean isMyBid() {
        return isMyBid;
    }

    public void setOnlineAuction(boolean onlineAuction) {
        isOnlineAuction = onlineAuction;
    }

    public boolean isOnlineAuction() {
        return isOnlineAuction;
    }

    public void setMyBid(boolean myBid) {
        isMyBid = myBid;
    }

    public String getDuPackageDetails() {
        return duPackageDetails;
    }

    public void setDuPackageDetails(String duPackageDetails) {
        this.duPackageDetails = duPackageDetails;
    }

    @Nullable
    public String getDuPackageName() {
        return duPackageName;
    }

    public void setDuPackageName(@Nullable String duPackageName) {
        this.duPackageName = duPackageName;
    }

    public String getImage() {
        return image;
    }

    public int getSellPrice() {
        return sellPrice;
    }

    public String getItemName() {
        return itemName;
    }

    public String getAmount() {
        return amount;
    }

    public long getTimeDiff() {
        return timeDiff;
    }

    public void setTimeDiff(long timeDiff) {
        this.timeDiff = timeDiff;
    }

    @Nullable
    public String getSoldBy() {
        return soldBy;
    }

    @Nullable
    public int getBooked() {
        return booked;
    }

    @Override
    public String toString() {
        return "MZItem{" +
                "id=" + id +
                ", category=" + category +
                ", endTime=" + endTime +
                ", city=" + city +
                ", type='" + type + '\'' +
                ", bidder='" + bidder + '\'' +
                ", details=" + details +
                ", currentTime=" + currentTime +
                ", timeDiff=" + timeDiff +
                ", isBided=" + isBided +
                ", isMyBid=" + isMyBid +
                ", isOnlineAuction=" + isOnlineAuction +
                ", depositType=" + depositType +
                ", deposit=" + deposit +
                ", minBid=" + minBid +
                ", price=" + price +
                ", sellPrice=" + sellPrice +
                ", booked=" + booked +
                ", soldBy='" + soldBy + '\'' +
                ", bids=" + bids +
                ", duPackageName='" + duPackageName + '\'' +
                ", duPackageDetails='" + duPackageDetails + '\'' +
                ", image='" + image + '\'' +
                ", itemName='" + itemName + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}
