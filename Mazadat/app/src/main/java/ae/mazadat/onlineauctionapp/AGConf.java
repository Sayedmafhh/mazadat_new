package ae.mazadat.onlineauctionapp;

/**
 * Created by agile on 3/23/16.
 */
public class AGConf {

    // By: pixelappz.com

    public static final String APP_NAME = "Online Auction";
    public static final String PACKAGE  = "ae.mazadat.onlineauctionapp";

    /**
     * Rest Configs
     */
    public static final String URL             = "http://mazadat.ae";
    public static final String FIREBASE_URL    = "https://mazadat.firebaseio.com/"; // LIVE

    public static final String REST_API        = URL + "/api_v1/";

    public static final String PAYMENT_URL     = URL + "/api_v1/payment";

    // Social Media Icons
    public static String SOCIAL_FACEBOOK  = "fb://www.facebook.com/online.auction.uae";
    public static String SOCIAL_TWITTER   = "twitter://twitter.com/onlineauction1";
    public static String SOCIAL_GMAIL     = "info@mazadat.ae";
    public static String SOCIAL_GPLUS     = "https://plus.google.com/105394429243637224864/posts";
    public static String SOCIAL_LINKEDIN  = "https://www.linkedin.com/company/online-auction";
    public static String SOCIAL_SNAP      = "https://www.snapchat.com/add/onlineauction";
    public static String SOCIAL_INSTAGRAM = "https://www.instagram.com/onlineauction.ae/";
    public static String SOCIAL_YOUTUBE   = "https://www.youtube.com/channel/UCZKqrjj0g9AC5j9pQwGnEzw";


    public static String SOCIAL_FACEBOOK_WEB  = "https://www.facebook.com/online.auction.uae";
    public static String SOCIAL_TWITTER_WEB   = "https://twitter.com/onlineauction1";
    public static String SOCIAL_GPLUS_WEB     = "https://plus.google.com/105394429243637224864/posts";
    public static String SOCIAL_LINKEDIN_WEB  = "https://www.linkedin.com/company/online-auction";
    public static String SOCIAL_SNAP_WEB      = "https://www.snapchat.com/add/onlineauction";
    public static String SOCIAL_INSTAGRAM_WEB = "https://www.instagram.com/onlineauction.ae/";
    public static String SOCIAL_YOUTUBE_WEB   = "https://www.youtube.com/channel/UCZKqrjj0g9AC5j9pQwGnEzw";


    public static final String FIREBASE_PLATES_URL         = FIREBASE_URL + "uae-plates/ITEMS";
    public static final String FIREBASE_DU_URL             = FIREBASE_URL + "du-vip-plan/ITEMS";
    public static final String FIREBASE_CARS_URL           = FIREBASE_URL + "cars/ITEMS";
    public static final String FIREBASE_BAGS_URL           = FIREBASE_URL + "bags/ITEMS";
    public static final String FIREBASE_COINS_URL          = FIREBASE_URL + "coins/ITEMS";
    public static final String FIREBASE_HORSES_URL         = FIREBASE_URL + "horses/ITEMS";
    public static final String FIREBASE_JEWELLERY_URL      = FIREBASE_URL + "jewellery/ITEMS";
    public static final String FIREBASE_PROPERTIES_URL     = FIREBASE_URL + "properties/ITEMS";
    public static final String FIREBASE_MIX_URL            = FIREBASE_URL + "mix/ITEMS";

    /**
     * Keys
     */

    public static final String ARG_REVEAL_START_LOCATION = "reveal_start_location";
    public static final String ARG_REVEAL_COLOR          = "reveal_color";

    public static final String KEY_LANG = "KEY_LANG";

    public static final String KEY_AUCTION_ITEM = "KEY_AUCTION_ITEM";
    public static final String KEY_AUCTION_TYPE = "KEY_AUCTION_TYPE";
    public static final String KEY_IS_EMPTY_AUCTION = "KEY_IS_EMPTY_AUCTION";
    public static final String KEY_ITEM_DATA    = "KEY_ITEM_DATA";
    public static final String KEY_ITEM_DATA_DETAILS    = "KEY_ITEM_DATA_DETAILS";
    public static final String KEY_ITEM_DEPOSIT = "KEY_ITEM_DEPOSIT";

    public static final String KEY_PLATES_AUCTION = "KEY_PLATES_AUCTION";
    public static final String KEY_DU_AUCTION     = "KEY_DU_AUCTION";
    public static final String KEY_CARS_AUCTION   = "KEY_CARS_AUCTION";
    public static final String KEY_BAGS_AUCTION   = "KEY_BAGS_AUCTION";
    public static final String KEY_COINS_AUCTION   = "KEY_COINS_AUCTION";
    public static final String KEY_PROP_AUCTION   = "KEY_PROP_AUCTION";
    public static final String KEY_JEWELLERY_AUCTION   = "KEY_JEWELLERY_AUCTION";
    public static final String KEY_HORSES_AUCTION   = "KEY_HORSES_AUCTION";
    public static final String KEY_MISC_AUCTION   = "KEY_MISC_AUCTION";

    public static final String KEY_DIRECT_SELL   = "KEY_DIRECT_SELL";
    public static final String KEY_MY_PURCHASES  = "KEY_MY_PURCHASES";
    public static final String KEY_MY_BIDS       = "KEY_MY_BIDS";


    public static final String KEY_AUCTION_GRID   = "KEY_CARS_AUCTION";

    public static final String KEY_PAGE         = "KEY_PAGE";
    public static final String KEY_PAGE_ABOUT   = "KEY_PAGE_ABOUT";
    public static final String KEY_PAGE_FAQ     = "KEY_PAGE_FAQ";
    public static final String KEY_PAGE_TOC     = "KEY_PAGE_TOC";
    public static final String KEY_PAGE_CONTACT = "KEY_PAGE_CONTACT";

    // Direct sell item ID
    public static final String KEY_DIRECT_SELL_ITEM_ID     = "KEY_DIRECT_SELL_ITEM_ID";
    public static final String KEY_PAYMENT_URL             = "KEY_PAYMENT_URL";
    public static final String KEY_DIRECT_PAYMENT          = "KEY_DIRECT_PAYMENT";
    public static final String KEY_EVALUATE_PLATE_PAYMENT  = "KEY_EVALUATE_PLATE_PAYMENT";

    public static final String KEY_ADS = "KEY_ADS";

    public static final int CAT_CARS   = 1;
    public static final int CAT_DU     = 2;
    public static final int CAT_PLATES = 5;
    public static final int CAT_PROP   = 6;
    public static final int CAT_HORSE  = 11;
    public static final int CAT_WATCH  = 13;
    public static final int CAT_MISC   = 14;
    public static final int CAT_COINS = 15;
    public static final int CAT_BAGS   = 16;

    public static final int CAT_DIRECT_SELL = 8;
    public static final int CAT_SERVICES  = 17;

    public static final int MY_PURCHASES  = -100;
    public static final int MY_BIDS       = -101;


    // Services
    public static final int CAT_DRAW_PLATE      = 21;
    public static final int CAT_REQUEST_PLATE   = 18;
    public static final int CAT_EVALUATE_PLATE  = 20;
    public static final int CAT_MIX_ADS         = 23;

    public static final int CAT_DRAW_NUMBER     = 22;
    public static final int CAT_REQUEST_NUMBER  = 19;
}