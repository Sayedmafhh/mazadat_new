package ae.mazadat.onlineauctionapp.adapter.draw;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.util.MZUtil;
import ae.mazadat.onlineauctionapp.viewholder.DrawItemViewHolder;
import ae.mazadat.onlineauctionapp.viewholder.DrawNumberItemViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MZItem} and makes a call to the
 * specified {@link ItemsListFragment.OnMZItemInteractionListener}.
 */
public class DrawNumbersAdapter extends DrawAdapter {

    public DrawNumbersAdapter(Context context) {
        super(context);
    }

    @Override
    public DrawItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_PLATE) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_draw_plate, parent, false);
            final DrawNumberItemViewHolder vh = new DrawNumberItemViewHolder(view);

//            vh.card.setOnLongClickListener(this);

            return vh;
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_draw_number, parent, false);
            final DrawNumberItemViewHolder vh = new DrawNumberItemViewHolder(view);
            return vh;
        }

    }


    @Override
    public void onBindViewHolder(final DrawItemViewHolder vh, int position) {

        final DrawNumberItemViewHolder holder = (DrawNumberItemViewHolder) vh;

        holder.mItem = mValues.get(position);

//        holder.card.setOnLongClickListener(this);
        holder.card.setTag(R.string.tag_position, position);

        int price = holder.mItem.getPrice().isEmpty() ? 0 : Integer.parseInt(holder.mItem.getPrice());
        holder.tvPrice.setText(String.format("%,d", price));


        if (holder.mItem.getType().contentEquals("plate")) {
            holder.tvPltR.setText(holder.mItem.getRightNum());
            holder.tvPltL.setText(holder.mItem.getLeftNum());

            holder.ivPlate.setImageDrawable(MZUtil.getPlateDrawable(Integer.parseInt(holder.mItem.getCity())));
//            if (holder.mItem.getCity().contentEquals("10")) { // Abu Dhabi
//                holder.tvPltL.setTextColor(mContext.getResources().getColor(R.color.white));
//            } else {
//                holder.tvPltL.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryText));
//            }
        } else {
            holder.tvPrefix.setText(holder.mItem.getPrefix());
            holder.tvNumber.setText(holder.mItem.getNumber());
        }

    }
}
