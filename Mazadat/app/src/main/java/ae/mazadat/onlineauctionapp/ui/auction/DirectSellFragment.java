package ae.mazadat.onlineauctionapp.ui.auction;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.DirectSellAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.ui.GridFragment;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnMZItemInteractionListener}
 * interface.
 */
public class DirectSellFragment extends GridFragment {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DirectSellFragment() {
    }


    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DirectSellFragment newInstance(int columnCount) {
        DirectSellFragment fragment = new DirectSellFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_base_list, container, false);

        setHasFab(true);
        setHasLoadMore(true);

        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(false);

        init(getActivity());

        adapter = new DirectSellAdapter(getActivity(), mListener);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));

        if (savedInstanceState == null) {
            loadItems();
        }

        return rootView;
    }


    @Override
    public void serviceLoadItems(final boolean reset) {
        super.serviceLoadItems(reset);

        filterParams.put("lang", session.getMZLang());
        filterParams.put("limit", String.valueOf(ITEMS_PER_PAGE));
        filterParams.put("offset", String.valueOf(currentPage * ITEMS_PER_PAGE));
        filterParams.put("development", String.valueOf(1));
        gettingItemsCall = api.getDirectSell(filterParams);
        gettingItemsCall.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {

                if (response.isSuccessful()) {
                    MZResponse MZResponse = response.body();
                    Log.i("Is Online Bid", "Is online bid: " + (MZResponse.isActivate_bid() ? "YES" : "NO") );
                    List<MZItem> items = MZResponse.getItems();

                    mFilters = MZResponse.getFilters();
                    mSorts   = MZResponse.getSorts();

                    // TODO: When failed - this may throw an error!

                    // TODO: Remove solr
                    _initFiltersAndSorts();

                    doWhenSuccessLoad(reset, items);

                    if (items != null && items.size() > 0) {
                        for (int i=0; i<items.size(); i++) {
                            MZItem item = items.get(i);
                            item.setOnlineAuction(!(MZResponse.isActivate_bid()));
                            adapter.add(item);
                        }
                    } else {
                        if (currentPage == 0) {
                            AGUtil.errorAlert(getActivity(), "No items!", getString(R.string.no_ds_items));
                        }
                    }
                } else {
                    doWhenFailureApiCall("Opps!", "Unable to load items!");
                }
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {

            }
        });
    }

    @Override
    protected void registerPN() {
    }

}
