package ae.mazadat.onlineauctionapp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by agile on 4/1/16.
 */
public class AdViewHolder extends RecyclerView.ViewHolder {

    public final View mView;

    @Bind(R.id.iv) public ImageView mIv;

    public MZItem mItem;

    public AdViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        mView = view;
    }
}
