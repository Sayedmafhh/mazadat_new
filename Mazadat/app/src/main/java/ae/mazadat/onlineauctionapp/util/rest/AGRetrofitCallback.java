package ae.mazadat.onlineauctionapp.util.rest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by agile on 4/5/16.
 */
public abstract class AGRetrofitCallback <T> implements Callback<T> {

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            T res = response.body();
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
    }
}