package ae.mazadat.onlineauctionapp.ui.details.tab;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import org.parceler.Parcels;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by agile on 4/22/16.
 */
public class PackageFragment extends Fragment {

    public PackageFragment() {}

    @Bind(R.id.tvPkg)
    TextView tvPkg;

    @Bind(R.id.webview)
    WebView webview;

    MZItem mItem;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PackageFragment newInstance(MZItem item, String type) {
        PackageFragment fragment = new PackageFragment();
        Bundle args = new Bundle();
        args.putParcelable(AGConf.KEY_ITEM_DATA, Parcels.wrap(item));
        args.putString(AGConf.KEY_AUCTION_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getParcelable(AGConf.KEY_ITEM_DATA) != null) {
                Parcelable data = getArguments().getParcelable(AGConf.KEY_ITEM_DATA);
                mItem = Parcels.unwrap(data);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_package, container, false);

        ButterKnife.bind(this, rootView);

        tvPkg.setText(mItem.getDuPackageName());
        webview.setBackgroundColor(Color.TRANSPARENT);

        SessionManager session = App.getSessionManager();

        String dir = session.getMZLang().contentEquals("ae") ? "rtl" : "ltr";
        String html = "<body dir=\"" + dir + "\">";
        html += mItem.getDuPackageDetails();
        html += "</body>";
        System.out.println("htma detail "+mItem.getDuPackageDetails());

        webview.loadData(html, "text/html; charset=UTF-8", null);

        return rootView;
    }

}
