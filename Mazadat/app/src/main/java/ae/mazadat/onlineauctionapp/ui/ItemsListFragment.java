package ae.mazadat.onlineauctionapp.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.BaseAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.fb.FBItem;
import ae.mazadat.onlineauctionapp.model.rest.MZFilter;
import ae.mazadat.onlineauctionapp.model.rest.MZOption;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.model.rest.MZSort;
import ae.mazadat.onlineauctionapp.model.rest.UniversalMsg;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGApiResponse;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnMZItemInteractionListener}
 * interface.
 */
public abstract class ItemsListFragment extends Fragment {

    protected int ITEMS_PER_PAGE = 30;
    protected BaseAdapter adapter;

    protected Context mContext;
    protected String mLang;

    // Options menu
    protected Menu mOptionsMenu;

    // Retrofit
    protected AGApi api;
    protected Call<MZResponse> gettingItemsCall;
    protected Call<AGApiResponse> regCall;// PN

    protected Map<String, String> regParams = new HashMap<>();

    protected Firebase fbRef;

    protected boolean moreContentToLoad = true;
    protected int currentPage, pastVisibleItems, visibleItemCount, totalItemCount, lastTotalItemCount;
    protected int triggerLoadMoreBeforeItems = 10;

    protected boolean hasFab = true;
    protected boolean hasLoadMore = true;

    protected boolean isEmptyAuction = false;

    @Nullable
    @Bind(R.id.pvContainer)
    public View pvContainer;

    @Nullable
    @Bind(R.id.tvEmpty)
    public TextView tvEmpty;

    @Nullable
    @Bind(R.id.llReg)
    public View llReg;

    @Nullable
    @Bind(R.id.btnReg)
    public Button btnReg;


    @Nullable
    @Bind(R.id.sortLoadingText)
    public TextView sortLoadingText;
    @Bind(R.id.swipeRefreshLayout)
    public SwipeRefreshLayout mSwipeRefreshLayout;

    @Nullable
    @Bind(R.id.lllistContainer)
    public LinearLayout lllistContainer;

    @Bind(R.id.list)
    public RecyclerView recyclerView;

    @Nullable
    @Bind(R.id.txtTopMessage)
    public TextView txtTopMessage;

    @Nullable
    @Bind(R.id.txtDateTime)
    public TextView txtDateTime;

    @Nullable
    @Bind(R.id.txtVenue)
    public TextView txtVenue;

    @Nullable
    @Bind(R.id.tabs)
    public TabLayout tabsLayout;

    // Social media icons
    @Nullable
    @Bind(R.id.icFace)
    public ImageView ivFace;

    @Nullable
    @Bind(R.id.icTwitter)
    public ImageView ivTwitter;

    @Nullable
    @Bind(R.id.icInsta)
    public ImageView ivInsta;

    @Nullable
    @Bind(R.id.icLinkedin)
    public ImageView ivLinkedin;

    @Nullable
    @Bind(R.id.icSnap)
    public ImageView ivSnap;

    @Nullable
    @Bind(R.id.icYoutube)
    public ImageView ivYoutube;

    @Nullable
    @Bind(R.id.icGmail)
    public ImageView ivGmail;

    @Nullable
    @Bind(R.id.icGplus)
    public ImageView ivGplus;

    /* From plates */
    // Dialogs
    protected Dialog filterDialog;
    protected Dialog sortDialog;

    protected boolean filtersInitiated = false;
    protected Map<String, String> filterParams = new HashMap<>();
    protected List<MZFilter> mFilters;
    protected List<MZSort> mSorts;
    protected List<UniversalMsg> universalMsgs;
    protected static boolean isActivatedBid = false;

    /* /From plates */

    protected int mColumnCount = 1;

    protected SessionManager session;

    // Keep here
    protected RecyclerView.LayoutManager layoutManager;

    protected OnMZItemInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemsListFragment() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Cancel HTTP calls
        if (gettingItemsCall != null) {
            gettingItemsCall.cancel();
        }
    }

    public void setHasFab(boolean hasFab) {
        this.hasFab = hasFab;
    }

    public void setHasLoadMore(boolean hasLoadMore) {
        this.hasLoadMore = hasLoadMore;
    }


    protected SwipeRefreshLayout.OnRefreshListener mOnSwipeRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            loadItems();
        }

    };

    protected void init(Context context) {

        this.mContext = context;

        session = App.getSessionManager();
        api = App.getRestAdapter().create(AGApi.class);

        // reg PN
        mLang = session.getMZLang();
        regParams.put("lang", mLang);
        regParams.put("platform", "android");


        // Set list type
        if (getArguments() != null) {
            int type = getArguments().getInt(AGConf.KEY_AUCTION_TYPE);
            if (type == AGConf.CAT_CARS || type == AGConf.CAT_PROP || type == AGConf.CAT_HORSE ||
                    type == AGConf.CAT_WATCH || type == AGConf.CAT_MISC || type == AGConf.CAT_COINS ||
                    type == AGConf.CAT_BAGS || type == AGConf.CAT_DIRECT_SELL || type == AGConf.MY_PURCHASES) {
                mColumnCount = 2;
            }

            // Check if empty auction, so will show register in PN
            isEmptyAuction = getArguments().getBoolean(AGConf.KEY_IS_EMPTY_AUCTION, false);
        }

        // Swipe Refresh Layout
        mSwipeRefreshLayout.setOnRefreshListener(mOnSwipeRefreshListener);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                                                    R.color.colorPrimaryDark,
                                                    R.color.colorPrimaryText,
                                                    R.color.colorLightPrimary);

        if (mColumnCount > 1) {
            layoutManager = new GridLayoutManager(recyclerView.getContext(), mColumnCount);
        } else {
            layoutManager = new LinearLayoutManager(recyclerView.getContext());
        }

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                if (mColumnCount == 1) {
                    pastVisibleItems = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                    // TODO: Handle load more for grid view
                } else {
                    pastVisibleItems = ((GridLayoutManager) layoutManager).findFirstVisibleItemPosition();
                }


                if (hasLoadMore && moreContentToLoad) {

                    if (totalItemCount == lastTotalItemCount) {
                        return; // Do nothing, do not load more
                    }

                    // Load when reaching x posts before
                    if ((visibleItemCount + pastVisibleItems + triggerLoadMoreBeforeItems) >= totalItemCount) {
                        lastTotalItemCount = totalItemCount;
                        loadMoreItems();
                    }
                }
            }
        });

        if (btnReg != null) {
            btnReg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    registerPN();
                }
            });
        }

        // Social media icons
        if (ivFace != null) {
            ivFace.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = AGConf.SOCIAL_FACEBOOK;
                    openUrl(url, AGConf.SOCIAL_FACEBOOK_WEB);
                }
            });
        }

        if (ivTwitter != null)
            ivTwitter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = AGConf.SOCIAL_TWITTER;
                    openUrl(url, AGConf.SOCIAL_TWITTER_WEB);
                }
            });

        if (ivLinkedin != null)
            ivLinkedin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = AGConf.SOCIAL_LINKEDIN;
                    openUrl(url, AGConf.SOCIAL_LINKEDIN_WEB);
                }
            });

        if (ivYoutube != null)
            ivYoutube.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = AGConf.SOCIAL_YOUTUBE;
                    openUrl(url, AGConf.SOCIAL_YOUTUBE_WEB);
                }
            });

        if (ivInsta != null)
            ivInsta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = AGConf.SOCIAL_INSTAGRAM;
                    openUrl(url, AGConf.SOCIAL_INSTAGRAM_WEB);
                }
            });

        if (ivSnap != null)
            ivSnap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = AGConf.SOCIAL_SNAP;
                    openUrl(url, AGConf.SOCIAL_SNAP_WEB);
                }
            });

        if (ivGplus != null)
            ivGplus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = AGConf.SOCIAL_GPLUS;
                    openUrl(url, AGConf.SOCIAL_GPLUS_WEB);
                }
            });

        if (ivGmail != null)
            ivGmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", AGConf.SOCIAL_GMAIL, null));
                    startActivity(Intent.createChooser(emailIntent, ""));
                }
            });

    }

    private void openUrl(String url, String webUrl) {
        try {
            Intent socialIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(socialIntent);
        }catch (Exception ex){
            Intent socialIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(webUrl));
            startActivity(socialIntent);
            ex.printStackTrace();
        }

//        Intent i = new Intent(Intent.ACTION_VIEW);
//        i.setData(Uri.parse(url));
//        startActivity(i);
    }

    protected void init(Context context, boolean isDu) {

        init(context);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorDU, R.color.colorDUDark,
                R.color.colorDUTransparent);
    }

    protected void iniFirebase(String url) {
        fbRef = new Firebase(url);
        fbRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                FBItem item = dataSnapshot.getValue(FBItem.class);
                List<MZItem> mzItems = adapter.getItems();
                Log.i("ChildChange", mzItems.toString());
                System.out.println("ChildChange "+ mzItems.toString());
                for (int i = 0; i < mzItems.size(); i++) {
                    // check if this item is changed
                    MZItem mzItem = mzItems.get(i);
                    if (item.getId() != mzItem.getId()) {
                        // Not this item... Next
                        continue;
                    } else {
                        // This is the changed item
                        updateItem(i, mzItem, item);
                    }
                }
                System.out.println("child Item "+item.getCurrent_price());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }


    protected void updateItem(int i, MZItem mzItem, FBItem item) {


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 2016-06-14 16:00:00

        String end_time = "";
        try {
            end_time = format.parse(item.getEnd_time()).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (item.getBids() != mzItem.getBids() || !end_time.contentEquals(mzItem.getEndTime().toString())) {
            AGUtil.bid(getActivity());
            mzItem.setPrice(Long.parseLong(item.getCurrent_price()));
            mzItem.setBids(item.getBids());
            String dtStart = item.getEnd_time();
            try {
                Date date = format.parse(dtStart);
                mzItem.setEndTime(date); // TODO: SET properly
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            mzItem.setBided(true);

            if (session.getUid() > 0 && item.getUser() != null && item.getUser().contentEquals(session.getUid() + "")) {
                mzItem.setMyBid(true);
            } else {
                mzItem.setMyBid(false);
            }

            // Notify adapter
            adapter.update(i, mzItem);
        }
    }

    protected void loadItems() {
        if (isEmptyAuction) {
            mSwipeRefreshLayout.setVisibility(View.GONE);
            llReg.setVisibility(View.VISIBLE);
        } else {
            // Reset current page
            currentPage = 0;
            lastTotalItemCount = 0;
            moreContentToLoad = true;

            if (pvContainer != null) {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    pvContainer.setVisibility(View.GONE);
                } else {
                    pvContainer.setVisibility(View.VISIBLE); // Show Progress Bar loader
                }
            }

            serviceLoadItems(true);
        }
    }


    public void serviceLoadItems(final boolean reset) {

        if (isAdded() && getActivity() != null) {
            tvEmpty.setVisibility(View.GONE);

            // Only show background loader when loading for the first time
            if (currentPage == 0 && mSwipeRefreshLayout.isRefreshing() == false) {
                pvContainer.setVisibility(View.VISIBLE);
            }
        }
    }


    public void loadMoreItems() {
        currentPage++; // Get next page
        System.out.println("currrent page "+currentPage );
        serviceLoadItems(false);
    }

    public void doWhenSuccessLoad(boolean reset) {
        if (getActivity() != null && isAdded()) {

            if (pvContainer != null) {
                pvContainer.setVisibility(View.GONE);
            }

            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    public void doWhenFailureApiCall(String title, String errorMessage) {

        if (getActivity() != null && isAdded()) {

            if (pvContainer != null) {
                pvContainer.setVisibility(View.GONE);
            }

            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }

            if (currentPage == 0) {
                AGUtil.errorAlert(getActivity(), title, errorMessage);
            }
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMZItemInteractionListener) {
            mListener = (OnMZItemInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMZItemInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMZItemInteractionListener {
        // TODO: Update argument type and name
        void onMZItemInteraction(MZItem item, String type);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.items_list, menu);
        mOptionsMenu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.act_filter:
                showFilterDialog();
                break;

            case R.id.act_sort:
                showSortDialog();
                break;
        }
        return true;
    }


    private void showFilterDialog() {
        filterDialog.show();
    }

    protected void _initFilters() {

        // TODO: Show default selected filter
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = inflater.inflate(R.layout.filter_items, null);

        ViewGroup.LayoutParams vParams = new ViewGroup.LayoutParams(AGUtil.dpToPx(240), ViewGroup.LayoutParams.WRAP_CONTENT);
        v.setLayoutParams(vParams);

        LinearLayout spnCont = (LinearLayout) v.findViewById(R.id.vSpinners);

        // Buttons
        Button btnFilter = (Button) v.findViewById(R.id.btnFilter);
        Button btnCancel = (Button) v.findViewById(R.id.btnCancel);

        EditText etSearch = (EditText) v.findViewById(R.id.etSearch);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterParams.put("search", s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.dismiss();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.dismiss();
                loadItems(); // TODO: Test
            }
        });

        if (mFilters != null && mFilters.size() > 0) {

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            for (int i = 0; i < mFilters.size(); i++) {

                LinearLayout fltrItem = (LinearLayout) inflater.inflate(R.layout.filter_item, null);
                TextView label = (TextView) fltrItem.findViewById(R.id.lbl);
                final Spinner spinner = (Spinner) fltrItem.findViewById(R.id.spn);

                params.setMargins(0, AGUtil.dpToPx(20), 0, 0); //substitute parameters for left, top, right, bottom
                spinner.setLayoutParams(params);

                final MZFilter item = mFilters.get(i);


                String[] spinnerArray = new String[item.getOptions().size() + 1];
                final HashMap<String, String> spinnerMap = new HashMap<String, String>();

                label.setText(item.getLabel());

                // First filter item - all
                spinnerMap.put(getString(R.string.filter_all), ""); // TODO: Test
                spinnerArray[0] = getString(R.string.filter_all);

                for (int x = 0; x < item.getOptions().size(); x++) {
                    MZOption option = item.getOptions().get(x);
                    spinnerMap.put(option.getName(), option.getId());
                    spinnerArray[x + 1] = option.getName();
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

                AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        String itemName = spinner.getSelectedItem().toString();
                        String itemCode = spinnerMap.get(itemName);

                        filterParams.put(item.getName(), itemCode);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                };

                spinner.setOnItemSelectedListener(listener);
                spnCont.addView(fltrItem);
            }
        }

        filterDialog = new Dialog(getActivity()); // Context, this, etc.
        filterDialog.setContentView(v);
        filterDialog.setTitle(getString(R.string.filter));

    }
    protected void _initSorts() {
        View v = ((LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                inflate(R.layout.sort_items_new, null);
        v.setLayoutParams(new ViewGroup.LayoutParams(AGUtil.dpToPx(240), -2));
        if (this.mSorts != null && this.mSorts.size() > 0) {
            for (int i = 0; i < this.mSorts.size(); i++) {
                MZSort sItem = (MZSort) this.mSorts.get(i);
                this.filterParams.put("sort", "bid");
                this.filterParams.put("by", "ASC");
                System.out.println("sort" + sItem.getName());
            }
        }
        TextView tvBid = (TextView) v.findViewById(R.id.tvSortBid);
        TextView tvTime = (TextView) v.findViewById(R.id.tvSortTime);
        final ImageView imgTime = (ImageView) v.findViewById(R.id.imgSortTime);
        final ImageView imgBid = (ImageView) v.findViewById(R.id.imgSortBid);
        final RelativeLayout rlBid = (RelativeLayout) v.findViewById(R.id.rlSortBid);
        final RelativeLayout rlTime = (RelativeLayout) v.findViewById(R.id.rlSorttime);
        rlTime.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        rlBid.setBackgroundColor(getActivity().getResources().getColor(R.color.colorDivider));
        imgTime.setVisibility(8);
        tvBid.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                imgTime.setVisibility(8);
                imgBid.setVisibility(0);
                rlTime.setBackgroundColor(ItemsListFragment.this.getActivity().getResources().getColor(R.color.white));
                rlBid.setBackgroundColor(ItemsListFragment.this.getActivity().getResources().getColor(R.color.colorDivider));
                ItemsListFragment.this.filterParams.put("sort", "bid");
                ItemsListFragment.this.filterParams.put("by", "ASC");
            }
        });
        final ImageView imageView = imgBid;
        final ImageView imageView2 = imgTime;
        final RelativeLayout relativeLayout = rlBid;
        final RelativeLayout relativeLayout2 = rlTime;
        tvTime.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                imageView.setVisibility(8);
                imageView2.setVisibility(0);
                relativeLayout.setBackgroundColor(ItemsListFragment.this.getActivity().getResources().getColor(R.color.white));
                relativeLayout2.setBackgroundColor(ItemsListFragment.this.getActivity().getResources().getColor(R.color.colorDivider));
                ItemsListFragment.this.filterParams.put("sort", "time");
                ItemsListFragment.this.filterParams.put("by", "ASC");
            }
        });
        ((Button) v.findViewById(R.id.btnSort)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                loadItemsNew();
                sortDialog.dismiss();
            }
        });
        this.sortDialog = new Dialog(getActivity());
        this.sortDialog.requestWindowFeature(1);
        this.sortDialog.setContentView(v);
    }
    protected void loadItemsNew() {
        if (this.isEmptyAuction) {
            this.mSwipeRefreshLayout.setVisibility(View.GONE);
            this.llReg.setVisibility(View.VISIBLE);
            return;
        }
        this.currentPage = 0;
        this.lastTotalItemCount = 0;
        this.moreContentToLoad = true;
        System.out.println("**** " + this.currentPage + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.lastTotalItemCount + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.moreContentToLoad);
        if (this.pvContainer != null) {
            if (this.mSwipeRefreshLayout.isRefreshing()) {
                this.pvContainer.setVisibility(View.GONE);
            } else {
                this.pvContainer.setVisibility(View.VISIBLE);
            }
        }
        this.sortLoadingText.setVisibility(View.VISIBLE);
        this.pvContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });



        serviceLoadItems(true);
    }
    protected void _initSortsOld() {
        // TODO: Show default selected sort
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = inflater.inflate(R.layout.sort_items, null);

        ViewGroup.LayoutParams vParams = new ViewGroup.LayoutParams(AGUtil.dpToPx(240), ViewGroup.LayoutParams.WRAP_CONTENT);
        v.setLayoutParams(vParams);

        LinearLayout vBtns = (LinearLayout) v.findViewById(R.id.vBtns);

        // Buttons
        Button btnCancel = (Button) v.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortDialog.dismiss();
            }
        });


        if (mSorts != null && mSorts.size() > 0) {

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            for (int i = 0; i < mSorts.size(); i++) {

                final MZSort sItem = mSorts.get(i);

                LinearLayout sortItem = (LinearLayout) inflater.inflate(R.layout.sort_item, null);
                Button btnAsc = (Button) sortItem.findViewById(R.id.btnSortAsc);
                Button btnDesc = (Button) sortItem.findViewById(R.id.btnSortDesc);

                params.setMargins(0, AGUtil.dpToPx(20), 0, 0); //substitute parameters for left, top, right, bottom

                sortItem.setLayoutParams(params);

                btnAsc.setText(sItem.getLabel());
                btnDesc.setText(sItem.getLabel());

                btnAsc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sortDialog.dismiss();
                        filterParams.put("sort", sItem.getName());
                        filterParams.put("by", "ASC");
                        loadItems();
                    }
                });

                btnDesc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sortDialog.dismiss();
                        filterParams.put("sort", sItem.getName());
                        filterParams.put("by", "DESC");
                        loadItems();
                    }
                });

                vBtns.addView(sortItem);
            }
        }

        sortDialog = new Dialog(getActivity()); // Context, this, etc.
        sortDialog.setContentView(v);
        sortDialog.setTitle(getString(R.string.sort));
    }


    protected void _initFiltersAndSorts() {
        if (!filtersInitiated) { // One time only

            if (mFilters == null) {
                // Disable filter button
                if (mOptionsMenu != null) {
                    mOptionsMenu.findItem(R.id.act_filter).setVisible(false);
                    onPrepareOptionsMenu(mOptionsMenu);
                }
            }

            if (mSorts == null) {
                // Disable sort button
                if (mOptionsMenu != null) {
                    mOptionsMenu.findItem(R.id.act_sort).setVisible(false);
                    onPrepareOptionsMenu(mOptionsMenu);
                }
            }

            _initFilters();
            _initSorts();

            filtersInitiated = true;

            setHasOptionsMenu(true);
        }
    }

    private void showSortDialog() {
        sortDialog.show();
    }


    protected void initNoItems() {
        if (currentPage == 0) {

            tvEmpty.setText(getString(R.string.no_results));
            tvEmpty.setVisibility(View.VISIBLE);
        }
    }

    // Register for push notification for this category
    protected abstract void registerPN();

    protected void _registerPN() {

        regParams.put("device_id", session.getRegistrationId());

        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(R.color.colorPrimaryDark);
        pDialog.setTitleText(getString(R.string.pn_registering));
        pDialog.setCancelable(false);
        pDialog.show();

        regCall = api.regPN(regParams);
        System.out.println("listing parameter "+regParams.toString());
        regCall.enqueue(new AGRetrofitCallback<AGApiResponse>() {
            @Override
            public void onResponse(Call<AGApiResponse> call, Response<AGApiResponse> response) {
                super.onResponse(call, response);
                if (isAdded() && getActivity() != null) {
                    pDialog.dismiss();

                    if (response.isSuccessful()) {
                        AGApiResponse res = response.body();

                        if (!res.getStatus().contentEquals("200")) {
                            AGUtil.errorAlert(getActivity(), getString(R.string.pn_register), res.getMsg());
                        } else {
                            String msg;
                            if (res.getMsg() != null && !res.getMsg().isEmpty()) {
                                msg = res.getMsg();
                            } else {
                                msg = getString(R.string.pn_rgisterd);
                            }
                            AGUtil.successAlert(getActivity(), getString(R.string.pn_register), msg);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AGApiResponse> call, Throwable t) {
                super.onFailure(call, t);
                if (isAdded() && getActivity() != null) {
                    pDialog.dismiss();
                }
            }
        });
    }

    /// Tabs
    public void _initTabs() {
        tabsLayout.addTab(tabsLayout.newTab().setText(getString(R.string.terms)), 0);
        tabsLayout.addTab(tabsLayout.newTab().setText(getString(R.string.map)), 1);
        tabsLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabSelected(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tabSelected(tab.getPosition());
            }
        });
    }

    private void tabSelected(int position) {
        switch (position) {
            case 0:
                Intent termsConditionIntent = new Intent(getActivity(), TermsConditionsActivity.class);
                termsConditionIntent.putExtra("webcontent", (String) (universalMsgs.get(2).getValue()));
                termsConditionIntent.putExtra("title", getString(R.string.terms));
                getActivity().startActivity(termsConditionIntent);
                break;
            case 1:
                Log.i("UMSG", "" + (((Map<String, Object>)universalMsgs.get(3).getValue()).get("lat")));
                Intent locationIntent = new Intent(getActivity(), LocationActivity.class);
                locationIntent.putExtra("title", getString(R.string.map));
                locationIntent.putExtra("lat", "" + (((Map<String, Object>)universalMsgs.get(3).getValue()).get("lat")));
                locationIntent.putExtra("lng", "" + (((Map<String, Object>)universalMsgs.get(3).getValue()).get("lng")));
                getActivity().startActivity(locationIntent);
                break;
        }
    }

}
