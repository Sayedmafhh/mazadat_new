package ae.mazadat.onlineauctionapp.model.fb;

/**
 * Created by agile on 4/20/16.
 */
public class FBLocation {
    String user;
    String country;

    public String getUser() {
        return user;
    }

    public String getCountry() {
        return country;
    }
}
