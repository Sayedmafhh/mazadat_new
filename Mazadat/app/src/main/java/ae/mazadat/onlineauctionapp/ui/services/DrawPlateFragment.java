package ae.mazadat.onlineauctionapp.ui.services;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.draw.DrawAdapter;
import ae.mazadat.onlineauctionapp.adapter.draw.DrawNumbersAdapter;
import ae.mazadat.onlineauctionapp.model.MZJsonObj;
import ae.mazadat.onlineauctionapp.model.draw.MZDrawNumberItem;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.MZUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGApiResponse;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class DrawPlateFragment extends Fragment
        implements DrawNumbersAdapter.OnLongClickListener {

    public static final int REQUEST_WRITE_STORAGE = 112;

    protected AGApi api;
    protected Call<AGApiResponse> drawCall;

    @Bind(R.id.card)
    CardView card;

    @Bind(R.id.ivImg)
    ImageView ivImg;

    @Bind(R.id.btnSave)
    Button btnSave;

    @Bind(R.id.spnType)
    Spinner spnType;

    @Bind(R.id.spnEmirate)
    Spinner spnEmirate;

    @Bind(R.id.spnCode)
    Spinner spnCode;

    @Bind(R.id.etNumber)
    EditText etNumber;

    @Bind(R.id.btnAdd)
    Button btnAdd;

    @Bind(R.id.btnDraw)
    Button btnDraw;

    @Bind(R.id.etContact)
    EditText etContact;

    @Bind(R.id.etPrice)
    EditText etPrice;

    @Bind(R.id.chAsk)
    CheckBox chAsk;

    @Bind(R.id.tivPrice)
    View tivPrice;

    boolean hasPermission;

    SessionManager session = App.getSessionManager();

    Map<String, String> requestParams = new HashMap<>();

    @Bind(R.id.list)
    RecyclerView recyclerView;

    String code     = "";
    String emirate  = "";
    String type     = "";
    String rightNum = "";
    String ask      = "";
    String price    = "";

    String contact  = "";

    String mURL;

    protected DrawAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DrawPlateFragment() {
    }


    @SuppressWarnings("unused")
    public static DrawPlateFragment newInstance(int columnCount) {
        DrawPlateFragment fragment = new DrawPlateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_draw_plate, container, false);

        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(true);

        api = App.getRestAdapter().create(AGApi.class);

        etContact.setText(session.getUserPhone());

        hasPermission = (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

        initSpinner(spnType,    "plate_types.json", "plate_type");
        initSpinner(spnEmirate, "emirates.json",    "emirate");
        initSpinner(spnCode,    "codes.json",       "code");

        requestParams.put("lang", session.getMZLang());

        adapter = new DrawNumbersAdapter(getActivity());
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new
                ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        adapter.remove(viewHolder.getAdapterPosition());
                    }
                };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        adapter.setOnListItemClickListener(this);

        chAsk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tivPrice.setVisibility(View.GONE);
                    ask = "1";
                } else {
                    tivPrice.setVisibility(View.VISIBLE);
                    ask = "0";
                }
            }
        });

        etPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                price = s.toString();
            }
        });

        etContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                contact = s.toString();
            }
        });

        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rightNum = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etNumber.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), getString(R.string.err_no_number), Toast.LENGTH_SHORT).show();
                } else {
                    if (adapter.getItemCount() >= 5) {
                        Toast.makeText(getActivity(), getString(R.string.err_max_draw_limit), Toast.LENGTH_SHORT).show();
                    } else {
                        _addItem();
                        etNumber.setText("");
                        etPrice.setTag("");
                    }
                    AGUtil.closeKeyboard(getActivity());
                }
            }
        });


        btnDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapter.getItemCount() == 0) {
                    Toast.makeText(getActivity(), getString(R.string.err_no_plates_to_draw), Toast.LENGTH_SHORT).show();
                } else {

                    new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getString(R.string.are_you_sure))
                            .setContentText(getString(R.string.draw_plate))
                            .setConfirmText(getString(R.string.yes))
                            .setCancelText(getString(R.string.cancel))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                                    pDialog.getProgressHelper().setBarColor(R.color.colorPrimaryDark);
                                    pDialog.setTitleText(getString(R.string.sending_request));
                                    pDialog.setCancelable(false);
                                    pDialog.show();

                                    _sendRequest(pDialog);
                                }
                            })
                            .show();

                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!hasPermission) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_WRITE_STORAGE);
                    return;
                }
                _saveImage();
            }
        });


        return rootView;
    }

    private void _saveImage() {
        MZUtil.downloadFile(getActivity(), mURL);
    }

    private void _sendRequest(final SweetAlertDialog pDialog) {

        requestParams.put("contact_no", contact.isEmpty() ? session.getUserPhone() : contact);

        for (int i=0; i<adapter.getItemCount(); i++) {
            MZDrawNumberItem item = adapter.getItem(i);
            requestParams.put("plate_type" + (i+1), item.getPlateType());
            requestParams.put("code" + (i+1), item.getLeftNum());
            requestParams.put("emirate" + (i+1), item.getCity());
            requestParams.put("price" + (i+1), item.getPrice()); // item.getAsk().contentEquals("1") ? item.getPrice() : "");
            requestParams.put("plate_num" + (i+1), item.getRightNum());
            requestParams.put("ask" + (i+1), item.getAsk());
        }


        drawCall = api.drawPlate(requestParams);
        drawCall.enqueue(new AGRetrofitCallback<AGApiResponse>() {
            @Override
            public void onResponse(Call<AGApiResponse> call, Response<AGApiResponse> response) {
                super.onResponse(call, response);

                if (response.isSuccessful()) {
                    AGApiResponse res = response.body();

                    if (getActivity() != null & isAdded()) {
                        pDialog.dismiss();

                        if (res.getImg() != null) {

                            mURL = res.getImg();
                            Glide.with(getActivity()).load(mURL).into(ivImg);
                            AGUtil.successAlert(getActivity(), "Requested Successfully!", res.getMsg());

                            ivImg.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            card.setVisibility(View.GONE);
                            btnSave.setVisibility(View.VISIBLE);

                        } else {
                            AGUtil.errorAlert(getActivity(), "Opps!", res.getMsg());
                        }
                    }
                }

                // TODO: Handle response
            }

            @Override
            public void onFailure(Call<AGApiResponse> call, Throwable t) {
                super.onFailure(call, t);
                pDialog.dismiss();
            }
        });
    }

    private void _addItem() {
        MZDrawNumberItem item = new MZDrawNumberItem();
        item.setType("plate");
        item.setPlateType(type);
        item.setAsk(ask);
        item.setPrice(price);
        item.setLeftNum(code);
        item.setRightNum(rightNum);
        item.setCity(emirate);
        adapter.add(item);
    }


    private void initSpinner(final Spinner spn, String file, final String param) {
        String data = AGUtil.loadJsonFile(file);

        List<MZJsonObj> items = new Gson().fromJson(data, new TypeToken<List<MZJsonObj>>() {}.getType());

        String[] spinnerArray = new String[items.size()];
        final HashMap<String, String> spinnerMap = new HashMap<String, String>();

        for (int x = 0; x<items.size(); x++)
        {
            MZJsonObj item = items.get(x);
            if (param.contentEquals("code")) {
                spinnerMap.put(item.getName(), item.getId());
                spinnerArray[x] = item.getName();
            } else {
                if (session.getLang().contentEquals("ar")) {
                    spinnerMap.put(item.getName_ae(), item.getId());
                    spinnerArray[x] = item.getName_ae();
                } else {
                    spinnerMap.put(item.getName_en(), item.getId());
                    spinnerArray[x] = item.getName_en();
                }
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn.setAdapter(adapter);
        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String itemName  = spn.getSelectedItem().toString();
                String itemCode  = spinnerMap.get(itemName);

                if (param.contentEquals("plate_type")) {
                    type = itemCode;
                }

                if(param.contentEquals("emirate")) {
                    emirate = itemCode;
                }

                if (param.contentEquals("code")) {
                    code = itemCode;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }


    @Override
    public void onRemove(View view, final int position) {
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.are_you_sure))
                .setContentText(getString(R.string.remove_item))
                .setConfirmText(getString(R.string.yes))
                .setCancelText(getString(R.string.cancel))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        adapter.remove(position);
                    }
                })
                .show();
    }

    /**
     * Save plate/number to disk
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    _saveImage();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.plz_enable_storage), Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
