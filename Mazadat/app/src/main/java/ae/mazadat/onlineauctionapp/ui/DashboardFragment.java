package ae.mazadat.onlineauctionapp.ui;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.rest.DashboardItem;
import ae.mazadat.onlineauctionapp.model.rest.MZSort;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class DashboardFragment extends Fragment {

    protected Dialog aboutDialog;
    AGApi api;
    Call<List<DashboardItem>> getDashboardCall;

    String lang = "";

    // Items
    @Bind(R.id.btn1)
    View btn1;

    @Bind(R.id.btn2)
    View btn2;

    @Bind(R.id.btn3)
    View btn3;

    @Bind(R.id.btn4)
    View btn4;

    @Bind(R.id.btn5)
    View btn5;

    @Bind(R.id.btn6)
    View btn6;

    @Bind(R.id.btn7)
    View btn7;

    @Bind(R.id.btn8)
    View btn8;

    @Bind(R.id.btn9)
    View btn9;

    @Bind(R.id.btn10)
    View btn10;

    @Bind(R.id.btn11)
    View btn11;

    SessionManager session = App.getSessionManager();

    final AnimatorSet mAnimationSet = new AnimatorSet();
    ObjectAnimator fadeIn;

    private static final int PERMISSIONS_REQUEST_PHONE_CALL = 100;
    private static String[] PERMISSIONS_PHONECALL = {Manifest.permission.CALL_PHONE};
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DashboardFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DashboardFragment newInstance(int columnCount) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);

        lang = session.getMZLang();

        initAnimation();
        api = App.getRestAdapter().create(AGApi.class);

        setItems();

        return view;
    }

    private void setItems() {

//        List<DashboardItem> items = session.getDashboard();
        List<DashboardItem> items = session.getDashboard();

        if (items != null && items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {
                drawItem(items.get(i));
            }

            animateItems();
        } else {
            // TODO: Error!!
//                        // TODO: Show proper message
            AGUtil.errorAlert(getActivity(), "No items!", "No dashboard Items!");
        }

    }

    private void animateItems() {
        int initDelay = 800;
        int speed = 550;
        btn5.animate().scaleX(1).scaleY(1).setStartDelay(initDelay).setDuration(speed).start();
        btn10.animate().scaleX(1).scaleY(1).setStartDelay(initDelay + 50).setDuration(speed).start();
        btn4.animate().scaleX(1).scaleY(1).setStartDelay(initDelay + 100).setDuration(speed).start();
        btn11.animate().scaleX(1).scaleY(1).setStartDelay(initDelay + 150).setDuration(speed).start();
        btn1.animate().scaleX(1).scaleY(1).setStartDelay(initDelay + 200).setDuration(speed).start();
        btn6.animate().scaleX(1).scaleY(1).setStartDelay(initDelay + 250).setDuration(speed).start();
        btn8.animate().scaleX(1).scaleY(1).setStartDelay(initDelay + 300).setDuration(speed).start();
        btn2.animate().scaleX(1).scaleY(1).setStartDelay(initDelay + 350).setDuration(speed).start();
        btn9.animate().scaleX(1).scaleY(1).setStartDelay(initDelay + 400).setDuration(speed).start();
        btn3.animate().scaleX(1).scaleY(1).setStartDelay(initDelay + 450).setDuration(speed).start();
        btn7.animate().scaleX(1).scaleY(1).setStartDelay(initDelay + 500).setDuration(speed).start();
    }

    private void initAnimation() {
        btn1.setScaleX(0);
        btn1.setScaleY(0);
        btn2.setScaleX(0);
        btn2.setScaleY(0);
        btn3.setScaleX(0);
        btn3.setScaleY(0);
        btn4.setScaleX(0);
        btn4.setScaleY(0);
        btn5.setScaleX(0);
        btn5.setScaleY(0);
        btn6.setScaleX(0);
        btn6.setScaleY(0);
        btn7.setScaleX(0);
        btn7.setScaleY(0);
        btn8.setScaleX(0);
        btn8.setScaleY(0);
        btn9.setScaleX(0);
        btn9.setScaleY(0);
        btn10.setScaleX(0);
        btn10.setScaleY(0);
        btn11.setScaleX(0);
        btn11.setScaleY(0);

//        fadeIn = ObjectAnimator.ofFloat(vCoordinator, "alpha", .0f, 1f);
//        fadeIn.setDuration(400);

    }

    private void drawItem(DashboardItem item) {

        switch (item.getId()) {
            case AGConf.CAT_CARS:
                drawCarsItem(item);
                break;

            case AGConf.CAT_PLATES:
                drawPlatesItem(item);
                break;

            case AGConf.CAT_DU:
                drawDuItem(item);
                break;

            case AGConf.CAT_HORSE:
                drawHorsesItem(item);
                break;

            case AGConf.CAT_COINS:
                drawMoneyItem(item);
                break;

            case AGConf.CAT_DIRECT_SELL:
                drawDirectSellItem(item);
                break;

            case AGConf.CAT_PROP:
                drawPropItem(item);
                break;

            case AGConf.CAT_SERVICES:
                drawServicesItem(item);
                break;

//            case AGConf.CAT_ADS: // HERE
//                drawMiscItem(item);
//                break;

            case AGConf.CAT_WATCH:
                drawWatchesItem(item);
                break;

            case AGConf.CAT_MISC:
                drawMiscItem(item);
                break;

            case AGConf.CAT_BAGS:
                drawBagsItem(item);
                break;

            default:
                return;
        }

    }

    private void drawPlatesItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn8.findViewById(R.id.img);
//        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_plates));
        _setItemInfo(item, btn8);
        vImg.setImageDrawable(null);
        Glide.with(getActivity()).load(item.getIcon()).into(vImg);

        TextView vName = (TextView) btn8.findViewById(R.id.name);
        vName.setText(getString(R.string.db_plates));

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);

//                BounceInterpolator bounceInterpolator = new BounceInterpolator();
//                ObjectAnimator anim = ObjectAnimator.ofFloat(v, "translation", 0f, 200 );
//                anim.setInterpolator(bounceInterpolator);
//                anim.setDuration(1100).start();

//
//                ScaleAnimation animation = new ScaleAnimation(v.getX(), v.getX(), v.getY(), v.getY(),
//                        Animation.RELATIVE_TO_SELF, 2.5f, Animation.RELATIVE_TO_SELF, 2.5f);
//                animation.setFillAfter(true);
//                animation.start();
//
                openAuction(item, view);
                //        Intent intent = new Intent(sourceActivity, ItemsActivity.class);
////        intent.putExtra(AGConf.KEY_AUCTION_ITEM, Parcels.wrap(item));
//        sourceActivity.startActivity(intent);

            }
        });
    }

    private void openAuction(DashboardItem item, View v) {
        int[] startingLocation = new int[2];
        v.getLocationOnScreen(startingLocation);
        startingLocation[0] += v.getWidth() / 2;
        startingLocation[1] += (v.getHeight() / 2) - 60;
        int color = 0xFFFFFFFF; // 0xFFD32F2F
        ItemsActivity.startFromLocation(startingLocation, getActivity(), item, color);
        getActivity().overridePendingTransition(0, 0);
    }

    private void openDirectSale(DashboardItem item, View v) {
        int[] startingLocation = new int[2];
        v.getLocationOnScreen(startingLocation);
        startingLocation[0] += v.getWidth() / 2;
        startingLocation[1] += (v.getHeight() / 2) - 80;
        int color = 0xFFD32F2F; // 0xFFD32F2F
        ItemsActivity.startFromLocation(startingLocation, getActivity(), item, color);
        getActivity().overridePendingTransition(0, 0);
    }


    private void openDu(DashboardItem item, View v) {
        int[] startingLocation = new int[2];
        v.getLocationOnScreen(startingLocation);
        startingLocation[0] += v.getWidth() / 2;
        startingLocation[1] += (v.getHeight() / 2) - 60;
        int color = 0xFF0099CC;
        ItemsActivity.startFromLocation(startingLocation, getActivity(), item, color);
        getActivity().overridePendingTransition(0, 0);
    }


    private void drawCarsItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn1.findViewById(R.id.img);
//        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_cars));
        vImg.setImageDrawable(null);
        Glide.with(getActivity()).load(item.getIcon()).into(vImg);
        _setItemInfo(item, btn1);

        TextView vName = (TextView) btn1.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_cars));

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);

                openAuction(item, view);
            }
        });
    }

    private void drawMiscItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn2.findViewById(R.id.img);
//        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_mix));
        vImg.setImageDrawable(null);
        Glide.with(getActivity()).load(item.getIcon()).into(vImg);
        _setItemInfo(item, btn2);

        TextView vName = (TextView) btn2.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_misc));
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);

                openAuction(item, view);
            }
        });
    }

    private void drawHorsesItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn4.findViewById(R.id.img);
//        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_horses));
        vImg.setImageDrawable(null);
        Glide.with(getActivity()).load(item.getIcon()).into(vImg);
        _setItemInfo(item, btn4);


        TextView vName = (TextView) btn4.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_horses));

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);

                openAuction(item, view);
            }
        });
    }

    private void drawPropItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn3.findViewById(R.id.img);
//        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_proprities));
        _setItemInfo(item, btn3);
        vImg.setImageDrawable(null);
        Glide.with(getActivity()).load(item.getIcon()).into(vImg);

        TextView vName = (TextView) btn3.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_prop));

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);
                openAuction(item, view);
            }
        });
    }

    private void drawServicesItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn7.findViewById(R.id.img);
//        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_services));
        vImg.setImageDrawable(null);
        Glide.with(getActivity()).load(item.getIcon()).into(vImg);
        _setItemInfo(item, btn7);

        View vBg = btn7.findViewById(R.id.bg);
        vBg.setBackground(getResources().getDrawable(R.drawable.db_black_item));

        TextView vName = (TextView) btn7.findViewById(R.id.name);
//        vName.setText(item.getAlias());
        vName.setText(item.getName());
//        vName.setText(getString(R.string.db_services));

        // TODO: Set child..
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);
                openAuction(item, view);
            }
        });
    }

    private void drawDirectSellItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn6.findViewById(R.id.img);
        TextView vBadge = (TextView) btn6.findViewById(R.id.badge);

        vBadge.setBackgroundResource(R.drawable.badge_primary);

        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_plates));

//        vImg.setImageDrawable(null);
        System.out.println("item.getIcon() " + item.getIcon());
//        Glide.with(getActivity()).load(item.getIcon()).into(vImg);

        _setItemInfo(item, btn6);
        View vBg = btn6.findViewById(R.id.bg);
        vBg.setBackground(getResources().getDrawable(R.drawable.db_red_item));

        TextView vName = (TextView) btn6.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_direct));

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDirectSale(item, view);
            }
        });
    }

    private void drawDuItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn5.findViewById(R.id.img);
//        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_du));
        vImg.setImageDrawable(null);
        Glide.with(getActivity()).load(item.getIcon()).into(vImg);
        _setItemInfo(item, btn5);

        View vBg = btn5.findViewById(R.id.bg);
        vBg.setBackground(getResources().getDrawable(R.drawable.db_du_item));

        TextView vName = (TextView) btn5.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_du));

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);
                openDu(item, view);
                //        Intent intent = new Intent(sourceActivity, ItemsActivity.class);
////        intent.putExtra(AGConf.KEY_AUCTION_ITEM, Parcels.wrap(item));
//        sourceActivity.startActivity(intent);

            }
        });
    }

    private void drawWatchesItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn9.findViewById(R.id.img);
//        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_watches));
        _setItemInfo(item, btn9);
        vImg.setImageDrawable(null);
        Glide.with(getActivity()).load(item.getIcon()).into(vImg);

        TextView vName = (TextView) btn9.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_watches));

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);

                openAuction(item, view);
            }
        });
    }

    private void drawMoneyItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn10.findViewById(R.id.img);
//        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_currency));
        _setItemInfo(item, btn10);
        vImg.setImageDrawable(null);
        Glide.with(getActivity()).load(item.getIcon()).into(vImg);

        TextView vName = (TextView) btn10.findViewById(R.id.name);
        vName.setText(item.getAlias());
//        vName.setText(getString(R.string.db_money));

        btn10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);

                openAuction(item, view);
            }
        });
    }

    private void drawBagsItem(final DashboardItem item) {
        ImageView vImg = (ImageView) btn11.findViewById(R.id.img);
//        vImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_bags));
        vImg.setImageDrawable(null);
        Glide.with(getActivity()).load(item.getIcon()).into(vImg);
        _setItemInfo(item, btn11);

        TextView vName = (TextView) btn11.findViewById(R.id.name);
        vName.setText(item.getName());
//        vName.setText(getString(R.string.db_bags));

        btn11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = v.findViewById(R.id.img);
//                openAuction(item, view);
                aboutDialogShow(item, view);
            }
        });

    }

    protected void aboutDialogShow(DashboardItem item, View view) {
        // TODO: Show default selected sort
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.about_dialog);
        dialog.setTitle("About Dialog");
//        LayoutInflater inflater = (LayoutInflater) getActivity()
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        View v = inflater.inflate(R.layout.about_dialog, null);
        dialog.show();
        final TextView mob = (TextView) dialog.findViewById(R.id.mobileTV);
        final TextView email = (TextView) dialog.findViewById(R.id.emailTV);
        mob.setText("600565654");
        email.setText("Info@mazadat.ae");
        mob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call();
                dialog.dismiss();
            }
        });
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ "Info@mazadat.ae"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                emailIntent.setType("message/rfc822");
                startActivity(emailIntent);
                dialog.dismiss();
            }
        });


        // Buttons
        Button btnCancel = (Button) dialog.findViewById(R.id.btcCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_PHONE_CALL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                call();
            } else {
                Toast.makeText(getActivity(), "Sorry!!! Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void call() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PERMISSIONS_REQUEST_PHONE_CALL);
        } else {
            //Open call function
            String number = "600565654";
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + number));
            startActivity(intent);
        }
    }

    private void _setItemInfo(DashboardItem item, View btn) {

//        TextView vName  = (TextView) btn.findViewById(R.id.name);
        TextView vBadge = (TextView) btn.findViewById(R.id.badge);


//        vName.setText(getString(R.string.db_plates));

        if (item.getTotal() > 0) {
            vBadge.setText(item.getTotal() + "");
        } else {
            vBadge.setVisibility(View.GONE);
        }
    }


    private void getDashboardItems() {

        getDashboardCall = api.getDashboard(lang);
        getDashboardCall.enqueue(new AGRetrofitCallback<List<DashboardItem>>() {
            @Override
            public void onResponse(Call<List<DashboardItem>> call, Response<List<DashboardItem>> response) {
                super.onResponse(call, response);

                if (response.isSuccessful()) {
                    List<DashboardItem> items = response.body();

                    session.saveDashboard(items);

                    // Update number of items
                    for (int i=0; i<items.size(); i++) {
                        drawItem(items.get(i));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<DashboardItem>> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getDashboardItems();
    }
}
