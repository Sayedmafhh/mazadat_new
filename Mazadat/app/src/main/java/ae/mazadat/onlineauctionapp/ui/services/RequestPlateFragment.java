package ae.mazadat.onlineauctionapp.ui.services;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZJsonObj;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.MZUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGApiResponse;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class RequestPlateFragment extends Fragment {

    protected AGApi api;
    protected Call<AGApiResponse> requestCall;

    @Bind(R.id.spnType)
    Spinner spnType;

    @Bind(R.id.spnEmirate)
    Spinner spnEmirate;

    @Bind(R.id.spnCode)
    Spinner spnCode;

    @Bind(R.id.btnRequest)
    Button btnRequest;

    @Bind(R.id.tvPltR)
    TextView tvPltR;

    @Bind(R.id.tvPltL)
    TextView tvPltL;

    @Bind(R.id.ivPlate)
    ImageView ivPlate;

    @Bind(R.id.etNumber)
    EditText etNumber;

    SessionManager session = App.getSessionManager();

    Map<String, String> requestParams = new HashMap<>();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RequestPlateFragment() {
    }


    @SuppressWarnings("unused")
    public static RequestPlateFragment newInstance(int columnCount) {
        RequestPlateFragment fragment = new RequestPlateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_request_plate, container, false);

        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(true);

        api = App.getRestAdapter().create(AGApi.class);

        initSpinner(spnType,    "plate_types.json",    "plate_type");
        initSpinner(spnEmirate, "emirates.json",    "emirate");
        initSpinner(spnCode,    "codes.json",    "code");

        requestParams.put("lang", session.getMZLang());

        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvPltR.setText(s);
                requestParams.put("plate_num", s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });


        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etNumber.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), getString(R.string.err_no_number), Toast.LENGTH_SHORT).show();
                } else {

                    final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(R.color.colorPrimaryDark);
                    pDialog.setTitleText(getString(R.string.sending_request));
                    pDialog.setCancelable(false);
                    pDialog.show();

                    requestCall = api.requestPlate(requestParams);
                    requestCall.enqueue(new AGRetrofitCallback<AGApiResponse>() {
                        @Override
                        public void onResponse(Call<AGApiResponse> call, Response<AGApiResponse> response) {
                            super.onResponse(call, response);

                            if (response.isSuccessful()) {
                                if (isAdded() && getActivity() != null) {
                                    AGApiResponse res = response.body();

                                    pDialog.dismiss();

                                    if (res.getStatus() != null && res.getStatus().contentEquals("200")) {
                                        AGUtil.successAlert(getActivity(), "Requested Successfully!", res.getMsg());
                                    } else {
                                        AGUtil.errorAlert(getActivity(), "Opps!", res.getMsg());
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<AGApiResponse> call, Throwable t) {
                            super.onFailure(call, t);
                            if (isAdded() && getActivity() != null) {
                                pDialog.dismiss();
                            }
                        }
                    });
                }
            }
        });

        return rootView;
    }


    private void initSpinner(final Spinner spn, String file, final String param) {
        String data = AGUtil.loadJsonFile(file);

        List<MZJsonObj> items = new Gson().fromJson(data, new TypeToken<List<MZJsonObj>>() {}.getType());

        String[] spinnerArray = new String[items.size()];
        final HashMap<String, String> spinnerMap = new HashMap<String, String>();

        for (int x = 0; x<items.size(); x++)
        {
            MZJsonObj item = items.get(x);
            if (param.contentEquals("code")) {
                spinnerMap.put(item.getName(), item.getId());
                spinnerArray[x] = item.getName();
            } else {
                if (session.getLang().contentEquals("ar")) {
                    spinnerMap.put(item.getName_ae(), item.getId());
                    spinnerArray[x] = item.getName_ae();
                } else {
                    spinnerMap.put(item.getName_en(), item.getId());
                    spinnerArray[x] = item.getName_en();
                }
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn.setAdapter(adapter);
        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String itemName  = spn.getSelectedItem().toString();
                String itemCode  = spinnerMap.get(itemName);

                requestParams.put(param, itemCode);

                if (param.contentEquals("code")) {
                    tvPltL.setText(itemName);
                }

                if (param.contentEquals("emirate")) {
                    ivPlate.setImageDrawable(MZUtil.getPlateDrawable(Integer.parseInt(itemCode)));
//                    if (itemCode.contentEquals("10")) { // Abu Dhabi
//                        tvPltL.setTextColor(getActivity().getResources().getColor(R.color.white));
//                    } else {
//                        tvPltL.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
//                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }


}
