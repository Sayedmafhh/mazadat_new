package ae.mazadat.onlineauctionapp.ui.auction;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;
import java.util.List;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.NumbersAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.MZFilter;
import ae.mazadat.onlineauctionapp.model.rest.MZOption;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link ItemsListFragment.OnMZItemInteractionListener}
 * interface.
 */
public class NumbersFragment extends ItemsListFragment {

    private boolean isTabInitiated = false;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NumbersFragment() {
    }


    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static NumbersFragment newInstance(int columnCount) {
        NumbersFragment fragment = new NumbersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_du_list, container, false);

        setHasFab(true);
        setHasLoadMore(true);

        ButterKnife.bind(this, rootView);

        setHasOptionsMenu(false);

        init(getActivity(), true);

        adapter = new NumbersAdapter(getActivity(), mListener,isActivatedBid);

        recyclerView.setAdapter(adapter);

        iniFirebase(AGConf.FIREBASE_DU_URL);

        if (savedInstanceState == null) {
            loadItems();
        }
        System.out.println("child "+adapter.getItems().size());
        return rootView;
    }

    @Override
    public void serviceLoadItems(final boolean reset) {
        super.serviceLoadItems(reset);

        filterParams.put("lang",  session.getMZLang());
        filterParams.put("limit", String.valueOf(ITEMS_PER_PAGE));
        filterParams.put("offset", String.valueOf(currentPage * ITEMS_PER_PAGE));
        filterParams.put("development", String.valueOf(1));
        gettingItemsCall = api.getNumbers(filterParams);
        gettingItemsCall.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {

                if (response.isSuccessful()) {
                    MZResponse MZResponse = response.body();
//                    System.out.println("response ***"+MZResponse.getItems().get(0).getDuPackageDetails());
                    Date currentTime = MZResponse.getCurrentTime();
                    long timeDiff;
                    if (currentTime != null) {
                        timeDiff = System.currentTimeMillis() - currentTime.getTime();
                    } else {
                        timeDiff = 0;
                    }

                    int deposit      = MZResponse.getUserDeposit();
                    List<MZItem> items = MZResponse.getItems();

                    mFilters = MZResponse.getFilters();
                    mSorts   = MZResponse.getSorts();
                    universalMsgs = MZResponse.getUniversalMsgs();
                    isActivatedBid = MZResponse.isActivate_bid();

                    // Set top message textviews: Niraj Kumar
                    if (MZResponse.isActivate_bid()) {
                        if (isTabInitiated == false) {
                            _initTabs();
                            isTabInitiated = true;
                        }

                        txtTopMessage.setText(MZResponse.getTopMessage());
                        if (MZResponse.getUniversalMsgs() != null) {
                            txtVenue.setText((String) (MZResponse.getUniversalMsgs().get(0).getValue()));
                            txtDateTime.setText((String) (MZResponse.getUniversalMsgs().get(1).getValue()));
                        }

//                        Log.i("UMSG", "" + (((Map<String, Object>)MZResponse.getUniversalMsgs().get(3).getValue()).get("lat")));
                    }else{
                        tabsLayout.setVisibility(View.GONE);
                        txtTopMessage.setVisibility(View.GONE);
                        txtVenue.setVisibility(View.GONE);
                        txtDateTime.setVisibility(View.GONE);
                    }
                    //////////////////////////////////////////

                    // TODO: When failed - this may throw an error!

                    _initFiltersAndSorts();

                    doWhenSuccessLoad(reset, items);
                    session.setDuDeposit(deposit);

                    if (items != null) {
                        if (items.size() > 0) {
                            for (int i=0; i<items.size(); i++) {
                                MZItem item = items.get(i);
                                item.setOnlineAuction(!(MZResponse.isActivate_bid()));
//                            item.setUserDeposit(deposit);
                                item.setCurrentTime(currentTime);
                                item.setTimeDiff(timeDiff);
                                // Set current package details
                                String pkgId = item.getDetails().getPackageId();
                                if (pkgId != null && mFilters.size() > 0) {
                                    for (MZFilter filter : mFilters) {
                                        if (filter.getName().contentEquals("packages")) {
                                            for (MZOption option : filter.getOptions()) {
                                                if (option.getId().contentEquals(pkgId)) {
                                                    item.setDuPackageName(option.getName());
                                                    item.setDuPackageDetails(option.getDetails());
                                                }
                                            }
                                        }
                                    }
                                }

                                if (session.getUid() > 0 && item.getBidder() != null && item.getBidder().contentEquals(session.getUid() + "")) {
                                    item.setMyBid(true);
                                }

                                adapter.add(item);
                            }
                        } else {
                            initNoItems();
                        }
                    }
                } else {
                    doWhenFailureApiCall("Opps!", "Unable to load items!");
                }
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {

            }
        });
    }


    public void doWhenSuccessLoad(boolean reset, List<MZItem> items) {
        super.doWhenSuccessLoad(reset);

        if (getActivity() != null && isAdded()) {
            if (reset) {
                adapter.clear();
            } else {
                // Remove latest element (as it's the (load more) loader)
//                if (adapter.getItemCount() > 0) {
//                    adapter.remove(adapter.getItemCount() - 1);
//                }
            }

            // No more content to load
            if (items != null && (items.isEmpty() || items.size() < 25)) { // TODO: Check number!
                moreContentToLoad = false;
            }
        }
    }

    public void doWhenFailureApiCall(String title, String errorMessage) {
        super.doWhenFailureApiCall(title, errorMessage);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.startUpdateTimer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        adapter.cancelUpdateTimer();
    }

    @Override
    protected void registerPN() {
        regParams.put("category", String.valueOf(AGConf.CAT_DU));
        _registerPN();
    }

}
