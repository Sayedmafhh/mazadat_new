package ae.mazadat.onlineauctionapp.ui;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.parceler.Parcels;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.MZValue;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sudhirkumar on 4/10/2017.
 */

public class DetailActivityNew extends BaseActivity {

    MZItem mItem;
    String currentAuction = "";
    @Bind(R.id.backBtn)
    ImageView backBtn;
    @Bind(R.id.container)
    LinearLayout container;
    LayoutInflater li;
    String mType;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

        setContentView(R.layout.activity_detail_new);
        ButterKnife.bind(this);
        String auctionType = getIntent().getStringExtra(AGConf.KEY_AUCTION_TYPE);
        Parcelable data = getIntent().getParcelableExtra(AGConf.KEY_ITEM_DATA);
        mItem = Parcels.unwrap(data);
//        mType = getIntent().getStringExtra(AGConf.KEY_AUCTION_TYPE);

        mType = auctionType;
        li = LayoutInflater.from(this);

//        if (mType != null && mType.contentEquals(AGConf.KEY_CARS_AUCTION) && mItem.getDetails().getNotes() != null) {
        // TODO: Format it
//            tvNotes.setText(Html.fromHtml(mItem.getDetails().getNotes()));
//        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        // NO need for && mType.contentEquals(AGConf.KEY_CARS_AUCTION)
        if (mType != null && mItem.getDetails().getValues() != null
                && mItem.getDetails().getValues().size() > 0) {

            for (MZValue item : mItem.getDetails().getValues()) {
                _drawItem(item.getLabel(), item.getValue(), false);
            }
        }
    }
    private void _drawItem(String lbl, String val, boolean hideHr) {
        View v = li.inflate(R.layout.lbl_val, null);
        TextView tvLbl = (TextView) v.findViewById(R.id.tvLbl);
        TextView tvVal = (TextView) v.findViewById(R.id.tvVal);

        tvLbl.setText(lbl);
        tvVal.setText(val);

        if (hideHr) {
            View hr = v.findViewById(R.id.hr);
            hr.setVisibility(View.GONE);
        }

        container.addView(v);
    }
}
