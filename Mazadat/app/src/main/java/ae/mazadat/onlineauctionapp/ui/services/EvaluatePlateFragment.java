package ae.mazadat.onlineauctionapp.ui.services;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZJsonObj;
import ae.mazadat.onlineauctionapp.ui.payment.PaymentWebActivity;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.MZUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class EvaluatePlateFragment extends Fragment {

    @Bind(R.id.spnType)
    Spinner spnType;

    @Bind(R.id.spnEmirate)
    Spinner spnEmirate;

    @Bind(R.id.spnCode)
    Spinner spnCode;

    @Bind(R.id.btnRequest)
    Button btnRequest;

    @Bind(R.id.tvPltR)
    TextView tvPltR;

    @Bind(R.id.tvPltL)
    TextView tvPltL;

    @Bind(R.id.ivPlate)
    ImageView ivPlate;

    @Bind(R.id.etNumber)
    EditText etNumber;

    @Bind(R.id.tvInfo)
    TextView tvInfo;

    SessionManager session = App.getSessionManager();

    Map<String, String> requestParams = new HashMap<>();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public EvaluatePlateFragment() {
    }


    @SuppressWarnings("unused")
    public static EvaluatePlateFragment newInstance(int columnCount) {
        EvaluatePlateFragment fragment = new EvaluatePlateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_request_plate, container, false);

        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(true);

        tvInfo.setVisibility(View.VISIBLE);
        tvInfo.setText(getString(R.string.info_evaluate_fees));

        btnRequest.setText(getString(R.string.confirm_request));

        initSpinner(spnType,    "plate_types.json",    "type");
        initSpinner(spnEmirate, "emirates.json",    "emirate");
        initSpinner(spnCode,    "codes.json",    "code");

        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvPltR.setText(s);
                requestParams.put("plate_num", s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });


        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etNumber.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), getString(R.string.err_no_number), Toast.LENGTH_SHORT).show();
                } else {
                    PaymentWebActivity.startEvaluatePlatePayment(AGConf.PAYMENT_URL + "?lang=" + session.getMZLang()
                            + "&type="+ requestParams.get("type")
                            + "&emirate=" + requestParams.get("emirate")
                            + "&code=" + requestParams.get("code")
                            + "&plate_num=" + requestParams.get("plate_num")
                            + "&amount=200", getActivity());
                }
            }
        });

        return rootView;
    }


    private void initSpinner(final Spinner spn, String file, final String param) {
        String data = AGUtil.loadJsonFile(file);

        List<MZJsonObj> items = new Gson().fromJson(data, new TypeToken<List<MZJsonObj>>() {}.getType());

        String[] spinnerArray = new String[items.size()];
        final HashMap<String, String> spinnerMap = new HashMap<String, String>();

        for (int x = 0; x<items.size(); x++)
        {
            MZJsonObj item = items.get(x);
            if (param.contentEquals("code")) {
                spinnerMap.put(item.getName(), item.getId());
                spinnerArray[x] = item.getName();
            } else {
                if (session.getLang().contentEquals("ar")) {
                    spinnerMap.put(item.getName_ae(), item.getId());
                    spinnerArray[x] = item.getName_ae();
                } else {
                    spinnerMap.put(item.getName_en(), item.getId());
                    spinnerArray[x] = item.getName_en();
                }
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn.setAdapter(adapter);
        spn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String itemName  = spn.getSelectedItem().toString();
                String itemCode  = spinnerMap.get(itemName);
                requestParams.put(param, itemCode);

                if (param.contentEquals("code")) {
                    tvPltL.setText(itemName);
                }

                if (param.contentEquals("emirate")) {
                    ivPlate.setImageDrawable(MZUtil.getPlateDrawable(Integer.parseInt(itemCode)));
//                    if (itemCode.contentEquals("10")) { // Abu Dhabi
//                        tvPltL.setTextColor(getActivity().getResources().getColor(R.color.white));
//                    } else {
//                        tvPltL.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
//                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }


}
