package ae.mazadat.onlineauctionapp.viewholder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import butterknife.Bind;

/**
 * Created by agile on 4/1/16.
 */
public class GridItemViewHolder extends ItemViewHolder {

    @Bind(R.id.ivBg) public ImageView ivBg;
    @Bind(R.id.tvBids) public TextView tvBids;
    @Bind(R.id.tvItemName) public TextView tvModel;
    @Bind(R.id.tvPrice) public TextView tvPrice;
//    @Bind(R.layout.car_price_counter) public ViewGroup car_price_layout;


    public GridItemViewHolder(View view) {
        super(view);
        auctionType = AGConf.KEY_AUCTION_GRID;
    }
}
