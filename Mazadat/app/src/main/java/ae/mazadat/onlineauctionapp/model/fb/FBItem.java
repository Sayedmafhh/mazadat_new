package ae.mazadat.onlineauctionapp.model.fb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by agile on 4/6/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FBItem {

    int   id;
    String end_time;
    String current_price;
    int   bids;
    FBLocation location;
    String min_bid;
    String user;

    public FBItem() {
    }

    public int getId() {
        return id;
    }

    public String getEnd_time() {
        return end_time;
    }

    public String getCurrent_price() {
        return current_price;
    }

    public int getBids() {
        return bids;
    }

    public FBLocation getLocation() {
        return location;
    }

    public String getMin_bid() {
        return min_bid;
    }

    public String getUser() {
        return user;
    }
}

