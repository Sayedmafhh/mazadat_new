package ae.mazadat.onlineauctionapp.ui.auction;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;
import java.util.List;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.PlatesAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link ItemsListFragment.OnMZItemInteractionListener}
 * interface.
 */
public class PlatesFragment extends ItemsListFragment {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PlatesFragment() {
    }


    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PlatesFragment newInstance(int columnCount) {
        PlatesFragment fragment = new PlatesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_base_list, container, false);

        setHasFab(true);
        setHasLoadMore(true);

        ButterKnife.bind(this, rootView);

        setHasOptionsMenu(false);

        init(getActivity());

        adapter = new PlatesAdapter(getActivity(), mListener);
        recyclerView.setAdapter(adapter);

        iniFirebase(AGConf.FIREBASE_PLATES_URL);

        if (savedInstanceState == null) {
            loadItems();
        }

        return rootView;
    }

    @Override
    public void serviceLoadItems(final boolean reset) {
        super.serviceLoadItems(reset);

        filterParams.put("lang", session.getMZLang());
        filterParams.put("limit", String.valueOf(ITEMS_PER_PAGE));
        filterParams.put("offset", String.valueOf(currentPage * ITEMS_PER_PAGE));
        filterParams.put("development", String.valueOf(1));
        gettingItemsCall = api.getPlates(filterParams);
        gettingItemsCall.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {

                if (response.isSuccessful()) {
                    MZResponse MZResponse = response.body();
                    Log.i("Is Online Bid", "Is online bid: " + (MZResponse.isActivate_bid() ? "YES" : "NO") );
                    Date currentTime = MZResponse.getCurrentTime();
                    long timeDiff;
                    if (currentTime != null) {
                        timeDiff = System.currentTimeMillis() - currentTime.getTime();
                    } else {
                        timeDiff = 0;
                    }

                    int deposit      = MZResponse.getUserDeposit();
                    List<MZItem> items = MZResponse.getItems();

                    mFilters = MZResponse.getFilters();
                    mSorts   = MZResponse.getSorts();

                    // TODO: When failed - this may throw an error!

                    _initFiltersAndSorts();

                    doWhenSuccessLoad(reset, items);

                    session.setPlatesDeposit(deposit);

                    if (items != null && items.size() > 0) {
                        for (int i=0; i<items.size(); i++) {
                            MZItem item = items.get(i);
                            item.setOnlineAuction(!(MZResponse.isActivate_bid()));
//                            item.setUserDeposit(deposit);
                            item.setCurrentTime(currentTime);
                            item.setTimeDiff(timeDiff);
                            if (session.getUid() > 0 && item.getBidder() != null && item.getBidder().contentEquals(session.getUid() + "")) {
                                item.setMyBid(true);
                            }
                            adapter.add(item);
                        }

//                        if (hasLoadMore && moreContentToLoad) {
//                            adapter.add(null);
//                        }

                    } else {
                        initNoItems();
                    }

                } else {
                    doWhenFailureApiCall("Opps!", "Unable to load items!");
                    AGUtil.errorAlert(getActivity(), "No items!", "Unable to load items!");

                }
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {
                doWhenFailureApiCall("Opps!", "Unable to load items!");
            }
        });
    }


    public void doWhenSuccessLoad(boolean reset, List<MZItem> items) {
        super.doWhenSuccessLoad(reset);

        if (getActivity() != null && isAdded()) {
            if (reset) {
                adapter.clear();
            } else {
                // Remove latest element (as it's the (load more) loader)
//                if (adapter.getItemCount() > 0) {
//                    adapter.remove(adapter.getItemCount() - 1);
//                }
            }

            // No more content to load
            if (items != null && (items.isEmpty() || items.size() < 25)) { // TODO: Check number!
                moreContentToLoad = false;
            }

        }
    }

    public void doWhenFailureApiCall(String title, String errorMessage) {
        super.doWhenFailureApiCall(title, errorMessage);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.startUpdateTimer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        adapter.cancelUpdateTimer();
    }

    @Override
    protected void registerPN() {
        regParams.put("category", String.valueOf(AGConf.CAT_PLATES));
        _registerPN();
    }

}
