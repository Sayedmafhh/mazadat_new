package ae.mazadat.onlineauctionapp.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import org.parceler.Parcels;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.gallery.PhotosPagerAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.MZItemDetails;
import ae.mazadat.onlineauctionapp.model.fb.FBItem;
import ae.mazadat.onlineauctionapp.model.rest.Deposit;
import ae.mazadat.onlineauctionapp.model.rest.MZDirectItem;
import ae.mazadat.onlineauctionapp.model.rest.MZDirectTotal;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.model.rest.MZValue;
import ae.mazadat.onlineauctionapp.model.rest.UniversalMsg;
import ae.mazadat.onlineauctionapp.ui.details.tab.BidFragment;
import ae.mazadat.onlineauctionapp.ui.details.tab.DepositFragment;
import ae.mazadat.onlineauctionapp.ui.details.tab.DetailsFragment;
import ae.mazadat.onlineauctionapp.ui.details.tab.DetailsFragment_New;
import ae.mazadat.onlineauctionapp.ui.details.tab.LocationFragment;
import ae.mazadat.onlineauctionapp.ui.details.tab.NotesFragment;
import ae.mazadat.onlineauctionapp.ui.details.tab.PackageFragment;
import ae.mazadat.onlineauctionapp.ui.payment.PaymentWebActivity;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.MZUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class ItemDetailsFragment extends Fragment {

    AGApi api;
    Call<Deposit> getDepositCall;
    Call<MZResponse> getDetailsItems;
    protected List<UniversalMsg> universalMsgs;

    protected Firebase fbRef;
    @Nullable
    @Bind(R.id.date)
    TextView date;
    @Nullable
    @Bind(R.id.bids)
    TextView mBids;

    // Plates
    @Nullable
    @Bind(R.id.tvPltL)
    TextView tvPltL;

    @Nullable
    @Bind(R.id.tvPltR)
    TextView tvPltR;

    @Nullable
    @Bind(R.id.ivPlate)
    ImageView ivPlate;

    // Du
    @Nullable
    @Bind(R.id.tvPrefix)
    TextView tvPrefix;

    @Nullable
    @Bind(R.id.tvNumber)
    TextView tvNumber;

    @Nullable
    @Bind(R.id.tvMotocycle)
    TextView tvMotocycle;

    MZItem mItem;
    //MZItemDetails mzItemDetails;

    Deposit deposit; // API Call
    SessionManager session = App.getSessionManager();
    MZItemDetails mItemDetails;
    @Nullable
    @Bind(R.id.ivNumber)
    ImageView ivNumber;
    @Nullable
    @Bind(R.id.tvLblBids)
    TextView tvLblBids;
    @Nullable
    @Bind(R.id.tvLblExpiry)
    TextView tvLblExpiry;
    @Nullable
    @Bind(R.id.tvPkgName)
    TextView tvPkgName;
    @Nullable
    @Bind(R.id.tvPrice)
    TextView tvPrice;
    @Nullable
    @Bind(R.id.tvNotes)
    TextView tvNotes;
    @Nullable
    @Bind(R.id.tvCartSubTotal)
    TextView tvCartSubTotal;
    @Nullable
    @Bind(R.id.btnDetails)
    TextView btnDetails;
    @Nullable
    @Bind(R.id.btnNotes)
    TextView btnNotes;
    @Nullable
    @Bind(R.id.btnLocation)
    TextView btnLocation;
    @Nullable
    @Bind(R.id.RLPkgBenefit)
    LinearLayout RLPkgBenefit;
    @Nullable
    @Bind(R.id.vHr)
    FrameLayout vHr;
    @Nullable
    @Bind(R.id.gallery)
    FrameLayout gallery;
    @Nullable
    @Bind(R.id.container)
    LinearLayout container;
    @Nullable
    @Bind(R.id.scrollviewLinearLAyout)
    LinearLayout scrollviewLinearLAyout;
    //    @Bind(R.id.tvPrefix)
//    TextView tvPrefix;

    @Nullable
    @Bind(R.id.layoutPkgBenefits)
    LinearLayout layoutPkgBenefits;
    @Nullable
    @Bind(R.id.layoutPrice)
    LinearLayout layoutPrice;
    @Nullable
    @Bind(R.id.layoutNotes)
    LinearLayout layoutNotes;
    @Nullable
    @Bind(R.id.layoutcartSubtotal)
    LinearLayout layoutcartSubtotal;
    @Nullable
    @Bind(R.id.tvCarAdminFee)
    TextView tvCarAdminFee;
    @Nullable
    @Bind(R.id.tvCommission)
    TextView tvCommission;
    @Nullable
    @Bind(R.id.tvTotal)
    TextView tvTotal;
    @Nullable
    @Bind(R.id.tvElectricfee)
    TextView tvElectricfee;

    //    @Bind(R.id.buyNowBtn)
//    Button buyNowBtn;
    LayoutInflater li;

    @Nullable
    @Bind(R.id.duNumber)
    RelativeLayout duNumber;
    @Nullable
    @Bind(R.id.lbls)
    LinearLayout lbls;

    /* Tabs */
    private TabsAdapter mTabsAdapter;
    @Nullable
    @Bind(R.id.pager)
    ViewPager mViewPager;
    @Nullable
    @Bind(R.id.tabs)
    TabLayout tabLayout;
    Call<MZDirectItem> getDetailsCall;
    // Cars

    @Nullable
    @Bind(R.id.picPager)
    ViewPager picPager;

    @Nullable
    @Bind(R.id.indicator)
    CircleIndicator indicator;
    private Button buyNowBtn;

    private TextView pkgBenefitBtn;

    private String currentAuction = AGConf.KEY_PLATES_AUCTION;
    private String fbURL = "";
    @Nullable
    @Bind(R.id.btnBuy)
    Button btnBuy;
    //    int price        = 0;
//    int adminFees    = 0; // Administration Fees"
//    int transferFees = 0; // Plate Ownership Transfer fees
    int totalPrice = 0;
    int commission = 0;
    int carAdmin = 0;
    int electricFee = 0;
    int subTotal = 0;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailsFragment() {
    }


    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ItemDetailsFragment newInstance(int columnCount) {
        ItemDetailsFragment fragment = new ItemDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getDepositCall != null) {
            getDepositCall.cancel();
        }
        ButterKnife.unbind(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            Parcelable data = getArguments().getParcelable(AGConf.KEY_ITEM_DATA);
            Parcelable datadetails = getArguments().getParcelable(AGConf.KEY_ITEM_DATA_DETAILS);
            if (data != null) {
                mItem = Parcels.unwrap(data);
                //mzItemDetails = Parcels.unwrap(datadetails);

                System.out.println("***********" + mItem.getDetails());
             //   mItemDetails
            }

            String auction = getArguments().getString(AGConf.KEY_AUCTION_TYPE);
            if (auction != null) {
                currentAuction = auction;
            }
        }

    }

    protected void iniFirebase(String url) {
        fbRef = new Firebase(url);
        fbRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                FBItem item = dataSnapshot.getValue(FBItem.class);

                if (item.getId() == mItem.getId()) {
                    // This is the changed item
                    updateItem(mItem, item);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    protected void updateItem(MZItem mzItem, FBItem item) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 2016-06-14 16:00:00

        Date end_time = new Date();
        try {
            end_time = format.parse(item.getEnd_time());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!end_time.toString().contentEquals(mzItem.getEndTime().toString())) {

            mzItem.setEndTime(end_time); // TODO: SET properly

            mItem = mzItem;

            if (getActivity() != null) {

                date.setText(AGUtil.formatMazad(mItem.getEndTime()));
            }
        }

        if (item.getBids() != mzItem.getBids()) {
            AGUtil.bid(getActivity());
            mzItem.setPrice(Integer.parseInt(item.getCurrent_price()));
            mzItem.setBids(item.getBids());

            mzItem.setBided(true);

            boolean mMyBid = false;
            if (session.getUid() > 0 && item.getUser().contentEquals(session.getUid() + "")) {
                mzItem.setMyBid(true);
                mMyBid = true;
            } else {
                mzItem.setMyBid(false);
            }

            mItem = mzItem;

            if (getActivity() != null) {

                // is it me ?
//                boolean isMyBid = ((ItemDetailsActivity)getActivity()).isMyBid;

                if (mMyBid) {
                    MZUtil.doWhenSuccessBid(getActivity(), getString(R.string.bid_success_title));
                } else {
                    // Update UI, Hide bid button if I'm the bidder
                    mBids.setText(getActivity().getString(R.string.bids_no, mzItem.getBids()));
                    date.setText(AGUtil.formatMazad(mItem.getEndTime()));
                    _initTabs();
                }
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView;
        li = LayoutInflater.from(getActivity());
        String title = "";
//        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
//        pDialog.getProgressHelper().setBarColor(R.color.colorPrimaryDark);
//        pDialog.setTitleText(getString(R.string.loading_deposit));
        pDialog.setMessage(getString(R.string.loading_deposit));
        pDialog.setCancelable(false);

        MZItemDetails details = mItem.getDetails();
     //   MZItemDetails details = mzItemDetails;

        System.out.println("item *** " + mItem.toString());

        if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_du_details_new, container, false);
                pDialog.show();
                pkgBenefitBtn = (TextView) rootView.findViewById(R.id.pkgBenefitBtn);
            } else {
                rootView = inflater.inflate(R.layout.fragment_du_details, container, false);
            }
            fbURL = AGConf.FIREBASE_DU_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_grid_details_new, container, false);
                pDialog.show();

            } else {
                rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            }
            fbURL = AGConf.FIREBASE_CARS_URL;

            if (details.getMake() != null && details.getModel() != null) {
                title += details.getMake() + " " + details.getModel();
            }

            if (details.getYear() != null) {
                title += " " + details.getYear();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_plate_details_new, container, false);
                pDialog.show();
                pkgBenefitBtn = (TextView) rootView.findViewById(R.id.pkgBenefitBtn);
            } else {
                rootView = inflater.inflate(R.layout.fragment_plate_details, container, false);
            }
            fbURL = AGConf.FIREBASE_PLATES_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_BAGS_AUCTION)) {

            rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            fbURL = AGConf.FIREBASE_BAGS_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_grid_details_new, container, false);
                pDialog.show();

            } else {
                rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            }
            fbURL = AGConf.FIREBASE_JEWELLERY_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_PROP_AUCTION)) {

            rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);

            fbURL = AGConf.FIREBASE_PROPERTIES_URL;

            if (details.getName() != null) {
                title = details.getName();
            }

        } else if (currentAuction.contentEquals(AGConf.KEY_MISC_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                rootView = inflater.inflate(R.layout.fragment_grid_details_new, container, false);
                pDialog.show();

            } else {
                rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);
            }
            fbURL = AGConf.FIREBASE_MIX_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_HORSES_AUCTION)) {

            rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);

            fbURL = AGConf.FIREBASE_HORSES_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_COINS_AUCTION)) {

            rootView = inflater.inflate(R.layout.fragment_grid_details, container, false);

            fbURL = AGConf.FIREBASE_COINS_URL;

            if (details.getName() != null) {
                title = details.getName();
            }
        } else {
            rootView = null;
        }

        ButterKnife.bind(this, rootView);

        getActivity().setTitle(title);

        mTabsAdapter = new TabsAdapter(getActivity().getSupportFragmentManager());

        if (session.isLoggedIn()) {
            _loadDeposit();
        } else {
            _initTabs();
         //   _loadTabs();

        }

        mBids.setText(getActivity().getString(R.string.bids_no, mItem.getBids()));


        if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
            _setDu(details);
        } else if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
            _setPlate(details);
        }

        date.setText(AGUtil.formatMazad(mItem.getEndTime()));

        iniFirebase(fbURL);

        // Grid
        if (picPager != null && indicator != null) {
            picPager.setAdapter(new PhotosPagerAdapter(getActivity(), mItem.getDetails().getImages()));
            indicator.setViewPager(picPager);
        }
        if (this.currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
            if (this.mItem.getBuyNow().equalsIgnoreCase("1")) {
                setDUDirectSellData(rootView);
                _loadDetails(pDialog);
                this.btnBuy.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (session.isLoggedIn()) {
                            paymentActivity(subTotal);
                        } else {
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            getActivity().startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_up,R.anim.no_change);
                        }
                    }
                });
            }
        } else if (this.currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
            if (this.mItem.getBuyNow().equalsIgnoreCase("1")) {
                setDUDirectSellData(rootView);
                _loadDetails(pDialog);
                this.btnBuy.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (session.isLoggedIn()) {
                            paymentActivity(subTotal);
                        } else {
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            getActivity().startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_up,R.anim.no_change);
                        }
                    }
                });
            }
        } else if (this.currentAuction.contentEquals(AGConf.FIREBASE_COINS_URL)) {
            if (this.mItem.getBuyNow().equalsIgnoreCase("1")) {
                setDUDirectSellData(rootView);
                _loadDetails(pDialog);
                this.btnBuy.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (session.isLoggedIn()) {
                            paymentActivity(subTotal);
                        } else {
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            getActivity().startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_up,R.anim.no_change);
                        }
                    }
                });
            }
        } else if (this.currentAuction.contentEquals("KEY_CARS_AUCTION")) {
            if (this.mItem.getBuyNow().equalsIgnoreCase("1")) {
                setData(rootView);
                _loadDetails(pDialog);
                System.out.println("direct****");
                this.btnBuy.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (session.isLoggedIn()) {
                            paymentActivity(subTotal);
                        } else {
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            getActivity().startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_up,R.anim.no_change);
                        }
                    }
                });
            } else {
                System.out.println("not direct****");
            }
        } else if (this.currentAuction.contentEquals(AGConf.KEY_MISC_AUCTION)) {
            if (this.mItem.getBuyNow().equalsIgnoreCase("1")) {
                setData(rootView);
                _loadDetails(pDialog);
                this.btnBuy.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (session.isLoggedIn()) {
                            paymentActivity(subTotal);
                        } else {
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            getActivity().startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_up,R.anim.no_change);
                        }
                    }
                });
            }
        } else if (this.currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION) && this.mItem.getBuyNow().equalsIgnoreCase("1")) {
            setData(rootView);
            _loadDetails(pDialog);
            this.btnBuy.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (session.isLoggedIn()) {
                        paymentActivity(subTotal);
                    } else {
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.slide_up,R.anim.no_change);
                    }
                }
            });
        }
        /*if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                setDUDirectSellData(rootView);
                _loadDetails(pDialog);
            }
        } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                setData(rootView);
                _loadDetails(pDialog);


            }
        } else if (currentAuction.contentEquals(AGConf.KEY_MISC_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                setData(rootView);
                _loadDetails(pDialog);

            }
        } else if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                if (rootView == null) {
                    rootView = inflater.inflate(R.layout.fragment_plate_details_new, container, false);
                    pDialog.show();
                } else {
                    setDUDirectSellData(rootView);
                    _loadDetails(pDialog);
                }


            }
        } else if (currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
            if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                setData(rootView);
                _loadDetails(pDialog);

            }
        }*/


        return rootView;
    }


    private void setDUDirectSellData(View view) {

//        buyNowBtn = (Button) view.findViewById(R.id.buyNowBtn);
        pkgBenefitBtn = (TextView) view.findViewById(R.id.pkgBenefitBtn);

        pkgBenefitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PackageActivityNew.class);
                intent.putExtra(AGConf.KEY_AUCTION_TYPE, currentAuction);
                intent.putExtra(AGConf.KEY_ITEM_DATA, Parcels.wrap(mItem));
                getActivity().startActivity(intent);
            }
        });

    }

    private void paymentActivity(int subTotal) {
        PaymentWebActivity.startDirectPayment("http://mazadat.ae/api_v1/payment?buy_now=1&lang=" +
                this.session.getMZLang() + "&category=" + this.mItem.getCategory() + "&amount=" + subTotal +
                "&item_id=" + this.mItem.getId(), getActivity());
        System.out.println("payment url== http://mazadat.ae/api_v1/payment?buy_now=1&lang=" +
                this.session.getMZLang() + "&category=" + this.mItem.getCategory() + "&amount=" +
                subTotal + "&item_id=" + this.mItem.getId());
    }

//    private void _loadDetails(final SweetAlertDialog pDialog)
    private void _loadDetails(final ProgressDialog pDialog)
    {
        api = App.getRestAdapter().create(AGApi.class);

        Map<String, String> params = new HashMap<>();

        // Get deposit for this item
        params.put("lang", session.getMZLang());
        params.put("item_id", String.valueOf(mItem.getId()));
        params.put("cat_id", String.valueOf(mItem.getCategory()));
        getDetailsCall = api.getDetailsDirectSellCars(params);
        System.out.println("param*** " + params.toString());

        getDetailsCall.enqueue(new AGRetrofitCallback<MZDirectItem>() {
            @Override
            public void onResponse(Call<MZDirectItem> call, Response<MZDirectItem> response) {
                if (response.isSuccessful()) {
                    MZDirectItem item = response.body();
                    System.out.println("param*** " + item.toString());
                    // TODO: Check correct response 200
                    pDialog.dismiss();

                    if (item.getTotal() != null) {

                        MZDirectTotal total = item.getTotal();
                        MZValue mCommission = total.getFirst();
                        MZValue mCarAdmin = total.getSecond();
                        MZValue mElectric = total.getThird();
                        MZValue mSubtotal = total.getCartSubtotal();

                        if (mItem.getPrice() != -1) {
                            _drawItem(getString(R.string.price),
                                    String.format("%,d", new Object[]{mItem.getPrice()}), false);
                        }
                        System.out.println("MZValue Price ***" + getString(R.string.price));
                        if (mCommission != null) {
//                            commission = Integer.parseInt(mCommission.getValue());
                            System.out.println("mCommission" + commission);
                            _drawItem(mCommission.getLabel(), String.format("%,d", new Object[]{Integer.valueOf(mCommission.getValue())}), false);
                        } else {

                            System.out.println("mCommission null");
                        }

                        if (mCarAdmin != null) {
//                            carAdmin = Integer.parseInt(mCarAdmin.getValue());
                            System.out.println("carAdmin ***" + carAdmin);
//                            _drawItem(mCarAdmin.getLabel(), String.format(mCarAdmin.getValue()), false);
                            _drawItem(mCarAdmin.getLabel(), String.format("%,d", new Object[]{Integer.valueOf(mCarAdmin.getValue())}), false);
                        } else {
                            System.out.println("carAdmin *** null");
                        }

                        if (mElectric != null) {
//                            electricFee = Integer.parseInt(mElectric.getValue());
                            System.out.println("electricFee *** " + electricFee);
//                            _drawItem(mElectric.getLabel(), String.format(mElectric.getValue()), false);
                            _drawItem(mElectric.getLabel(), String.format("%,d", new Object[]{Integer.valueOf(mElectric.getValue())}), false);
                        } else {

                            System.out.println("electricFee *** null");
                        }
                        if (mSubtotal != null) {
                            ItemDetailsFragment.this.subTotal = Integer.parseInt(mSubtotal.getValue());
                            System.out.println("subTotal*** " + ItemDetailsFragment.this.subTotal);
                            ItemDetailsFragment.this._drawItem(mSubtotal.getLabel(), String.format("%,d", new Object[]{Integer.valueOf(ItemDetailsFragment.this.subTotal)}), true);
                            return;
                        }
                        System.out.println("mSubtotaln *** null");

                    }

                } else {
                    if (getActivity() != null && isAdded()) {
                        Toast.makeText(getActivity(), getString(R.string.err_cant_get_details), Toast.LENGTH_LONG).show();
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<MZDirectItem> call, Throwable t) {
                if (getActivity() != null && isAdded()) {
                    Toast.makeText(getActivity(), getString(R.string.err_cant_get_details), Toast.LENGTH_LONG).show();
                    getActivity().finish();
                }
            }
        });
      /*  btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PaymentWebActivity.startDirectPayment(AGConf.PAYMENT_URL + "?buy_now=" + mItem.getBuyNow() + "&lang=" + session.getMZLang()
                        + "&category=" + mItem.getCategory() + "&amount=" + mItem.getPrice()
                        + "&item_id=" + mItem.getId(), getActivity());
            }
        });*/

    }

    private void _drawItem(String lbl, String val, boolean hideHr) {

        View v = li.inflate(R.layout.lbl_val, null);
        TextView tvLbl = (TextView) v.findViewById(R.id.tvLbl);
        TextView tvVal = (TextView) v.findViewById(R.id.tvVal);

        tvLbl.setText(lbl);
        tvVal.setText(val);

        if (hideHr) {
            View hr = v.findViewById(R.id.hr);
            hr.setVisibility(View.GONE);

            tvVal.setTypeface(null, Typeface.BOLD);
        }

        scrollviewLinearLAyout.addView(v);

    }

    private void _loadDeposit() {
        api = App.getRestAdapter().create(AGApi.class);

        // Get deposit for this item
        Map<String, String> params = new HashMap<>();
        params.put("lang", session.getMZLang());
        params.put("item", String.valueOf(mItem.getId()));
        getDepositCall = api.getDeposit(params);

        getDepositCall.enqueue(new AGRetrofitCallback<Deposit>() {
            @Override
            public void onResponse(Call<Deposit> call, Response<Deposit> response) {
                if (response.isSuccessful()) {
                    deposit = response.body();

                    setHasOptionsMenu(true);

                    int userDeposit = deposit.getUserDeposit();

                    if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
                        // Update plates deposit
                        session.setPlatesDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
                        session.setDuDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {
                        session.setCarsDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_BAGS_AUCTION)) {
                        session.setBagsDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_COINS_AUCTION)) {
                        session.setCoinsDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_HORSES_AUCTION)) {
                        session.setHorsesDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_PROP_AUCTION)) {
                        session.setPropDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_MISC_AUCTION)) {
                        session.setMixDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
                        session.setWatchesDeposit(userDeposit);
                    }

                } else {
                    Toast.makeText(getActivity(), getString(R.string.err_cant_get_deposit), Toast.LENGTH_LONG).show();
                }

                _initTabs();
            }

            @Override
            public void onFailure(Call<Deposit> call, Throwable t) {
                if (isAdded() && getActivity() != null) {
                    Toast.makeText(getActivity(), getString(R.string.err_cant_get_deposit), Toast.LENGTH_LONG).show();
                    _initTabs();
                }
            }
        });
    }


    //todo ali zohair
    private void _loadTabs() {
        api = App.getRestAdapter().create(AGApi.class);






        // Get details  for this item
        Map<String, String> params = new HashMap<>();
        params.put("lang", session.getMZLang());
        params.put("category", String.valueOf(mItem.getCategory()));
        params.put("item_id", String.valueOf(mItem.getId()));




        getDetailsItems = api.getItem(params);

        getDetailsItems.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {
                if (response.isSuccessful()) {

                    MZResponse MZResponse = response.body();
                 //   mItem = response.body();
                    List<MZItem> items = MZResponse.getItems();
                    mItem = items.get(0);
                  /*  setHasOptionsMenu(true);

                    int userDeposit = deposit.getUserDeposit();

                    if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
                        // Update plates deposit
                        session.setPlatesDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
                        session.setDuDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {
                        session.setCarsDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_BAGS_AUCTION)) {
                        session.setBagsDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_COINS_AUCTION)) {
                        session.setCoinsDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_HORSES_AUCTION)) {
                        session.setHorsesDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_PROP_AUCTION)) {
                        session.setPropDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_MISC_AUCTION)) {
                        session.setMixDeposit(userDeposit);
                    } else if (currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
                        session.setWatchesDeposit(userDeposit);
                    }*/

                } else {
                    Toast.makeText(getActivity(), getString(R.string.err_cant_get_deposit), Toast.LENGTH_LONG).show();
                }

                _initTabs();
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {
                if (isAdded() && getActivity() != null) {
                    Toast.makeText(getActivity(), getString(R.string.err_cant_get_deposit), Toast.LENGTH_LONG).show();
                    _initTabs();
                }
            }
        });
    }

    private void _initTabs() {
        /* Tabs */
        // Set up the ViewPager with the sections adapter.
        System.out.println("mTabsAdapter" + mTabsAdapter.getCount());
        mViewPager.setAdapter(mTabsAdapter);
        mViewPager.setOffscreenPageLimit(10);
        tabLayout.setupWithViewPager(mViewPager);
    }


    private void _setDu(MZItemDetails details) {
        if (details != null && details.getNumber() != null) {
            String number = details.getNumber();
            tvPrefix.setText(number.substring(0, 3));
            tvNumber.setText(number.substring(3));
        }
    }


    @SuppressLint("NewApi")
    private void _setPlate(MZItemDetails details) {
        if (details != null && details.getPlateLetter() != null && details.getNumber() != null) {

            String type = mItem.getDetails().getPlateType();

            ivPlate.setImageDrawable(MZUtil.getPlateDrawable(mItem.getCity(), type));

            if (mItem.getCity() == 7) { // Dubai
                if (type.contentEquals("3")) {
                    tvPltR.setTextColor(getActivity().getResources().getColor(R.color.white));
                    tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                } else if (type.contentEquals("2")) {
                    tvPltR.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
                    tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                } else {
                    tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    tvPltR.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
                }
            } else {
                if (type.contentEquals("2")) { // Motocycle
                    tvMotocycle.setVisibility(View.VISIBLE);
                } else {
                    tvMotocycle.setVisibility(View.GONE);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
                tvPltR.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
            }

            tvPltL.setText(mItem.getDetails().getPlateLetter());
            tvPltR.setText(mItem.getDetails().getNumber());
        }
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class TabsAdapter extends FragmentPagerAdapter {

        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            Fragment frag;

            if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
                frag = _getPlateItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {

                frag = _getCarItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_BAGS_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_COINS_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_PROP_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_MISC_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_HORSES_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
                frag = _getOtherItemFrags(position);
            } else if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
                frag = _getDuItemFrags(position);
            } else {
                frag = _getOtherItemFrags(position);
            }

            return frag;
        }

        @Override
        public int getCount() {

            return getTabsCount();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getTabTitle(position);
        }
    }

    private int getTabsCount() {
        if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
            return 3;
        } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {

            if (mItem.getBuyNow().equals("1")) {
                return 3;
            } else {
                return 5;
            }
        } else {
            return 4;
        }
    }

    private void setData(View view) {

        buyNowBtn = (Button) view.findViewById(R.id.buyNowBtn);
        btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DetailActivityNew.class);
                intent.putExtra(AGConf.KEY_AUCTION_TYPE, currentAuction);
                intent.putExtra(AGConf.KEY_ITEM_DATA, Parcels.wrap(mItem));
                getActivity().startActivity(intent);
            }
        });
        btnNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotesActivityNew.class);
                intent.putExtra(AGConf.KEY_AUCTION_TYPE, currentAuction);
                System.out.println("notes *** " + mItem + " ");
                intent.putExtra(AGConf.KEY_ITEM_DATA, Parcels.wrap(mItem));
                getActivity().startActivity(intent);
            }
        });
        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LocationActivityNew.class);
                intent.putExtra(AGConf.KEY_AUCTION_TYPE, currentAuction);
                intent.putExtra(AGConf.KEY_ITEM_DATA, Parcels.wrap(mItem));
                getActivity().startActivity(intent);
            }
        });

    }

    private String getTabTitle(int position) {
        String title;

        if (currentAuction.contentEquals(AGConf.KEY_PLATES_AUCTION)) {
            title = _getPlateTabTitles(position);
        } else if (currentAuction.contentEquals(AGConf.KEY_CARS_AUCTION)) {

           /* if (mItem.getBuyNow().equalsIgnoreCase("1")) {
                title = _getCarsTabTitles_directSell(position);
            } else {
            }*/
            title = _getCarsTabTitles(position);
        } else if (currentAuction.contentEquals(AGConf.KEY_JEWELLERY_AUCTION)) {
            title = _getWatchesTabTitles(position);
        } else if (currentAuction.contentEquals(AGConf.KEY_DU_AUCTION)) {
            title = _getDuTabTitles(position);
        } else {
            title = _getOthersTabTitles(position);
        }

        return title;

        // TODO: Handle difference cases
    }

    private String _getPlateTabTitles(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_bid);
            case 1:
                return getString(R.string.tab_notes);
            case 2:
                return getString(R.string.tab_deposit);
        }
        return null;
    }

    private String _getDuTabTitles(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_bid);
            case 1:
                return getString(R.string.tab_package);
            case 2:
                return getString(R.string.tab_notes);
            case 3:
                return getString(R.string.tab_deposit);
        }
        return null;
    }

    private String _getCarsTabTitles(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_bid);
            case 1:
                return getString(R.string.tab_details);
            case 2:
                return getString(R.string.tab_notes);
            case 3:
                return getString(R.string.tab_location);
            case 4:
                return getString(R.string.tab_deposit);
        }
        return null;
    }

    private String _getCarsTabTitles_directSell(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_details);
            case 1:
                return getString(R.string.tab_notes);
            case 2:
                return getString(R.string.tab_location);

        }
        return null;
    }

    private String _getWatchesTabTitles(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_bid);
            case 1:
                return getString(R.string.tab_details);
            case 2:
                return getString(R.string.tab_notes);
            case 3:
                return getString(R.string.tab_deposit);
        }
        return null;
    }

    private String _getOthersTabTitles(int position) {
        switch (position) {
            case 0:
                return getString(R.string.tab_bid);
            case 1:
                return getString(R.string.tab_details);
            case 2:
                return getString(R.string.tab_notes);
            case 3:
                return getString(R.string.tab_deposit);
        }
        return null;
    }


    private Fragment _getPlateItemFrags(int position) {
        switch (position) {
            case 0:
                return BidFragment.newInstance(mItem, currentAuction, deposit);
            case 1:
                return NotesFragment.newInstance(mItem, currentAuction);
            case 2:
                return DepositFragment.newInstance(mItem, deposit);
        }
        return null;
    }

    private Fragment _getDuItemFrags(int position) {
        switch (position) {
            case 0:
                return BidFragment.newInstance(mItem, currentAuction, deposit);
            case 1:
                return PackageFragment.newInstance(mItem, currentAuction);
            case 2:
                return DetailsFragment.newInstance(mItem, currentAuction);
            case 3:
                return DepositFragment.newInstance(mItem, deposit);
        }
        return null;
    }

    private Fragment _getCarItemFrags(int position) {
        switch (position) {
            case 0:
                return BidFragment.newInstance(mItem, currentAuction, deposit);
            case 1:
                //todo ali zohair
                return DetailsFragment.newInstance(mItem, currentAuction);
            case 2:
                return NotesFragment.newInstance(mItem, currentAuction);
            case 3:
                return LocationFragment.newInstance(mItem, currentAuction);
            case 4:
                return DepositFragment.newInstance(mItem, deposit);
        }
        return null;
    }

    private Fragment _getCarItemFrags_directSell(int position) {

        switch (position) {
            case 0:
                System.out.println("Position " + position);
                return DetailsFragment_New.newInstance(mItem, currentAuction);
            case 1:
                System.out.println("Position " + position);
                return DetailsFragment_New.newInstance(mItem, currentAuction);
            case 2:
                System.out.println("Position " + position);
                return DetailsFragment_New.newInstance(mItem, currentAuction);
        }
        return DetailsFragment_New.newInstance(mItem, currentAuction);
    }

    private Fragment _getOtherItemFrags(int position) {
        switch (position) {
            case 0:
                return BidFragment.newInstance(mItem, currentAuction, deposit);
            case 1:
                return DetailsFragment.newInstance(mItem, currentAuction);
            case 2:
                return NotesFragment.newInstance(mItem, currentAuction);
            case 3:
                return DepositFragment.newInstance(mItem, deposit);
        }
        return null;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.item_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.act_payment:
                BaseActivity.openPayment(getActivity(), currentAuction);
                return true;
        }
        return false;
    }

}
