package ae.mazadat.onlineauctionapp.adapter.draw;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.draw.MZDrawNumberItem;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.viewholder.DrawItemViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MZItem} and makes a call to the
 * specified {@link ItemsListFragment.OnMZItemInteractionListener}.
 */
public abstract class DrawAdapter extends RecyclerView.Adapter<DrawItemViewHolder>
//        implements View.OnLongClickListener
{

    protected OnLongClickListener onLongClickListener;

    public interface OnLongClickListener {
        public void onRemove(View view, int position);
    }

    protected String auctionType = "";

    protected final int TYPE_PLATE = 1;
    protected final int TYPE_NUMBER = 2;

    protected Context mContext;
    protected final List<MZDrawNumberItem> mValues;

    public DrawAdapter(Context context) {
        mValues   = new ArrayList<>();
        mContext  = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (mValues.get(position).getType().contentEquals("plate")) {
            return TYPE_PLATE;
        } else {
            return TYPE_NUMBER;
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public List<MZDrawNumberItem> getItems() {
        return mValues;
    }

    public MZDrawNumberItem getItem(int position) {
        return mValues.get(position);
    }

    public void add(MZDrawNumberItem model) {
        mValues.add(model);
        notifyItemInserted(mValues.size() - 1);
    }

    public void remove(int position) {
        mValues.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public void update(int position, MZDrawNumberItem item) {
        mValues.remove(position);
        mValues.add(position, item);
        notifyItemChanged(position);
    }

    public void clear() {
        mValues.clear();
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(String.valueOf(mValues.get(position).getId()));
    }

    public void setOnListItemClickListener(OnLongClickListener listener) {
        this.onLongClickListener = listener;
    }

//    @Override
//    public boolean onLongClick(View v) {
//        final int viewId = v.getId();
//        if(viewId == R.id.card) {
//            if (onLongClickListener != null) {
//                onLongClickListener.onRemove(v, (int) v.getTag(R.string.tag_position));
//            }
//        }
//        return true;
//    }
}
