package ae.mazadat.onlineauctionapp.ui;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import ae.mazadat.onlineauctionapp.model.MZItem;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnMZItemInteractionListener}
 * interface.
 */
public abstract class GridFragment extends ItemsListFragment {

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {

//            outRect.set(0, 0, 0, 0);
//
////            if (session.getLang().contentEquals("ar")) {
////                outRect.right = space;
////            } else {
//                outRect.left = space;
////            }
//
//            outRect.top = space;
//            outRect.bottom = space;
//
//            // Add top margin only for the first item to avoid double space between items
//            int pos = parent.getChildLayoutPosition(view);
//            if (pos % 2 == 1) {
////                if (session.getLang().contentEquals("ar")) {
////                    outRect.left = space;
////                } else {
//                    outRect.right = space;
////                }
//            } else {
////                if (session.getLang().contentEquals("ar")) {
////                    if (adapter.getItemCount() > 1) {
////                        outRect.left = 0;
////                    } else {
////                        outRect.left = space;
////                    }
////                } else {
//                    if (adapter.getItemCount() > 1) {
//                        outRect.right = 0;
//                    } else {
//                        outRect.right = space;
//                    }
////                }
//            }
//
//            if (pos > 1) {
//                outRect.top = 0;
//            }
        }
    }

    public void doWhenSuccessLoad(boolean reset, List<MZItem> items) {
        super.doWhenSuccessLoad(reset);

        if (getActivity() != null && isAdded()) {
            if (reset) {
                adapter.clear();
            } else {
                // Remove latest element (as it's the (load more) loader)
//                if (adapter.getItemCount() > 0) {
//                    adapter.remove(adapter.getItemCount() - 1);
//                }
            }

            // No more content to load
            if (items != null && (items.isEmpty() || items.size() < ITEMS_PER_PAGE)) {
                moreContentToLoad = false;
                System.out.println("item *** "+items.size()+" item per page "+ITEMS_PER_PAGE);
            }

        }
    }

    public void doWhenFailureApiCall(String title, String errorMessage) {
        super.doWhenFailureApiCall(title, errorMessage);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.startUpdateTimer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        adapter.cancelUpdateTimer();
    }
}
