package ae.mazadat.onlineauctionapp.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.HashMap;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.MZItemDetails;
import ae.mazadat.onlineauctionapp.model.rest.MZDirectItem;
import ae.mazadat.onlineauctionapp.model.rest.MZDirectTotal;
import ae.mazadat.onlineauctionapp.model.rest.MZValue;
import ae.mazadat.onlineauctionapp.ui.payment.PaymentWebActivity;
import ae.mazadat.onlineauctionapp.util.MZUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class DirectSellDetailsFragment extends Fragment {

    AGApi api;
    Call<MZDirectItem> getDetailsCall;

    // Plates
    @Nullable
    @Bind(R.id.tvPltL)
    TextView tvPltL;

    @Nullable
    @Bind(R.id.tvPltR)
    TextView tvPltR;

    @Nullable
    @Bind(R.id.ivPlate)
    ImageView ivPlate;

    @Nullable
    @Bind(R.id.tvMotocycle)
    TextView tvMotocycle;

    @Bind(R.id.btnBuy)
    Button btnBuy;

    @Bind(R.id.lbls)
    ViewGroup container;

    LayoutInflater li;

    MZItem mItem;

    int price        = 0;
    int adminFees    = 0; // Administration Fees"
    int transferFees = 0; // Plate Ownership Transfer fees
    int totalPrice   = 0;


    SessionManager session = App.getSessionManager();

    private String currentAuction = AGConf.KEY_DIRECT_SELL;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DirectSellDetailsFragment() {
    }


    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DirectSellDetailsFragment newInstance(int columnCount) {
        DirectSellDetailsFragment fragment = new DirectSellDetailsFragment();
        Bundle args = new Bundle();
//        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getDetailsCall != null) {
            getDetailsCall.cancel();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            Parcelable data = getArguments().getParcelable(AGConf.KEY_ITEM_DATA);
            if (data != null) {
                mItem = Parcels.unwrap(data);
            }

            String auction = getArguments().getString(AGConf.KEY_AUCTION_TYPE);
            if (auction != null) {
                currentAuction = auction;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_direct_plate_details, container, false);

        ButterKnife.bind(this, rootView);

        li = LayoutInflater.from(getActivity());

        setHasOptionsMenu(false);

        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(R.color.colorPrimaryDark);
        pDialog.setTitleText(getString(R.string.loading_deposit));
        pDialog.setCancelable(false);
        pDialog.show();

        _loadDetails(pDialog);

        price = mItem.getSellPrice();
        _setPlate(mItem.getDetails());

        _drawItem(getString(R.string.plate_price), String.format(getString(R.string.price_aed), price), false);

        if ((mItem.getSoldBy() != null && !mItem.getSoldBy().contentEquals("0"))
                || mItem.getBooked() == 1) {
            btnBuy.setVisibility(View.GONE);
        }

        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentWebActivity.startDirectPayment(AGConf.PAYMENT_URL + "?lang=" + session.getMZLang()
                        + "&category="+ mItem.getCategory() +"&amount=" + totalPrice
                        + "&item_id=" + mItem.getId(), getActivity());
            }
        });

        return rootView;
    }

    private void _loadDetails(final SweetAlertDialog pDialog) {
        api = App.getRestAdapter().create(AGApi.class);

        Map<String, String> params = new HashMap<>();

        // Get deposit for this item
        params.put("lang",    session.getMZLang());
        params.put("item_id", String.valueOf(mItem.getId()));
        getDetailsCall = api.getDirectSellDetails(params);

        getDetailsCall.enqueue(new AGRetrofitCallback<MZDirectItem>() {
            @Override
            public void onResponse(Call<MZDirectItem> call, Response<MZDirectItem> response) {
                if (response.isSuccessful()) {
                    MZDirectItem item = response.body();

                    // TODO: Check correct response 200
                    pDialog.dismiss();

                    if (item.getTotal() != null) {
                        MZDirectTotal total = item.getTotal();
                        MZValue mTotal  = total.getTotal();
                        MZValue mAdFee  = total.getAdminFees();
                        MZValue mTrans  = total.getTransferFees();

                        if (mAdFee != null) {
                            adminFees = Integer.parseInt(mAdFee.getValue());
                            _drawItem(mAdFee.getLabel(), String.format(getString(R.string.price_aed), adminFees), false);
                        }

                        if (mTrans != null) {
                            transferFees = Integer.parseInt(mTrans.getValue());
                            _drawItem(mTrans.getLabel(), String.format(getString(R.string.price_aed), transferFees), false);
                        }

                        if (mTotal != null) {
                            totalPrice = Integer.parseInt(mTotal.getValue());
                            _drawItem(mTotal.getLabel(), String.format(getString(R.string.price_aed), totalPrice), true);
                        }

                    }

                } else {
                    if (getActivity() != null && isAdded()) {
                        Toast.makeText(getActivity(), getString(R.string.err_cant_get_details), Toast.LENGTH_LONG).show();
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<MZDirectItem> call, Throwable t) {
                if (getActivity() != null && isAdded()) {
                    Toast.makeText(getActivity(), getString(R.string.err_cant_get_details), Toast.LENGTH_LONG).show();
                    getActivity().finish();
                }
            }
        });
    }



    private void _setPlate(MZItemDetails details) {
        if (details != null && details.getPlateLetter() != null && details.getNumber() != null) {

            String type = mItem.getDetails().getPlateType();

            ivPlate.setImageDrawable(MZUtil.getPlateDrawable(mItem.getCity(), type));

            if (mItem.getCity() == 7) { // Dubai
                if (type.contentEquals("3")) {
                    tvPltR.setTextColor(getActivity().getResources().getColor(R.color.white));
                    tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                } else if (type.contentEquals("2")) {
                    tvPltR.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
                    tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                } else {
                    tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    tvPltR.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
                }
            } else {
                if (type.contentEquals("2")) { // Motocycle
                    tvMotocycle.setVisibility(View.VISIBLE);
                } else {
                    tvMotocycle.setVisibility(View.GONE);
                }

                tvPltR.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                tvPltR.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryText));
            }

            tvPltL.setText(mItem.getDetails().getPlateLetter());
            tvPltR.setText(mItem.getDetails().getNumber());
        }
    }

    private void _drawItem(String lbl, String val, boolean hideHr) {
        View v = li.inflate(R.layout.lbl_val, null);
        TextView tvLbl = (TextView) v.findViewById(R.id.tvLbl);
        TextView tvVal = (TextView) v.findViewById(R.id.tvVal);

        tvLbl.setText(lbl);
        tvVal.setText(val);

        if (hideHr) {
            View hr = v.findViewById(R.id.hr);
            hr.setVisibility(View.GONE);

            tvVal.setTypeface(null, Typeface.BOLD);
        }

        container.addView(v);
    }
}
