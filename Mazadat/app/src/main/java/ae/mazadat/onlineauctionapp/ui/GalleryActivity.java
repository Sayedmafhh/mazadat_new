package ae.mazadat.onlineauctionapp.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.gallery.FullscreenAdapter;
import ae.mazadat.onlineauctionapp.model.rest.MZImg;
import ae.mazadat.onlineauctionapp.util.view.TouchImageView;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by agile on 5/18/16.
 */
public class GalleryActivity extends BaseActivity
        implements FullscreenAdapter.FullScreenImageLoader {

    List<MZImg> mImages;
    private int mPosition;

    @Bind(R.id.vp)
    ViewPager mViewPager;


    private final ViewPager.OnPageChangeListener mViewPagerOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (mViewPager != null) {
                mViewPager.setCurrentItem(position);

                setActionBarTitle(position);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasDrawer(false);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

        setContentView(R.layout.activity_gallery);

        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);

            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
            actionBar.setHomeAsUpIndicator(upArrow);
        }

        Intent intent = getIntent();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                mImages = Parcels.unwrap(extras.getParcelable("images"));
                mPosition = extras.getInt("position");
            }
        }

        setUpViewPager();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeListeners();
    }
    // endregion

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return false;
    }

    // region FullscreenAdapter.FullScreenImageLoader Methods

    @Override
    public void loadFullScreenImage(TouchImageView iv, String imageUrl, int width) {
        imageUrl = imageUrl.replace("https", "http");

        Glide.with(this).load(imageUrl).placeholder(getResources().getDrawable(R.drawable.ic_mix))
                .crossFade().into(iv);
        iv.requestLayout();
//        sFullScreenImageLoader.loadFullScreenImage(iv, imageUrl, width, bglinearLayout);
    }

    // endregion

    private void setUpViewPager() {
        List<MZImg> images = new ArrayList<>();
        images.addAll(mImages);

        FullscreenAdapter FullscreenAdapter = new FullscreenAdapter(images);
        FullscreenAdapter.setFullScreenImageLoader(this);
        mViewPager.setAdapter(FullscreenAdapter);
        mViewPager.addOnPageChangeListener(mViewPagerOnPageChangeListener);
        mViewPager.setCurrentItem(mPosition);

        setActionBarTitle(mPosition);
    }

    private void setActionBarTitle(int position) {
        if (mViewPager != null && mImages.size() > 1) {
            int totalPages = mViewPager.getAdapter().getCount();

            ActionBar actionBar = getSupportActionBar();
            if(actionBar != null){
                actionBar.setTitle(String.format("%d/%d", (position + 1), totalPages));
            }
        }
    }

    private void removeListeners() {
        mViewPager.removeOnPageChangeListener(mViewPagerOnPageChangeListener);
    }

//    public static void setFullScreenImageLoader(FullscreenAdapter.FullScreenImageLoader loader) {
//        sFullScreenImageLoader = loader;
//    }
}
