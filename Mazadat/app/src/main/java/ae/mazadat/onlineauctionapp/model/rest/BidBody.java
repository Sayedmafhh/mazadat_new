package ae.mazadat.onlineauctionapp.model.rest;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by agile on 4/3/16.
 */
@Parcel
public class BidBody {

    @SerializedName("bid_id")
    int id;

    @SerializedName("bid_amount")
    int amount;

    @SerializedName("auto_bid")
    int autoBid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAutoBid() {
        return autoBid;
    }

    public void setAutoBid(int autoBid) {
        this.autoBid = autoBid;
    }
}
