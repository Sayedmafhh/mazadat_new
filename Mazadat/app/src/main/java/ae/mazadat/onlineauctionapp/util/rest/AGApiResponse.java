package ae.mazadat.onlineauctionapp.util.rest;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by agile on 3/23/16.
 */
@Parcel
public class AGApiResponse {

    String status;
    String msg;

    @Nullable
    @SerializedName("need_update")
    int needUpdate;

    @Nullable
    String img;

    public String getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    @Nullable
    public String getImg() {
        return img;
    }

    @Nullable
    public int isNeedUpdate() {
        return needUpdate;
    }
}
