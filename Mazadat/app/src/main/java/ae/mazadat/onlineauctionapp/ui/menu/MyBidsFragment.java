package ae.mazadat.onlineauctionapp.ui.menu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.menu.MyBidsAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class MyBidsFragment extends ItemsListFragment {

    @Bind(R.id.btnWin)
    Button btnWin;

//    @Bind(R.id.btnLost)
//    Button btnLost;

    @Bind(R.id.btnHigher)
    Button btnHigher;

    @Bind(R.id.vEmpty)
    View vEmpty;

    String type = "0";

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static MyBidsFragment newInstance(int columnCount) {
        MyBidsFragment fragment = new MyBidsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_my_bids, container, false);

        setHasFab(true);
        setHasLoadMore(false);

        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(false);

        init(getActivity());

        btnWin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "0";
                adapter.clear();
                vEmpty.setVisibility(View.GONE);
                loadItems();
            }
        });

//        btnLost.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                type = "1";
//                adapter.clear();
//                vEmpty.setVisibility(View.GONE);
//                loadItems();
//            }
//        });

        btnHigher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "2";
                adapter.clear();
                vEmpty.setVisibility(View.GONE);
                loadItems();
            }
        });

        adapter = new MyBidsAdapter(getActivity(), mListener);
        recyclerView.setAdapter(adapter);

        if (savedInstanceState == null) {
            loadItems();
        }

        return rootView;
    }


    @Override
    public void serviceLoadItems(final boolean reset) {
        super.serviceLoadItems(reset);

        filterParams.put("lang", session.getMZLang());
        filterParams.put("type", type);

        gettingItemsCall = api.getMyBids(filterParams);
        gettingItemsCall.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {

                if (response.isSuccessful()) {
                    if (isAdded() && getActivity() != null) {
                        MZResponse MZResponse = response.body();

                        List<MZItem> items = MZResponse.getMyBids();

                        // TODO: When failed - this may throw an error!

                        // TODO: Remove solr
                        doWhenSuccessLoad(reset, items);

                        if (items != null && items.size() > 0) {
                            vEmpty.setVisibility(View.GONE); // Important
                            for (int i=0; i<items.size(); i++) {
                                MZItem item = items.get(i);
                                adapter.add(item);
                            }
                        } else {
                            vEmpty.setVisibility(View.VISIBLE);
                        }
                    }

                } else {
                    if (isAdded() && getActivity() != null) {
                        doWhenFailureApiCall("Opps!", "Unable to load items!");
                    }
                }
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {

            }
        });
    }

    public void doWhenSuccessLoad(boolean reset, List<MZItem> items) {
        super.doWhenSuccessLoad(reset);

        if (getActivity() != null && isAdded()) {
            if (reset) {
                adapter.clear();
            } else {
                // Remove latest element (as it's the (load more) loader)
//                if (adapter.getItemCount() > 0) {
//                    adapter.remove(adapter.getItemCount() - 1);
//                }
            }

            // No more content to load
            if (items != null && (items.isEmpty() || items.size() < 25)) { // TODO: Check number!
                moreContentToLoad = false;
            }
        }
    }

    @Override
    protected void registerPN() {
    }

}
