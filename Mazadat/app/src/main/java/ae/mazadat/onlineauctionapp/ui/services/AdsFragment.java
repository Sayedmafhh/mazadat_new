package ae.mazadat.onlineauctionapp.ui.services;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.AdsAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link ItemsListFragment.OnMZItemInteractionListener}
 * interface.
 */
public class AdsFragment extends ItemsListFragment {

    AdsAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AdsFragment() {
    }


    @SuppressWarnings("unused")
    public static AdsFragment newInstance(int columnCount) {
        AdsFragment fragment = new AdsFragment();
        Bundle args = new Bundle();
//        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_base_list, container, false);

        setHasFab(false);
        setHasLoadMore(false);
        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(false);

        init(getActivity());

        adapter = new AdsAdapter(getActivity(), mListener);
        recyclerView.setAdapter(adapter);

        if (savedInstanceState == null) {
            loadItems();
        }

        return rootView;
    }

    @Override
    public void serviceLoadItems(final boolean reset) {
        super.serviceLoadItems(reset);

        filterParams.put("lang", session.getMZLang());
        filterParams.put("limit", "25");
        filterParams.put("offset", String.valueOf(currentPage * 25));

//        doWhenSuccessLoad(false);

        gettingItemsCall = api.getAds(filterParams);
        gettingItemsCall.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {

                if (response.isSuccessful()) {
                    MZResponse MZResponse = response.body();

                    List<MZItem> items = MZResponse.getItems();

                    mFilters = MZResponse.getFilters();
                    mSorts   = MZResponse.getSorts();

                    if (!filtersInitiated) { // One time only
                        _initFilters();
                        _initSorts();
                        filtersInitiated = true;
                    }

                    doWhenSuccessLoad(reset, items);


                    if (items != null && items.size() > 0) {
                        for (int i=0; i<items.size(); i++) {
                            MZItem item = items.get(i);
//                            item.setUserDeposit(deposit);
                            adapter.add(item);
                        }
                    } else {
                        AGUtil.errorAlert(getActivity(), "No items!", getString(R.string.no_items));
                    }

                } else {
                    doWhenFailureApiCall("Opps!", "Unable to load items!");
                    AGUtil.errorAlert(getActivity(), "No items!", "Unable to load ads!");

                }
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {

            }
        });

    }

    public void doWhenSuccessLoad(boolean reset, List<MZItem> items) {
        super.doWhenSuccessLoad(reset);

        if (getActivity() != null && isAdded()) {
            if (reset) {
                adapter.clear();
            }

            // No more content to load
            if (items != null && (items.isEmpty() || items.size() < 25)) {
                moreContentToLoad = false;
            }
        }
    }


    public void doWhenFailureApiCall(String title, String errorMessage) {
        super.doWhenFailureApiCall(title, errorMessage);

    }

    @Override
    protected void registerPN() {
        // No need here
    }

}
