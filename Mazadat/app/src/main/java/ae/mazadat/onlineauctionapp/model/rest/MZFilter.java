package ae.mazadat.onlineauctionapp.model.rest;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by agile on 4/17/16.
 */
@Parcel
public class MZFilter {
    String name;
    String label;

    List<MZOption> options;

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public List<MZOption> getOptions() {
        return options;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setOptions(List<MZOption> options) {
        this.options = options;
    }
}
