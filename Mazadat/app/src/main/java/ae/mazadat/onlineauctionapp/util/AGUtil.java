package ae.mazadat.onlineauctionapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ae.mazadat.onlineauctionapp.App;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by agile on 3/29/16.
 */
public class AGUtil {

    public static void closeKeyboard(Activity activity) {
        toggleKeyboard(false, activity);
    }

    private static void toggleKeyboard(boolean doOpen, Activity activity) {
        InputMethodManager inputManager =
                (InputMethodManager) activity.getApplicationContext().
                        getSystemService(Context.INPUT_METHOD_SERVICE);

        if (doOpen) {
            // Open keyboard
            inputManager.toggleSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), InputMethodManager.SHOW_IMPLICIT, 0);
        } else {
            if (inputManager.isAcceptingText()) { // Check keyboard already open
                // Close keyboard
                if (activity.getCurrentFocus() != null) {
                    inputManager.hideSoftInputFromWindow(
                            activity.getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }
    }

    public static boolean isPermissionGranted(Activity activity, String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            // android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            if (activity.checkSelfPermission(permission)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            return false;
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    public static boolean isPermissionsGranted(Activity activity, String[] permissions, int code) {
        if (Build.VERSION.SDK_INT >= 23) {

            // android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            boolean result = true;

            int countPerms = 0;
            ArrayList<String> newPerms = new ArrayList<>();
            for (String p : permissions) {
                // TODO: Ask for only MISSING permission not for all
                if (activity.checkSelfPermission(p) != PackageManager.PERMISSION_GRANTED) {
                    result = false;
                    newPerms.add(p);
                    countPerms++;
                }
            }

            if (!result) {
                String[] newPermissions = new String[countPerms];
                for (int i = 0; i < countPerms; i++) {
                    newPermissions[i] = newPerms.get(i);
                }
                ActivityCompat.requestPermissions(activity, newPermissions, code);
                return false;
            }

            return true;
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }


    /**
     * Show Error Alert - Sweet alert
     *
     * @param context
     * @param title
     * @param message
     */
    public static void errorAlert(Context context, String title, String message) {
        new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .show();
    }


    public static void successAlert(Context context, String title, String message) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .show();
    }

    public static String formatDate(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format, java.util.Locale.getDefault());
        return df.format(date);

    }

    public static String formatMazad(Date date) {
        String format = "yyyy-MM-dd HH:mm";
        return formatDate(date, format);

    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static Typeface getTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/DroidKufi_Regular.ttf");
        // TODO: What about bold? Test it please
    }

    public static String loadJsonFile(String file) {
        String json = null;
        try {

            InputStream is = App.getAppContext().getAssets().open("data/" + file);

            int size = is.available();

            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }


    public static int getScreenWidth(@NonNull Context context) {
        Point size = new Point();
        ((Activity) context).getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }

    /**
     * Push notifications
     */

    public static boolean checkPlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
//                GooglePlayServicesUtil.getErrorDialog(resultCode, context,
//                        AGConf.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }


    public static void playSound(Context context, final int soundID) {
        AudioManager audioManager = App.getAudioManager();
        SoundPool soundPool = App.getSoundPool();
        float curVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        final float leftVolume = curVolume/maxVolume;
        final float rightVolume = curVolume/maxVolume;
        if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
            soundPool.load(context, soundID, 2);
            soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                @Override
                public void onLoadComplete(SoundPool soundPool, int sound,
                                           int status) {
                    soundPool.play(sound, leftVolume, rightVolume, 1, 0, 1f);
                }
            });
        }
    }

    public static void bid(Context context) {
        if (context != null) {

            // commented by Hussam 2016-08-28 to stop sounds and vibrations

            // playSound(context, R.raw.bid);
            // Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            // long[] pattern = {0, 50, 50, 50};
            // v.vibrate(pattern, -1);
        }
    }
}


