package ae.mazadat.onlineauctionapp.model.rest;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by agile on 6/2/16.
 */
@Parcel
public class MZUser {

    @SerializedName("id")
    int id;

    @SerializedName("token")
    String token;

    @SerializedName("username")
    String name;

    @SerializedName("ph_no")
    String phone;

    @SerializedName("user_language")
    String lang;

    @SerializedName("email")
    String email;

    @SerializedName("last_login")
    String lastLogin;

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public int getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public String getLang() {
        return lang;
    }
}
