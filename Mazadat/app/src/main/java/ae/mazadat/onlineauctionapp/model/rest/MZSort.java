package ae.mazadat.onlineauctionapp.model.rest;

import org.parceler.Parcel;

/**
 * Created by agile on 4/17/16.
 */
@Parcel
public class MZSort {
    String label;
    String name;

    public String getLabel() {
        return label;
    }

    public String getName() {
        return name;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setName(String name) {
        this.name = name;
    }
}
