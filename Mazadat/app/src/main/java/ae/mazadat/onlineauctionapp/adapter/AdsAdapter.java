package ae.mazadat.onlineauctionapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.ui.ItemsListFragment;
import ae.mazadat.onlineauctionapp.viewholder.AdViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MZItem} and makes a call to the
 * specified {@link ItemsListFragment.OnMZItemInteractionListener}.
 */
public class AdsAdapter extends RecyclerView.Adapter<AdViewHolder> {

    private final List<MZItem> mValues;
    private final ItemsListFragment.OnMZItemInteractionListener mListener;
    private Context mContext;

    public AdsAdapter(List<MZItem> items, ItemsListFragment.OnMZItemInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    public AdsAdapter(Context context, ItemsListFragment.OnMZItemInteractionListener listener) {
        mValues = new ArrayList<>();
        mListener = listener;
        mContext = context;
    }
    @Override
    public AdViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ad, parent, false);
        return new AdViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        // make sure Glide doesn't load anything into this view until told otherwise
        Glide.clear(holder.mIv);
        // remove the placeholder (optional); read comments below
        holder.mIv.setImageDrawable(null);
        Glide.with(mContext).load(holder.mItem.getImage()).into(holder.mIv);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onMZItemInteraction(holder.mItem, AGConf.KEY_ADS);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void add(MZItem model) {
        mValues.add(model);
        notifyItemInserted(mValues.size() - 1);
    }

    public void remove(int position) {
        mValues.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public void clear() {
        mValues.clear();
        notifyDataSetChanged();
    }

}
