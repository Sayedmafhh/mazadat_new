package ae.mazadat.onlineauctionapp.ui.details.tab;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import org.parceler.Parcels;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.Deposit;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by agile on 4/22/16.
 */
public class NotesFragment extends Fragment {

    public NotesFragment() {}

    @Bind(R.id.webview)
    WebView webview;

    MZItem mItem;
    String mType;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static NotesFragment newInstance(MZItem item, String type) {
        NotesFragment fragment = new NotesFragment();
        Bundle args = new Bundle();
        args.putParcelable(AGConf.KEY_ITEM_DATA, Parcels.wrap(item));
        args.putString(AGConf.KEY_AUCTION_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }
    public static NotesFragment newInstance(MZItem item, String currentAuction, Deposit deposit) {
        NotesFragment fragment = new NotesFragment();
        Bundle args = new Bundle();
        args.putParcelable(AGConf.KEY_ITEM_DATA, Parcels.wrap(item));
        args.putParcelable(AGConf.KEY_ITEM_DEPOSIT, Parcels.wrap(deposit));
        args.putString(AGConf.KEY_AUCTION_TYPE, currentAuction);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getParcelable(AGConf.KEY_ITEM_DATA) != null) {
                Parcelable data = getArguments().getParcelable(AGConf.KEY_ITEM_DATA);
                mItem = Parcels.unwrap(data);
            }

            if (getArguments().getString(AGConf.KEY_AUCTION_TYPE) != null) {
                mType = getArguments().getString(AGConf.KEY_AUCTION_TYPE);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notes, container, false);

        ButterKnife.bind(this, rootView);

        SessionManager session = App.getSessionManager();

        webview.setBackgroundColor(Color.TRANSPARENT);

        if (mType != null) {

            String dir = session.getMZLang().contentEquals("ae") ? "rtl" : "ltr";
            String html = "<body dir=\"" + dir + "\">";

            // Cars
            if ((mType.contentEquals(AGConf.KEY_CARS_AUCTION) || mType.contentEquals(AGConf.KEY_PLATES_AUCTION))
                    && mItem.getDetails().getNotes() != null) {
                html += mItem.getDetails().getNotes();
            }

            // Watches
            if (mType.contentEquals(AGConf.KEY_JEWELLERY_AUCTION) && mItem.getDetails().getDescription() != null) {
                html += mItem.getDetails().getDescription();
            }

            // Properties
            if (mType.contentEquals(AGConf.KEY_PROP_AUCTION) && mItem.getDetails().getDesc() != null) {
                html += mItem.getDetails().getDesc();
            }

            html += "</body>";
            webview.loadData(html, "text/html; charset=UTF-8", null);
        }

        return rootView;
    }
}
