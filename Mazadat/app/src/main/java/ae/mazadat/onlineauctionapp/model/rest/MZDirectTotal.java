package ae.mazadat.onlineauctionapp.model.rest;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by agile on 5/30/16.
 */
@Parcel
public class  MZDirectTotal {

    @SerializedName("sub_total")
    MZValue total;

    @SerializedName("0")
    MZValue first;
    @SerializedName("1")
    MZValue second;
    @SerializedName("2")
    MZValue third;
    @SerializedName("cart_subtotal")
    MZValue cartSubtotal;

    @SerializedName("admin_fees")
    MZValue adminFees;

    @SerializedName("reg_fees")
    MZValue transferFees;

    public MZValue getFirst() {
        return first;
    }

    public MZValue getSecond() {
        return second;
    }

    public MZValue getThird() {
        return third;
    }

    public MZValue getCartSubtotal() {
        return cartSubtotal;
    }

    public MZValue getTotal() {
        return total;
    }

    public MZValue getAdminFees() {
        return adminFees;
    }

    public MZValue getTransferFees() {
        return transferFees;
    }

    @Override
    public String toString() {
        return "MZDirectTotal{" +
                "total=" + total +
                ", first=" + first +
                ", second=" + second +
                ", third=" + third +
                ", cartSubtotal=" + cartSubtotal +
                ", adminFees=" + adminFees +
                ", transferFees=" + transferFees +
                '}';
    }
}
