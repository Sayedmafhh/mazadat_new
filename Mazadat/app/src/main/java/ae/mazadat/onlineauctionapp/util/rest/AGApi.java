package ae.mazadat.onlineauctionapp.util.rest;

import java.util.List;
import java.util.Map;

import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.DashboardItem;
import ae.mazadat.onlineauctionapp.model.rest.Deposit;
import ae.mazadat.onlineauctionapp.model.rest.LoginResponse;
import ae.mazadat.onlineauctionapp.model.rest.MZDirectItem;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.model.rest.ResetResponse;
import ae.mazadat.onlineauctionapp.model.rest.SignupResponse;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by agile on 1/21/15.
 */
public interface AGApi {

    //@FormUrlEncoded
    @GET("login")
    Call<LoginResponse> login(@Query("username") String username, @Query("password") String password,
                              @Query("device_id") String deviceId, @Query("platform") String platform,@Query("lang") String lang);
    @GET("buy_now")
    Call<MZDirectItem> getDetailsDirectSellCars(@QueryMap Map<String, String> params);

    // Reset password
   // @FormUrlEncoded
    @GET("reset_password_request")
    Call<ResetResponse> resetPwd(@Query("email") String email,@Query("lang") String lang);

    // Sign up
    @FormUrlEncoded
    @POST("register")
    Call<SignupResponse> signup(@FieldMap Map<String, String> params);

    @POST("update_password")
    Call<AGApiResponse> changePwd(@QueryMap Map<String, String> params);

    // Dashboard Items
    @GET("count")
    Call<List<DashboardItem>> getDashboard(@Query("lang") String lang);


    // Plates
    @GET("f/uae-plates/auction")
    Call<MZResponse> getPlates(@QueryMap Map<String, String> params);
//    public void getPlates(@QueryMap Map<String, String> params, AGRestCallback<MZResponse> cb);

    // Du planes
    @GET("f/du-vip-plan/auction")
    Call<MZResponse> getNumbers(@QueryMap Map<String, String> params);

    // Cars
    @GET("c/cars/auction")
    Call<MZResponse> getCars(@QueryMap Map<String, String> params);

  // get_Item
    @GET("get_Item")
    Call<MZResponse> getItem(@QueryMap Map<String, String> params);

    // Watches
    @GET("f/jewellery/auction")
    Call<MZResponse> getWatches(@QueryMap Map<String, String> params);

    // Properties
    @GET("f/properties/auction")
    Call<MZResponse> getProps(@QueryMap Map<String, String> params);

    // Coins
    @GET("f/coins/auction")
    Call<MZResponse> getCoins(@QueryMap Map<String, String> params);

    // Horses
    @GET("f/horses/auction")
    Call<MZResponse> getHorses(@QueryMap Map<String, String> params);

    // Bags
    @GET("f/bags/auction")
    Call<MZResponse> getBags(@QueryMap Map<String, String> params);

    // Mix
    @GET("f/mix/auction")
    Call<MZResponse> getMix(@QueryMap Map<String, String> params);

    // Direct sell
    @GET("f/special-plate/direct-sell")
    Call<MZResponse> getDirectSell(@QueryMap Map<String, String> params);

    // Direct sell details
    @GET("item")
    Call<MZDirectItem> getDirectSellDetails(@QueryMap Map<String, String> params);



    @GET("get_deposit")
    Call<Deposit> getDeposit(@QueryMap Map<String, String> params);

    @GET("post_bid")
    Call<AGApiResponse> postBid(@QueryMap Map<String, String> params);

    // Draw number
    @POST("post_draw_number")
    Call<AGApiResponse> drawNumber(@QueryMap Map<String, String> params);

    // Request plate number
    @GET("post_request")
    Call<AGApiResponse> requestPlate(@QueryMap Map<String, String> params);

    // Request plate number
    @GET("post_du_request")
    Call<AGApiResponse> requestNumber(@QueryMap Map<String, String> params);


    // Draw plate number
    @POST("post_draw_plate")
    Call<AGApiResponse> drawPlate(@QueryMap Map<String, String> params);

    // Ads
    @GET("mix_add")
    Call<MZResponse> getAds(@QueryMap Map<String, String> params);

    // My Ads
    @GET("my_adds")
    Call<List<MZItem>> getMyAds(@QueryMap Map<String, String> params);

    // My Bids
    @GET("my_bids")
    Call<MZResponse> getMyBids(@QueryMap Map<String, String> params);

    @GET("my_purchases")
    Call<MZResponse> getMyPurchases(@QueryMap Map<String, String> params);


    // My Profile
    @GET("profile")
    Call<MZResponse> getMyProfile(@QueryMap Map<String, String> params);

    @GET("push_register")
    Call<AGApiResponse> regPN(@QueryMap Map<String, String> params);

    @GET("get_version")
    Call<AGApiResponse> checkUpdateIsRequired(@QueryMap Map<String, String> params);
}
