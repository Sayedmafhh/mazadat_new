package ae.mazadat.onlineauctionapp.ui.menu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.adapter.menu.MyPurchasesAdapter;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.ui.GridFragment;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * interface.
 */
public class MyPurchasesFragment extends GridFragment {

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static MyPurchasesFragment newInstance(int columnCount) {
        MyPurchasesFragment fragment = new MyPurchasesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_base_list, container, false);

        setHasFab(true);
        setHasLoadMore(false);

        ButterKnife.bind(this, rootView);
        setHasOptionsMenu(false);

        init(getActivity());

        adapter = new MyPurchasesAdapter(getActivity(), mListener);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));

        if (savedInstanceState == null) {
            loadItems();
        }

        return rootView;
    }


    @Override
    public void serviceLoadItems(final boolean reset) {
        super.serviceLoadItems(reset);

        filterParams.put("lang", session.getMZLang());

        gettingItemsCall = api.getMyPurchases(filterParams);
        gettingItemsCall.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {

                if (response.isSuccessful()) {
                    if (isAdded() && getActivity() != null) {
                        MZResponse MZResponse = response.body();

                        List<MZItem> items = MZResponse.getMyPurchseItems();

                        // TODO: When failed - this may throw an error!

                        // TODO: Remove solr
                        doWhenSuccessLoad(reset, items);

                        if (items != null && items.size() > 0) {
                            for (int i=0; i<items.size(); i++) {
                                MZItem item = items.get(i);
                                adapter.add(item);
                            }
                        } else {
                            // TODO: Show proper message
                            AGUtil.errorAlert(getActivity(), "No items!", getString(R.string.no_purchase_items));
                        }
                    }

                } else {
                    if (isAdded() && getActivity() != null) {
                        doWhenFailureApiCall("Opps!", "Unable to load items!");
                    }
                }
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {

            }
        });
    }

    @Override
    protected void registerPN() {
        // TODO:
    }

}
