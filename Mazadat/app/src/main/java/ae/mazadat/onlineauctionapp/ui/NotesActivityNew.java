package ae.mazadat.onlineauctionapp.ui;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sudhirkumar on 4/10/2017.
 */

public class NotesActivityNew extends BaseActivity {
    @Bind(R.id.customAction)
    RelativeLayout customAction;
    @Bind(R.id.webview)

    WebView webview;
    @Bind(R.id.backBtn)
    ImageView backBtn;
    MZItem mItem;
    String mType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }

        setContentView(R.layout.activity_notes_new);
        ButterKnife.bind(this);
        String auctionType = getIntent().getStringExtra(AGConf.KEY_AUCTION_TYPE);
        Parcelable data = getIntent().getParcelableExtra(AGConf.KEY_ITEM_DATA);
        mItem = Parcels.unwrap(data);

        mType = auctionType;

        System.out.println(" type***** "+mType);
        System.out.println(" item***** "+mItem.getDetails().getNotes());
        webview.setBackgroundColor(Color.TRANSPARENT);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if (mType != null) {

            String dir = session.getMZLang().contentEquals("ae") ? "rtl" : "ltr";
            String html = "<body dir=\"" + dir + "\">";

            // Cars
            if ((mType.contentEquals(AGConf.KEY_CARS_AUCTION) || mType.contentEquals(AGConf.KEY_PLATES_AUCTION))
                    && mItem.getDetails().getNotes() != null) {
                html += mItem.getDetails().getNotes();

            }

            // Watches
            if (mType.contentEquals(AGConf.KEY_JEWELLERY_AUCTION) && mItem.getDetails().getNotes() != null) {
//                html += mItem.getDetails().getDescription();
                html += mItem.getDetails().getNotes();
            }

            // Properties
            if (mType.contentEquals(AGConf.KEY_PROP_AUCTION) && mItem.getDetails().getNotes() != null) {
//                html += mItem.getDetails().getDesc();
                html += mItem.getDetails().getNotes();
            }
            if (mType.contentEquals(AGConf.KEY_MISC_AUCTION) && mItem.getDetails().getNotes() != null) {
//                html += mItem.getDetails().getDesc();
                html += mItem.getDetails().getNotes();
            }
            html += "</body>";
            System.out.println("Notes*** "+html);
            webview.loadData(html, "text/html; charset=UTF-8", null);
        }
    }
}
