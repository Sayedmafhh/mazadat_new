package ae.mazadat.onlineauctionapp.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;

import org.parceler.Parcels;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.MZResponse;
import ae.mazadat.onlineauctionapp.ui.menu.ChangePwdFragment;
import ae.mazadat.onlineauctionapp.ui.menu.MyAdsFragment;
import ae.mazadat.onlineauctionapp.ui.menu.MyBidsFragment;
import ae.mazadat.onlineauctionapp.ui.menu.MyPurchasesFragment;
import ae.mazadat.onlineauctionapp.ui.menu.PageFragment;
import ae.mazadat.onlineauctionapp.ui.menu.ProfileFragment;
import ae.mazadat.onlineauctionapp.ui.payment.PaymentActivity;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Response;

public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ItemsListFragment.OnMZItemInteractionListener {
    protected Call<MZResponse> gettingItemsCall;
    @Nullable
    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;

    @Nullable
    @Bind(R.id.nav_view)
    NavigationView navigationView;

    @Nullable
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Nullable
    @Bind(R.id.logoDu)
    ImageView ivDu;

    protected boolean viewIsAtHome;
    protected boolean hasDrawer = true;

    AGApi api;

    SessionManager session = App.getSessionManager();

    @Override
    public void setContentView(int layoutResID) {
        initLang();
        super.setContentView(layoutResID);
        ButterKnife.bind(this);

        Fabric.with(this, new Answers());

        // Drawer menu items
        if (!session.isLoggedIn() && navigationView != null) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.nav_no_login);
        }

        buildDrawer();

        setSupportActionBar(toolbar);

        api = App.getRestAdapter().create(AGApi.class);

        if (hasDrawer && drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();
            navigationView.setNavigationItemSelectedListener(this);
        }
    }

    private void initLang() {
        Resources res = getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        String locale = session.getLang();
        conf.locale = new Locale(locale);
        res.updateConfiguration(conf, dm);
        if (locale.contentEquals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }


//    @Override
//    public void onBackPressed() {
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        displayView(id);

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }
        return true;
    }


    public void displayView(int viewId) {

        Fragment fragment = null;
        Bundle args = new Bundle();

        String title = ""; // getString(R.string.app_name);

        switch (viewId) {
            case R.id.nav_home:
                fragment = new DashboardFragment();
                title  = getString(R.string.nav_Home);
                viewIsAtHome = true;
                break;

            case R.id.nav_about:
                fragment = new PageFragment();
                args.putString(AGConf.KEY_PAGE, AGConf.KEY_PAGE_ABOUT);
                title  = getString(R.string.menu_about);
                viewIsAtHome = false;
                break;

            case R.id.nav_faq:
                fragment = new PageFragment();
                args.putString(AGConf.KEY_PAGE, AGConf.KEY_PAGE_FAQ);
                title  = getString(R.string.menu_faq);
                viewIsAtHome = false;
                break;

            case R.id.nav_terms:
                fragment = new PageFragment();
                args.putString(AGConf.KEY_PAGE, AGConf.KEY_PAGE_TOC);
                title  = getString(R.string.menu_terms);
                viewIsAtHome = false;
                break;

            case R.id.nav_contact:
                fragment = new PageFragment();
                args.putString(AGConf.KEY_PAGE, AGConf.KEY_PAGE_CONTACT);
                title  = getString(R.string.menu_contact_us);
                viewIsAtHome = false;
                break;

            case R.id.nav_my_bids:
                args.putInt(AGConf.KEY_AUCTION_TYPE, AGConf.MY_BIDS);
                fragment = new MyBidsFragment();
                title  = getString(R.string.nav_my_bids);
                viewIsAtHome = false;
                break;

            case R.id.nav_my_purchases:
                args.putInt(AGConf.KEY_AUCTION_TYPE, AGConf.MY_PURCHASES);
                fragment = new MyPurchasesFragment();
                title  = getString(R.string.nav_my_purchases);
                viewIsAtHome = false;
                break;

            case R.id.nav_my_ads:
                fragment = new MyAdsFragment();
                title  = getString(R.string.nav_my_ads);
                viewIsAtHome = false;
                break;

            case R.id.nav_reset:
                fragment = new ChangePwdFragment();
                title  = getString(R.string.nav_reset);
                viewIsAtHome = false;
                break;

            case R.id.nav_profile:
                fragment = new ProfileFragment();
                title  = getString(R.string.nav_profile);
                viewIsAtHome = false;
                break;


            // Login
            case R.id.nav_login:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_up,R.anim.no_change);
                break;

            case R.id.act_logout:
                session.logout();
                Intent i = new Intent(this, InitActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(i);
                finish();
                break;


        }

        if (fragment != null) {
            fragment.setArguments(args);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        // set the toolbar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        if (hasDrawer && drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }


//    public static void openAuction(Dashboard.Item item, Context sourceActivity) {
//        Intent intent = new Intent(sourceActivity, ItemsActivity.class);
////        intent.putExtra(AGConf.KEY_AUCTION_ITEM, Parcels.wrap(item));
//        sourceActivity.startActivity(intent);
//    }
//
//    public static void openAds(Dashboard.Item item, Context sourceActivity) {
//        Intent intent = new Intent(sourceActivity, AdsActivity.class);
////        intent.putExtra(AGConf.KEY_AUCTION_ITEM, Parcels.wrap(item));
//        sourceActivity.startActivity(intent);
//    }


    public static void openItemDetails(Context sourceActivity, String auctionType, MZItem item) {
        SessionManager session = App.getSessionManager();

        if (auctionType.contentEquals(AGConf.KEY_DIRECT_SELL))
        {
            // Should be logged in to see plate details in direct sell
            if (!session.isLoggedIn()) {
                Intent intent = new Intent(sourceActivity, LoginActivity.class);
                sourceActivity.startActivity(intent);
                ((Activity) sourceActivity).overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                return;
            }

            // If sold out plate, disable open its details!
            if ((item.getSoldBy() != null && !item.getSoldBy().contentEquals("0"))
                    || item.getBooked() == 1) {
                return;
            }
        }


        if (AGConf.KEY_CARS_AUCTION.equalsIgnoreCase(auctionType)){




            //todo ali zohair
            Intent intent = new Intent(sourceActivity, ItemDetailsActivity.class);
            intent.putExtra(AGConf.KEY_AUCTION_TYPE, auctionType);
            intent.putExtra(AGConf.KEY_ITEM_DATA, Parcels.wrap(item));
            sourceActivity.startActivity(intent);


        }else{

            Intent intent = new Intent(sourceActivity, ItemDetailsActivity.class);
            intent.putExtra(AGConf.KEY_AUCTION_TYPE, auctionType);
            intent.putExtra(AGConf.KEY_ITEM_DATA, Parcels.wrap(item));
            sourceActivity.startActivity(intent);
        }



    }


    public static void openPayment(Context sourceActivity, String auctionType) {
        Intent intent = new Intent(sourceActivity, PaymentActivity.class);
        intent.putExtra(AGConf.KEY_AUCTION_TYPE, auctionType);
//        intent.putExtra(AGConf.KEY_DEPOSIT_AMOUNT, amount);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sourceActivity.startActivity(intent);
    }



    public void setHasDrawer(boolean hasDrawer) {
        this.hasDrawer = hasDrawer;
    }


    public void switchLang() {
        String currentLang = session.getLang();
        String locale;
        String nLang;
        if (currentLang.contentEquals("ar")) {
            locale = "en";
            nLang  = "en";
        } else {
            locale = "ar";
            nLang  = "ae";
        }
        session.setLang(locale);

        Intent i = new Intent(this, InitActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        i.putExtra(AGConf.KEY_LANG, nLang);
        startActivity(i);

///        Intent refresh = getIntent();
//        startActivity(refresh);
//        this.overridePendingTransition(R.anim.rotate_out, R.anim.rotate_in);
        overridePendingTransition(0, 0);
    }


    public void buildDrawer() {
        // Ser user info
        if (session.isLoggedIn() && navigationView != null) {
            View headerLayout = navigationView.getHeaderView(0);

            TextView tvUserName  = (TextView) headerLayout.findViewById(R.id.tvUserName);
            TextView tvUserEmail = (TextView) headerLayout.findViewById(R.id.tvUserEmail);
                ImageView ivUser     = (ImageView) headerLayout.findViewById(R.id.ivUser);
//                Glide.with(this).load("http://goo.gl/gEgYUd").into(ivUser);

            tvUserName.setText(session.getUserName());
            tvUserEmail.setText(session.getUserEmail());
        }
    }

    @Override
    public void onMZItemInteraction(MZItem item, String type) {
        // Cancel click for the following items
        if (type.contentEquals(AGConf.KEY_ADS) || type.contentEquals(AGConf.KEY_MY_PURCHASES) ||
            type.contentEquals(AGConf.KEY_MY_BIDS)) {
            // Do nothing!
        } else {

            if (item.isOnlineAuction() == false  && type.contentEquals(AGConf.KEY_DU_AUCTION)){
                Log.i("1TYPE", type + " " + AGConf.KEY_DU_AUCTION);
                Intent termsConditionIntent = new Intent(this, TermsConditionsActivity.class);
                termsConditionIntent.putExtra("webcontent", item.getDuPackageDetails());
                termsConditionIntent.putExtra("title", getString(R.string.du_package_details));
                startActivity(termsConditionIntent);
            }else {
                Log.i("TYPE", type + " " + AGConf.KEY_DU_AUCTION);
                openItemDetails(this, type, item);
            }
        }
    }
   /* public void serviceLoadItems(final boolean reset, MZItem item) {

         Map<String, String> filterParams = new HashMap<>();
     //   super.serviceLoadItems(reset);

        filterParams.put("lang", session.getMZLang());
        filterParams.put("category", String.valueOf(item.getCategory()));
        filterParams.put("item_id", String.valueOf(item.getId()));
        // filterParams.put("development", String.valueOf(1));

        gettingItemsCall = api.getItem(filterParams);
        System.out.println("cars fragments "+filterParams.toString());
        gettingItemsCall.enqueue(new AGRetrofitCallback<MZResponse>() {
            @Override
            public void onResponse(Call<MZResponse> call, Response<MZResponse> response) {

                if (response.isSuccessful()) {
                    MZResponse MZResponse = response.body();
                    Log.i("Is Online Bid", "Is online bid: " + (MZResponse.isActivate_bid() ? "YES" : "NO") );
                    Date currentTime = MZResponse.getCurrentTime();
                    long timeDiff;
                    if (currentTime != null) {
                        timeDiff = System.currentTimeMillis() - currentTime.getTime();
                    } else {
                        timeDiff = 0;
                    }

                    int deposit      = MZResponse.getUserDeposit();
                    List<MZItem> items = MZResponse.getItems();

                 *//*   mFilters = MZResponse.getFilters();
                    mSorts   = MZResponse.getSorts();

                    // TODO: When failed - this may throw an error!

                    _initFiltersAndSorts();

                    doWhenSuccessLoad(reset, items);*//*

                    //session.setCarsDeposit(deposit);

                 *//*   if (items != null && items.size() > 0) {
                        for (int i=0; i<items.size(); i++) {
                            MZItem item = items.get(i);
                            item.setOnlineAuction(!(MZResponse.isActivate_bid()));
//                            item.setUserDeposit(deposit);
                            item.setCurrentTime(currentTime);
                            item.setTimeDiff(timeDiff);
                            if (session.getUid() > 0 && item.getBidder() != null && item.getBidder().contentEquals(session.getUid() + "")) {
                                item.setMyBid(true);
                            }
                            adapter.add(item);
                        }
                    } else {
                        initNoItems();
                    }*//*
                } else {
              //      doWhenFailureApiCall("Opps!", "Unable to load items!");
                }
            }

            @Override
            public void onFailure(Call<MZResponse> call, Throwable t) {
            }
        });

    }

*/


}
