package ae.mazadat.onlineauctionapp.viewholder;

import android.view.View;
import android.widget.TextView;

import ae.mazadat.onlineauctionapp.R;
import butterknife.Bind;

/**
 * Created by agile on 6/8/16.
 */
public class MyBidItemViewHolder extends ItemViewHolder {

    @Bind(R.id.tvItemName)
    public TextView tvItemName;

    @Bind(R.id.price2) // Max Bid
    public TextView price2;

    public MyBidItemViewHolder(View view) {
        super(view);
    }
}
