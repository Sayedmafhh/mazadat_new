package ae.mazadat.onlineauctionapp.ui.menu;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by agile on 7/4/16.
 */
public class PageFragment extends Fragment {

    SessionManager session = App.getSessionManager();

    @Bind(R.id.webview)
    WebView webview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_page, container, false);
        ButterKnife.bind(this, view);


        webview.setBackgroundColor(Color.TRANSPARENT);
        WebSettings webSetting = webview.getSettings(); // added by Hussam 2016-08-28

        // Set true for enable JavaScript feature or Set False to Disable JavaScript.
        webSetting.setJavaScriptEnabled(true); // added by Hussam 2016-08-28

        if (getArguments() != null) {
            String page = getArguments().getString(AGConf.KEY_PAGE);
            if (page != null) {
                switch (page) {
                    case AGConf.KEY_PAGE_ABOUT:
                        showPage("about_page");
                        break;

                    case AGConf.KEY_PAGE_FAQ:
                        showPage("faq_page");
                        break;

                    case AGConf.KEY_PAGE_TOC:
                        showPage("toc_page");
                        break;

                    case AGConf.KEY_PAGE_CONTACT:
                        showPage("contact_us");
                        break;

                }
            }

        }

        return view;
    }

    private void showPage(String page) {
        String lang = session.getMZLang();
        webview.loadUrl(AGConf.REST_API + page + "?lang=" + lang);
    }
}
