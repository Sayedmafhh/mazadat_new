package ae.mazadat.onlineauctionapp.model.rest;

import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;
import java.util.List;

import ae.mazadat.onlineauctionapp.model.MZItem;

/**
 * Created by agile on 4/3/16.
 */
@Parcel
public class MZResponse {

    @Nullable
    @SerializedName("user")
    MZUser profile;

    @SerializedName("current_time")
    Date currentTime;

    @Nullable
    @SerializedName("user_deposit")
    int userDeposit;

    @Nullable
    @SerializedName("activate_bid")
    int activate_bid = 0;

    @Nullable
    @SerializedName("top_message")
    String topMessage;

    @Nullable
    @SerializedName("universal_msgs")
    List<UniversalMsg> universalMsgs;

    @Nullable
    @SerializedName("filters")
    List<MZFilter> filters;

    @Nullable
    @SerializedName("sorts")
    List<MZSort> sorts;

    @SerializedName("items")
    List<MZItem> items;

    // My Bids
    @Nullable
    @SerializedName("bids")
    List<MZItem> myBids;

    // My Purchases
    @Nullable
    @SerializedName("list_items")
    List<MZItem> myPurchseItems;

    public Date getCurrentTime() {
        return currentTime;
    }

    @Nullable
    public int getUserDeposit() {
        return userDeposit;
    }

    public List<MZItem> getItems() {
        return items;
    }

    @Nullable
    public List<MZFilter> getFilters() {
        return filters;
    }

    @Nullable
    public List<MZSort> getSorts() {
        return sorts;
    }

    @Nullable
    public MZUser getProfile() {
        return profile;
    }

    @Nullable
    public List<MZItem> getMyPurchseItems() {
        return myPurchseItems;
    }

    @Nullable
    public List<MZItem> getMyBids() {
        return myBids;
    }

    @Nullable
    public boolean isActivate_bid() {
        Log.i("ACTIVATEBID", activate_bid + "");
        return activate_bid == 0;
    }

    @Nullable
    public String getTopMessage() {
        return topMessage;
    }

    @Nullable
    public List<UniversalMsg> getUniversalMsgs() {
        return universalMsgs;
    }

    @Override
    public String toString() {
        return "MZResponse{" +
                "profile=" + profile +
                ", currentTime=" + currentTime +
                ", userDeposit=" + userDeposit +
                ", activate_bid=" + activate_bid +
                ", topMessage='" + topMessage + '\'' +
                ", universalMsgs=" + universalMsgs +
                ", filters=" + filters +
                ", sorts=" + sorts +
                ", items=" + items +
                ", myBids=" + myBids +
                ", myPurchseItems=" + myPurchseItems +
                '}';
    }
}
