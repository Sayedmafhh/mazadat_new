package ae.mazadat.onlineauctionapp.ui.details.tab;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import org.parceler.Parcels;

import java.util.HashMap;
import java.util.Map;

import ae.mazadat.onlineauctionapp.AGConf;
import ae.mazadat.onlineauctionapp.App;
import ae.mazadat.onlineauctionapp.R;
import ae.mazadat.onlineauctionapp.model.MZItem;
import ae.mazadat.onlineauctionapp.model.rest.Deposit;
import ae.mazadat.onlineauctionapp.ui.LoginActivity;
import ae.mazadat.onlineauctionapp.util.AGUtil;
import ae.mazadat.onlineauctionapp.util.SessionManager;
import ae.mazadat.onlineauctionapp.util.rest.AGApi;
import ae.mazadat.onlineauctionapp.util.rest.AGApiResponse;
import ae.mazadat.onlineauctionapp.util.rest.AGRetrofitCallback;
import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by agile on 4/22/16.
 */
public class BidFragment extends Fragment {

    AGApi api;

    @Bind(R.id.min_bid)
    TextView minBid;

    @Bind(R.id.tvPriceD)
    TextView tvPriceD;

    @Bind(R.id.price)
    TextView tvPrice;

    @Bind(R.id.swAuto)
    Switch swAuto;

    @Bind(R.id.chTerms)
    CheckBox chTerms;

    @Bind(R.id.tvTerms)
    TextView tvTerms;

    @Bind(R.id.etNumber)
    EditText etNumber;

    @Bind(R.id.tiNumber)
    TextInputLayout tiNumber;

    @Bind(R.id.btnDec)
    Button btnDec;

    @Bind(R.id.btnInc)
    Button btnInc;

    @Bind(R.id.btnBid)
    Button btnBid;

    // Bid UIs
    @Bind(R.id.vAutoBid)
    View vAutoBid;

    @Bind(R.id.vBid)
    View vBid;

    @Bind(R.id.vTerms)
    View vTerms;

    @Bind(R.id.tvBidderMsg)
    TextView tvBidderMsg;

    MZItem mItem;
    Deposit mDeposit;
    String currentAuction;

    SessionManager session = App.getSessionManager();

    int min;
    long price;
    int autoBid = 0;
    int isVip = 0;

    int userDeposit = 0;
    int maxAutoBid = 0;
    int bidDeposit = 0;

    public BidFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static BidFragment newInstance(MZItem item, String currentAuction, Deposit deposit) {
        BidFragment fragment = new BidFragment();
        Bundle args = new Bundle();
        args.putParcelable(AGConf.KEY_ITEM_DATA, Parcels.wrap(item));
        args.putParcelable(AGConf.KEY_ITEM_DEPOSIT, Parcels.wrap(deposit));
        args.putString(AGConf.KEY_AUCTION_TYPE, currentAuction);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Parcelable data = getArguments().getParcelable(AGConf.KEY_ITEM_DATA);
            currentAuction = getArguments().getString(AGConf.KEY_AUCTION_TYPE);
            Parcelable depositData = getArguments().getParcelable(AGConf.KEY_ITEM_DEPOSIT);

            mItem = Parcels.unwrap(data);
            mDeposit = Parcels.unwrap(depositData);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bid, container, false);

        ButterKnife.bind(this, rootView);

        min = mItem.getMinBid();
        price = mItem.getPrice();

        if (mItem.isBided()) {
            blink(tvPrice, 0);
        }

        api = App.getRestAdapter().create(AGApi.class);

        if (mDeposit != null) {
            userDeposit = mDeposit.getUserDeposit();
            maxAutoBid = mDeposit.getMaxBid();
            isVip = mDeposit.isVip();
            autoBid = mDeposit.isAutoBid();
        }


        swAuto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (autoBid != 1) {
                        if (etNumber.getText().toString().length() > 0) {
                            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) SweetAlertDialog alertAb = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getString(R.string.bid_auto_title))
                                    .setContentText(getString(R.string.bid_confirm_auto, Integer.parseInt(etNumber.getText().toString())))
                                    .setConfirmText(getString(R.string.yes))
                                    .setCancelText(getString(R.string.cancel))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog dialog) {
                                            autoBid = 1;
                                            swAuto.setChecked(true);
                                            dialog.dismissWithAnimation();
                                            // Show indication about the max bid
                                            tiNumber.setHintEnabled(true);
//                                        tiNumber.setHint(getString(R.string.bid_enter_limit));
                                            btnBid.setText(getString(R.string.bid_start_auto));
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog dialog) {
                                            swAuto.setChecked(false);
                                            dialog.dismissWithAnimation();
                                            tiNumber.setHintEnabled(false);
                                        }
                                    });

                            alertAb.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    swAuto.setChecked(false);
                                    tiNumber.setHintEnabled(false);
                                }
                            });

                            alertAb.show();
                        }
                    }else{
                        Toast.makeText(getActivity(),"Bidding value is empty",Toast.LENGTH_LONG).show();;
                    }
                } else {
                    autoBid = 0;
                    tiNumber.setHintEnabled(false);
                    btnBid.setText(getString(R.string.bid_now));
                }
            }
        });

        bidDeposit = mItem.getDeposit();

        // Format it
        minBid.setText(String.format(getString(R.string.price_aed), min));
        tvPrice.setText(String.format(getString(R.string.price_aed), price));

        // Bids
        final long userMinBid = price + min;
        etNumber.setText(userMinBid + "");

        // Calc Details
        calcDetails();

        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                calcDetails();
            }
        });

        if (session.isTermsAgreed(currentAuction)) {
            chTerms.setEnabled(false);
            chTerms.setChecked(true);
            btnBid.setEnabled(true); // Always enabled
        }

        chTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                btnBid.setEnabled(isChecked);
                if (isChecked) {
                    session.setTermsAgreed(currentAuction);
                }
            }
        });

        _handleDeposit();

        // TODO: Disable btnBid when I'm the last bidder


        if (mItem.isOnlineAuction() == false) {
            Log.i("BidFragment", "Bid is online");
            etNumber.setEnabled(false);
            btnBid.setEnabled(false);
            btnInc.setEnabled(false);
            btnDec.setEnabled(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                btnBid.setBackgroundColor(getActivity().getColor(R.color.colorItemRedTrans));
            }
        }else {
            btnBid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (chTerms.isChecked()) {
                        if (!session.isLoggedIn()) {
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            getActivity().startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_up, R.anim.no_change);
                            return;
                        } else {
                            postBid();
                        }
                    }
                }
            });
        }


        btnInc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentNumber = etNumber.getText().toString().isEmpty() ? 0 :
                        Integer.parseInt(etNumber.getText().toString());
                etNumber.setText((currentNumber + min) + "");
            }
        });

        btnDec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentNumber = etNumber.getText().toString().isEmpty() ? 0 :
                        Integer.parseInt(etNumber.getText().toString());

                int newCurrent = currentNumber - min;
                if (newCurrent < (price + min)) {
                    // Do nothing, can not bid with low number
                } else {
                    etNumber.setText(newCurrent + "");
                }
            }
        });

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity()); // Context, this, etc.
                LayoutInflater li = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);

                View vi = li.inflate(R.layout.terms, null);
                WebView webview = (WebView) vi.findViewById(R.id.webview);

                String lang = session.getMZLang();
                webview.loadUrl(AGConf.REST_API + "toc_page?lang=" + lang);

                dialog.setContentView(vi);
                dialog.setTitle(getString(R.string.terms));
                dialog.show();
            }
        });

        _setBidderUI(mItem.isMyBid());

        return rootView;
    }

    public void blink(final TextView v, final int timeNo) {
        ViewPropertyAnimator an = v.animate().setDuration(1200).alpha(1);
        final int redColor;
        final int normalColor;
        if (isAdded() && getActivity() != null) {
            redColor = getActivity().getResources().getColor(R.color.colorItemRed);
            normalColor = getActivity().getResources().getColor(R.color.colorPrimaryText);

            if (timeNo < 4) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    an.withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            v.setTextColor(redColor);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                v.animate().setDuration(700).alpha(.1f).withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        blink(v, timeNo + 1);
                                    }
                                });
                            }

                        }
                    });
                }
            } else {
                v.setTextColor(normalColor);
            }
        }
    }

    private void _setBidderUI(boolean isBidder) {
        btnBid.setVisibility(isBidder ? View.GONE : View.VISIBLE);
        tvBidderMsg.setVisibility(!isBidder ? View.GONE : View.VISIBLE);
        vBid.setVisibility(isBidder ? View.GONE : View.VISIBLE);
        vAutoBid.setVisibility(isBidder ? View.GONE : View.VISIBLE);
        vTerms.setVisibility(isBidder ? View.GONE : View.VISIBLE);
    }

    private void _handleDeposit() {
        if (autoBid != 0) {
            //- Make the checkbox of "Auto Bid" on, and make it disabled.
            // - Change the label of amount field to "Max Limit Bid" instead of "Bid Amount"
            // - Show the "max_auto_bid" value inside the field of next bid amount, and make it disabled.
            // - Change the label of the button to be "Start Auto Bid", and make it disabled
            swAuto.setEnabled(false);
            swAuto.setChecked(true);
            // Show indication about the max bid
            tiNumber.setHintEnabled(true);
            tiNumber.setHint(getString(R.string.bid_enter_limit));

            tiNumber.setEnabled(false);
            etNumber.setText(maxAutoBid + "");

            swAuto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getActivity(), getString(R.string.err_autobid_mode), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    public void calcDetails() {
        int bidAmount = etNumber.getText().toString().isEmpty() ? 0 : Integer.parseInt(etNumber.getText().toString());

        int deposit;
        int toPay;
        int remain;

        if (mItem.getDepositType() == 1) {
            // Percentage
            Double dDep = new Double(bidAmount * ((double) bidDeposit / 100));
            deposit = dDep.intValue(); // amount * percentage
        } else {
            // Fixed
            deposit = bidDeposit;
        }

        if (userDeposit > deposit) {
            toPay = 0;
            remain = (userDeposit - deposit);
        } else if (userDeposit < deposit) {
            toPay = (deposit - userDeposit);
            remain = 0;
        } else {
            // Equals ==
            toPay = 0;
            remain = 0;
        }

        // Update deposit tab plz :)
        DepositFragment.deposit = getString(R.string.price_aed, deposit);
        DepositFragment.userDeposit = getString(R.string.price_aed, userDeposit);
        DepositFragment.toPay = getString(R.string.price_aed, toPay);
        DepositFragment.remain = getString(R.string.price_aed, remain);
    }

    SweetAlertDialog pDialog;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }


    public void postBid() {

        Answers.getInstance().logCustom(new CustomEvent("Post Bid"));

        String bidAmount = String.valueOf(etNumber.getText().toString());
//        ((ItemDetailsActivity)getActivity()).isMyBid = true;

        Map<String, String> params = new HashMap<>();
        params.put("item_id", String.valueOf(mItem.getId()));
        params.put("bid_amount", bidAmount);
        params.put("auto_bid", String.valueOf(autoBid));
        params.put("lang", session.getMZLang());

        pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(R.color.colorPrimaryDark);
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();


        Call<AGApiResponse> postBidCall = api.postBid(params);
        postBidCall.enqueue(new AGRetrofitCallback<AGApiResponse>() {
            @Override
            public void onResponse(Call<AGApiResponse> call, Response<AGApiResponse> response) {

                if (response.isSuccessful()) {
                    AGApiResponse itemsResponse = response.body();
                    if (itemsResponse != null) {

                        if (itemsResponse.getStatus().contentEquals("200")) {
                            String msg = "";
                            msg = itemsResponse.getMsg();

                            // DO nothing, logic moved to ItemDetailsFragment - as per firebase feedback
//                            pDialog.dismiss();
//                            MZUtil.doWhenSuccessBid(getActivity(), msg);
                        } else {
                            pDialog.dismiss();
                            AGUtil.errorAlert(getActivity(), "Opps!", itemsResponse.getMsg());
                            Answers.getInstance().logCustom(new CustomEvent("Post Bid").putCustomAttribute("success", "No")
                                    .putCustomAttribute("Error Me", "No"));
                        }

                    } else {
                        pDialog.dismiss();
                        AGUtil.errorAlert(getActivity(), "Opps!", "Unable to post your bid!");
                    }

                } else {
                    pDialog.dismiss();
                    AGUtil.errorAlert(getActivity(), "Opps!", "An error has been occurred!");
                }
            }

            @Override
            public void onFailure(Call<AGApiResponse> call, Throwable t) {
                super.onFailure(call, t);
                pDialog.dismiss();
                t.printStackTrace();
                AGUtil.errorAlert(getActivity(), "Opps!", t.getMessage());
            }
        });
    }

}
