package ae.mazadat.onlineauctionapp.model.rest;

import org.parceler.Parcel;

/**
 * Created by agile on 4/4/16.
 */
@Parcel
public class ResetResponse {

    String token;
    int code;
    String msg;

    public String getToken() {
        return token;
    }

    public int getCode() {
        return code;
    }

    public String getMsg(String defaultMsg) {
        if (msg == null) {
            return defaultMsg;
        }
        return msg;
    }
}
