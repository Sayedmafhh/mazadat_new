package ae.mazadat.onlineauctionapp.model.rest;

import org.parceler.Parcel;

/**
 * Created by agile on 4/30/16.
 */
@Parcel
public class MZImg {
    String src;
    String id;
    String featured;
    String url;
    String thump;

    public void setSrc(String src) {
        this.src = src;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThump() {
        return thump;
    }

    public void setThump(String thump) {
        this.thump = thump;
    }

    public String getSrc() {
        return src;
    }

    @Override
    public String toString() {
        return "MZImg{" +
                "src='" + src + '\'' +
                ", id='" + id + '\'' +
                ", featured='" + featured + '\'' +
                ", url='" + url + '\'' +
                ", thump='" + thump + '\'' +
                '}';
    }
}
