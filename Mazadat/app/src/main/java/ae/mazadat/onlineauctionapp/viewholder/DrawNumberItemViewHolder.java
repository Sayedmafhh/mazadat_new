package ae.mazadat.onlineauctionapp.viewholder;

import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ae.mazadat.onlineauctionapp.R;
import butterknife.Bind;

/**
 * Created by agile on 4/1/16.
 */
public class DrawNumberItemViewHolder extends DrawItemViewHolder {

    @Bind(R.id.card)
    public CardView card;

    @Nullable
    @Bind(R.id.tvPltR)
    public TextView tvPltR;

    @Nullable
    @Bind(R.id.tvPltL)
    public TextView tvPltL;

    @Nullable
    @Bind(R.id.ivPlate)
    public ImageView ivPlate;

    @Nullable
    @Bind(R.id.ivNumber)
    ImageView ivNumber;

    @Nullable
    @Bind(R.id.tvPrefix)
    public TextView tvPrefix;

    @Nullable
    @Bind(R.id.tvNumber)
    public TextView tvNumber;

    @Nullable
    @Bind(R.id.price)
    public TextView tvPrice;

    public DrawNumberItemViewHolder(View view) {
        super(view);
    }


}
